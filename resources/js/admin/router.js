import VueRouter from "vue-router";
import NotFound from "./components/NotFound";

const DashboardComponent = () => import(/* webpackChunkName: "admin-dashboard" */ './views/dashboard/IndexComponent');
const AllNotificationComponent = () => import(/* webpackChunkName: "admin-dashboard" */ './views/dashboard/AllNotificationComponent');
//user-managment
const UserMangIndexComponent = () => import(/* webpackChunkName: "admin-um-index" */ './views/user-management/UserMangIndexComponent');
const CustomerShowComponent = () => import(/* webpackChunkName: "admin-um-cus-show" */ './views/user-management/CustomerShowComponent');
const EmployeeShowComponent = () => import(/* webpackChunkName: "admin-um-emp-show" */ './views/user-management/EmployeeShowComponent');
const OrganizationVerifyComponent = () => import(/* webpackChunkName: "admin-um-org-verify-show" */ './views/user-management/OrganizationVerifyComponent');
const OrganizationShowComponent = () => import(/* webpackChunkName: "admin-um-org-show" */ './views/user-management/OrganizationShowComponent');
// Profile
const ProfileComponent = () => import(/* webpackChunkName:  "admin-profile-view" */ './views/dashboard/ProfileComponent');
const EditProfileComponent = () => import(/* webpackChunkName:  "admin-profile-edit" */ './views/dashboard/EditProfileComponent');
//clients
const JobsComponent = () => import(/* webpackChunkName: "admin-job-index" */ './views/jobs/IndexComponent');
const JobsShowComponent = () => import(/* webpackChunkName: "admin-job-show-index" */ './views/jobs/ShowComponent');
const JobsCandidatesComponent = () => import(/* webpackChunkName: "admin-job-cand-index" */ './views/jobs/CandidateComponent');
// Packages
const PackageComponent = () => import(/* webpackChunkName: "admin-package-index" */ './views/packages/IndexComponent');
const CommisionComponent = () => import(/* webpackChunkName: "admin-comm-index" */ './views/packages/CommisionComponent');
// Report
const ReportLogComponent = () => import(/* webpackChunkName: "admin-rep-index" */ './views/reports/IndexComponent');
const ReportShowComponent = () => import(/* webpackChunkName: "admin-rep-show-index" */ './views/reports/ShowComponent');
// Payment
const PaymentIndexComponent = () => import(/* webpackChunkName:  "admin-payment-index" */ './views/payments/IndexComponent');
const SubLogComponent = () => import(/* webpackChunkName:  "admin-sub-log-index" */ './views/payments/SubLogComponent');
// Rating & Reviews
const RatingComponent = () => import(/* webpackChunkName:  "admin-ratings-index" */ './views/ratings/IndexComponent');
const RatingShowComponent = () => import(/* webpackChunkName:  "admin-rat-show-index" */ './views/ratings/ShowComponent');

const router = new VueRouter({
    mode: 'history',
    base: `${process.env.NODE_ENV === 'adminproduction' ? process.env.MIX_PRODUCTION_BASE : process.env.MIX_BASE_URL}/admin/`,
    //base: `admin`,
    routes: [
        {
            path: '/',
            name: 'index',
            component: DashboardComponent,
            meta: {
                title: "Dashboard",
                description: ""
            }
        },
        {
            path: '/dashboard',
            name: 'home',
            component: DashboardComponent,
            meta: {
                title: "Dashboard",
                description: ""
            }
        },
        {
            path: '/home',
            name: 'home',
            component: DashboardComponent,
            meta: {
                title: "Dashboard",
                description: ""
            }
        },
        {
            path: '/profile',
            name: 'profile',
            component: ProfileComponent,
            meta: {
                title: "Profile",
                description: ""
            }
        },
        {
            path: '/profile/edit',
            name: 'profile.edit',
            component: EditProfileComponent,
            meta: {
                title: "Edit Profile",
                description: ""
            }
        },
        {
            path: '/user-management',
            name: 'usermg',
            component: UserMangIndexComponent,
            meta: {
                title: "User Management",
                description: ""
            }

        },
        {
            path: '/user-management/customer/:id',
            name: 'usermg.customer.show',
            component: CustomerShowComponent,
            meta: {
                title: "Customer Profile",
                description: ""
            }
        },
        {
            path: '/user-management/employee/:id',
            name: 'usermg.employee.show',
            component: EmployeeShowComponent,
            meta: {
                title: "Employee Profile",
                description: ""
            }
        },
        {
            path: '/user-management/organizations/:id',
            name: 'usermg.org.show',
            component: OrganizationShowComponent,
            meta: {
                title: "Organization Profile",
                description: ""
            }
        },
        {
            path: '/user-management/organization/verify',
            name: 'usermg.org.verify',
            component: OrganizationVerifyComponent,
            meta: {
                title: "Verify Organization",
                description: ""
            }
        },        
        {
            path: '/job-management',
            name: 'jobs',
            component: JobsComponent,
            meta: {
                title: "Jobs Managment",
                description: ""
            }
        },
        {
            path: '/job-management/:id',
            name: 'jobs.show',
            component: JobsShowComponent,
            meta: {
                title: "Job Detail",
                description: ""
            }
        },
        {
            path: '/job-management/:id/candidates',
            name: 'jobs.candidates,show',
            component: JobsCandidatesComponent,
            meta: {
                title: "Job Candidates",
                description: ""
            }
        },        
        {
            path: '/package-managment',
            name: 'package',
            component: PackageComponent,
            meta: {
                title: "Package Management",
                description: ""
            }
        },        
        {
            path: '/commision-module',
            name: 'commision',
            component: CommisionComponent,
            meta: {
                title: "Commision Module",
                description: ""
            }
        },        
        {
            path: '/report-log',
            name: 'report',
            component: ReportLogComponent,
            meta: {
                title: "Report Logs",
                description: ""
            }
        },        
        {
            path: '/report-log/:id',
            name: 'report.show',
            component: ReportShowComponent,
            meta: {
                title: "Report Detail",
                description: ""
            }
        },        
        {
            path: '/payment-logs',
            name: 'payment',
            component: PaymentIndexComponent,
            meta: {
                title: "Payment Logs",
                description: ""
            }
        },     
        {
            path: '/subscription-logs',
            name: 'sublog',
            component: SubLogComponent,
            meta: {
                title: "Subscription Logs",
                description: ""
            }
        },     
        {
            path: '/rate-and-reviews',
            name: 'reviews',
            component: RatingComponent,
            meta: {
                title: "Rating & Reviews",
                description: ""
            }
        },     
        {
            path: '/rate-and-reviews/:id',
            name: 'reviews.show',
            component: RatingShowComponent,
            meta: {
                title: "Rating & Review - Detail",
                description: ""
            }
        },     

        {
            path: '*',
            name: 'notfound',
            component: NotFound,
            meta: {
                title: "Not found",
                description: ""
            }
        },

    ],
});

router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = window.appname+" - " + nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
        // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});

export default router;
