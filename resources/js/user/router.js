import VueRouter from "vue-router";
import NotFound from "./components/NotFound";

const HomeComponent = () => import(/* webpackChunkName: "home" */ './views/home/IndexComponent');
const ChatComponent = () => import(/* webpackChunkName: "chat" */ './views/chat/ChatComponent');
const AboutUsComponent = () => import(/* webpackChunkName: "aboutus" */ './views/static/AboutUsComponent');
const ContactUsComponent = () => import(/* webpackChunkName: "contact-us" */ './views/static/ContactComponent');
const ProfileComponent = () => import(/* webpackChunkName: "profile" */ './views/static/ProfileComponent');
const EditProfileComponent = () => import(/* webpackChunkName: "profile-edit" */ './views/static/EditProfileComponent');
const EditProfileSecurityQuestionsComponent = () => import(/* webpackChunkName: "profile-edit" */ './views/static/EditProfileSecurityQuestionsComponent');
const NotificationComponemt = () => import(/* webpackChunkName: "notitifications" */ './views/notification/NotificationComponent');
const UserShowComponent = ()=> import(/* webpackChunkName: "user-show" */ './views/users/ShowComponent.vue');
const BookARide = ()=> import(/* webpackChunkName: "book-a-ride" */ './views/booking/BookARide.vue');
const MyRides = ()=> import(/* webpackChunkName: "my-rides" */ './views/booking/MyRides.vue');
const BookingDetail = ()=> import(/* webpackChunkName: "my-rides" */ './views/booking/BookingDetail.vue');
const ComplaintComponent = ()=> import(/* webpackChunkName: "complaint-component" */ './views/ComplaintComponent.vue');
const RefferalComponent = ()=> import(/* webpackChunkName: "refferal-component" */ './views/RefferalComponent.vue');
const WalletComponent = ()=> import(/* webpackChunkName: "refferal-component" */ './views/WalletComponent.vue');



import ProjectRoute from './views/projects/router';
import OrganizationRoute from './views/organizations/router';
import NetworkRoute from './views/network/router';
import JobRoute from './views/job/router';

const baseRoutes = [
    {
        path: '/',
        name: 'index',
        component: HomeComponent,
        meta: {
            title: "Dashboard",
            description: ""
        }
    },
    {
        path: '/home',
        name: 'home',
        component: HomeComponent,
        meta: {
            title: "Dashboard",
            description: ""
        }
    },
    {
        path: '/ride-book',
        name: 'bookaride',
        component: BookARide,
        meta: {
            title: "Ride a Book",
            description: ""
        }
    },
    {
        path: '/my-rides',
        name: 'myrides',
        component: MyRides,
        meta: {
            title: "My Rides",
            description: ""
        }
    },
    {
        path: '/booking-detail/:id',
        name: 'bookingdetail',
        component: BookingDetail,
        meta: {
            title: "Booking detail",
            description: ""
        }
    },
    // { path: '/dashboard', redirect: { name: 'bookaride' }}
    // ,
    {
        path: '/dashboard',
        name: 'dashboard',
        component: HomeComponent,
        meta: {
            title: "Dashboard",
            description: ""
        }
    },
    
    {
        path: '/profile',
        name: 'profile',
        component: ProfileComponent,
        meta: {
            title: "Profile",
            description: ""
        }
    },    
    {
        path: '/edit-profile',
        name: 'profile.edit',
        component: EditProfileComponent,
        meta: {
            title: "Edit Profile",
            description: ""
        }
    },
    {
        path: '/edit-profile-security-questions',
        name: 'profile.edit.security.questions',
        component: EditProfileSecurityQuestionsComponent,
        meta: {
            title: "Edit Profile Security Questions",
            description: ""
        }
    },     
    {
        path: '/about-us',
        name: 'about',
        component: AboutUsComponent,
        meta: {
            title: "About Us",
            description: ""
        }
    },    
    {
        path: '/contact-us',
        name: 'contactus',
        component: ContactUsComponent,
        meta: {
            title: "Contact Us",
            description: ""
        }
    },
    {
        path: '/notifications',
        name: 'notifications',
        component: NotificationComponemt,
        meta: {
            title: "Notifications",
            description: ""
        }
    },        
    {
        path: '/chat/:id?',
        name: 'chat',
        component: ChatComponent,
        meta: {
            title: "Chat",
            description: ""
        }
    },
    {
        path: '/users/:id',
        name: 'users.show',
        component: UserShowComponent,
        meta: {
            title: "User Detail",
            description: ""
        }
    },
    {
        path: '/complaint',
        name: 'complaint',
        component: ComplaintComponent,
        meta: {
            title: "Complaint Component",
            description: ""
        }
    }, 
    {
        path: '/refferal',
        name: 'refferal',
        component: RefferalComponent,
        meta: {
            title: "Refferal Component",
            description: ""
        }
    },
    {
        path: '/wallet',
        name: 'wallet',
        component: WalletComponent,
        meta: {
            title: "Wallet Component",
            description: ""
        }
    }, 

    {
        path: '*',
        name: 'notfound',
        component: NotFound,
        meta: {
            title: "Not found",
            description: ""
        }
    },

];

const routes = baseRoutes.concat(ProjectRoute, OrganizationRoute, NetworkRoute, JobRoute);

const router = new VueRouter({
    mode: 'history',
    base: `${process.env.NODE_ENV === 'userproduction' ? process.env.MIX_PRODUCTION_BASE : process.env.MIX_BASE_URL}/`,
    //base: `admin`,
    routes: routes,
});

router.beforeEach((to, from, next) => {

    // if("middleware" in to.meta){
        if(window.user){
            // do nothing
         }else if(to.name != 'about' && to.name != 'notfound' && to.name != 'contactus' && to.name != 'home' && to.name != 'index'){
             window.location.href = window.base_url + '/login';
         }
    // }

    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = window.appname+" - " + nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
        // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});

export default router;
