require('../bootstrap');

// window.Vue = require('vue');
import Vue from 'vue';
import VueImg from 'v-img';
Vue.use(VueImg);
import VueRouter from "vue-router";
import VuejsDialog from 'vuejs-dialog';
import VueToastr2 from 'vue-toastr-2';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import App from './components/App'
import router from "./router";
import VeeValidate from 'vee-validate';
import Vuetify from 'vuetify';
import VueTimeago from 'vue-timeago'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import Fragment from 'vue-fragment'
import DaySpanVuetify from 'dayspan-vuetify'
import VueMomentsAgo from 'vue-moments-ago'
import 'vuetify/dist/vuetify.min.css'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Img from './components/Img.vue';
import * as VueGoogleMaps from 'vue2-google-maps'
import VueClipboard from 'vue-clipboard2'


VueClipboard.config.autoSetContainer = true // add this line
Vue.use(VueClipboard)


// import VueGooglePlaces from 'vue-google-places'
window.toastr = require('toastr');
Vue.use(Vuetify);
Vue.use(Fragment.Plugin)
Vue.use(VueRouter);
Vue.use(VueToastr2);
Vue.use(VeeValidate);
Vue.use(VueMomentsAgo);
Vue.component('Img', Img);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
});
Vue.use(DaySpanVuetify, {
    methods: {
      getDefaultEventColor: () => '#1976d2'
    }
});
Vue.use(PerfectScrollbar)

Vue.use(VuejsDialog, {
    html: true,
    loader: true,
    okText: 'Proceed',
    cancelText: 'Cancel',
    // animation: 'bounce'
});
Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyCyDamIuNTXON7fh1TMdf5ik2J0bfQdrdo',
      libraries: 'places', // This is required if you use the Autocomplete plugin
      // OR: libraries: 'places,drawing'
      // OR: libraries: 'places,drawing,visualization'
      // (as you require)
   
      //// If you want to set the version, you can do so:
      // v: '3.26',
    },
   
    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    autobindAllEvents: false,
   
    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
  })

Vue.mixin({
    methods:{
        buildFormData(formData, data, parentKey) {

            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    this.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;

                formData.append(parentKey, value);
            }
        },
        dateFormat(date){
            return (date) ? date.getFullYear() + '-' + ((date.getMonth() +1).toString().padStart(2, 0)) + '-' + (date.getDate().toString().padStart(2, 0)) : '';
        },
        getIconByMimeType(mime_type){
            let types = {
                'img' : ['image/jpeg', 'image/jpg', 'image/png', 'image/bmp', 'image/gif'],
                'doc' : ['application/doc', 'application/docx'],
                'excel' : ['application/xls', 'application/xlsx'],
                'pdf' : ['application/pdf'],
            }

            console.log(types, mime_type, types.img.includes(mime_type));
            if(types.img.includes(mime_type)) return '/users/images/img.png';
            else if(types.doc.includes(mime_type)) return '/users/images/doc.png';
            else if(types.excel.includes(mime_type)) return '/users/images/excel.png';
            else if(types.pdf.includes(mime_type)) return '/users/images/pdf.png';
            else return '/users/images/default.png';
        },
        formatAmount(amount){
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
            });
            return formatter.format(amount);
        },
        openBtModel(elem){
            jQuery(`#${elem}`).modal('show')
            // let model = document.getElementById(elem);
            // model.style.display = "block";
            // model.classList.add('modal-visible');
            // model.classList.remove('modal-hidden');
            // model.setAttribute('aria-hidden', true);            
        },
        closeBtModel(elem){
            jQuery(`#${elem}`).modal('hide')
            // let model = document.getElementById(elem);
            // model.style.display = "none";
            // model.classList.remove('modal-visible');
            // model.classList.add('modal-hidden');
            // model.setAttribute('aria-hidden', false);            
        }


    }
});


Vue.prototype.$baseUrl = window.base_url;
Vue.prototype.$placeApiKey = "AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4";
Vue.prototype.$user = window.user;

Vue.component('top-header', require('./components/partials/HeaderComponent.vue').default);
Vue.component('side-navbar', require('./components/partials/SidebarComponent.vue').default);

let prod_arr = ['userproduction', 'production'];
let baseUrl;
if(prod_arr.includes(process.env.NODE_ENV))
    baseUrl = '/tapngo';
else
    baseUrl = '';

window.axios.defaults.baseURL =  baseUrl + '/api/user/';

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    // vuetify : new Vuetify()
});
