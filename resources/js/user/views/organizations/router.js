const O_IndexComponent = () => import(/* webpackChunkName: "organizations" */ './IndexComponent.vue');
const O_ShowComponent = () => import(/* webpackChunkName: "organizations-show" */ './ShowComponent.vue');


export default [
    {
        path: '/organizations',
        name: 'organizations',
        component: O_IndexComponent,
        meta: {
            title: "Organizations",
            description: ""
        }
    },     
    {
        path: '/organizations/:id',
        name: 'organizations.show',
        component: O_ShowComponent,
        meta: {
            title: "Organization Detail",
            description: ""
        }
    },     
]