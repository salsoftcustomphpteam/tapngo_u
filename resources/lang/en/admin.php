<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'post' => [
        'deleted' => "Post is deleted successfully",
    ],
    'customer' => [
        "store" => "Client is added successfully",
        "store_mail_error" => "Client is added successfully but mail is not sent, Please check the mailing credentials"
    ],
    'employee' => [
        "store" => "Employee is added successfully",
        "store_mail_error" => "Employee is added successfully but mail is not sent, Please check the mailing credentials"
    ],
    'org' => [        
        "store" => "Organization is added successfully",
        "store_mail_error" => "Organization is added successfully but mail is not sent, Please check the mailing credentials",
        "rejected" => "Organization is rejected successfully",
        "approved" => "Organization is approved successfully",
    ],
    "package" => [
        'update' => 'Packages are updated'
    ],
    "commision" => [
        'update' => 'Commisions are updated'
    ]    
];
