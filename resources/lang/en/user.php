<?php

return [
    'update' => "Profile has been updated successfully",
    'event' => [
        'store' => "Event is created successfully",
    ],
    'project' => [
        'title' => [
            'required' => "Project title field is required",
        ],
        'store'  => "Project is added successfully",
        'update' => "Project is updated successfully",
        'doc_upload' => "Document is added succesfully",
        'board' => "Project is added succesfully to board",
        'task' => [
            'store'  => "Task is added successfully",
            'update' => "Task is updated successfully",
        ]
    ],
    "follow" => "You are now following ",
    "unfollow" => "You have unfollowed ",
    "followaccepted" => "You have accepted follow request",
    "followrejected" => "You have rejected follow request",
    'post' => [
        'store' => "Post is added successfully",
        'privacy' => "Privacy is updated successfully",
        'repost' => "Post is reposted successfully",
        'deleted' => "Post is deleted successfully",
    ],
    'group' => [
        'created' => "Group is creating successfully",
        "withdraw" => "You have withdrawn the request of group",
        "accept" => "You have accepted the request of group",
        "reject" => "You have rejected the request of group",
        "undefined" => "Group not found",        
    ],
    'contact' => "You query has been submitted to support. They will get back to you soon",
    'job' => [
        'create' => "Job is created successfully",
        'update' => "Job is updated successfully",
    ]

];