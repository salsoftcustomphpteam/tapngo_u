
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{url('/')}}/">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('users/images/favicon.ico')}}" />
 
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/vendors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('users/css/app.css') }}">


    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/plugins/calendars/fullcalendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/assets/css/style.css')}}">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- <link rel="stylesheet" href="{{ asset('users/assets/css/jquery.mCustomScrollbar.css')}}" type="text/css"> -->
    <!-- <link rel="stylesheet" href="{{ asset('users/assets/css/CustomScrollbar.css')}}" type="text/css"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- END Custom CSS-->





    <style>
    body {
        font-family: 'Poppins' !important;
    }
    </style>
</head>
<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click"
    data-menu="vertical-menu" data-col="2-columns">
    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light u-nav-bg navbar-border">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a
                            class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                class="ft-menu font-large-1"></i></a>
                    </li>
                    <li class="nav-item"> <a class="navbar-brand" href="dashboard"> <img
                                class="brand-logo img-fluid" alt="stack admin logo" src="{{asset('users/images/new-one.png')}}"> </a> </li>
                    <li class="nav-item d-md-none"> <a class="nav-link open-navbar-container" data-toggle="collapse"
                            data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a> </li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <!-- <li class="nav-item d-md-block">
							<a class="nav-link nav-menu-main wallet-nav" href="my-wallet.php"><i class="fa fa-money-bill"></i> $100</a>
						</li> -->

                        <li class="dropdown dropdown-notification nav-item">
                         <a class="nav-link nav-link-label" href="#"
                                data-toggle="dropdown"><i class="ficon ft-bell"></i> 
                                <!-- <span
                                    class="badge badge-pill badge-default badge-danger badge-default badge-up">5</span> -->
                            </a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <h6 class="dropdown-header m-0 text-center"> <span
                                            class="grey darken-2 ">Notifications</span> </h6>
                                </li>
                                <!-- <li class="scrollable-container media-list ps-container ps-theme-dark ps-active-y"
                                    data-ps-id="75e644f2-605d-3ecc-f100-72d86e4a64d8">
                                    <a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left align-self-center">
                                            </div>
                                            <div class="media-body">
                                                <span class="blck2">You have upcoming</span> <span class="media-heading">ride mm/dd/yy
                                                    </span><br>
													<div class="text-center pt-1">
													<span class="media-heading">10:30 PM</span>
													</div>
                                            </div>
                                        </div>
                                    </a>

                                </li> -->
                                <li>
                                <span class="blck2">No New Notifications</span></li>

                                <li class="dropdown-menu-footer"><a class="dropdown-item oornge-txt2 text-right"
                                        href="javascript:;">View All</a>
                                </li>

                            </ul>
                        </li>
                        <li class="dropdown dropdown-user nav-item"> <a
                                class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <span class="avatar avatar-online"> <img
                                        src="{{asset('users/app-assets/images/portrait/small/avatar-s-1.png')}}" alt="avatar"> </span>
                                <span class="user-name"></span> </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="edit-profile"><i class="fa  fa-user"></i> view
                                    Profile </a>
                                <a class="dropdown-item" href="complaint"><i
                                        class="fa fa-user-circle"></i>Complaint </a>
                                <a class="dropdown-item" href="logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
                            </div>
                        </li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
                                href="#"><i class="ft-menu"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
     <!-- ////////////////////////////////////////////////////////////////////////////-->
     <div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item active "><a href=""><i class="fa fa-home"></i><span class="menu-title"
                            data-i18n="">HOME</span></a>
                </li>

                <li class="nav-item  "><a href="ride-book"><i class="fas fa-car"></i>
                        <span class="menu-title " data-i18n="">BOOK RIDE</span></a>

                </li>
                <li class="nav-item  "><a href="my-rides"><i class="fas fa-car"></i>
                        <span class="menu-title " data-i18n="">MY RIDES</span></a>

                </li>
                <li class="nav-item  "><a href="wallet"><i class="fas fa-car"></i>
                        <span class="menu-title " data-i18n="">WALLET</span></a>

                </li>
                <li class="nav-item  "><a href="refferal"><i class="fas fa-car"></i>
                        <span class="menu-title " data-i18n="">REFERRAL CODE</span></a>

                </li>

            </ul>
        </div>
    </div>


<script type="text/javascript">
        window.base_url = "{{ url('/') }}";
        window.user = @json(auth()->user());
        window.appname = "{{config('app.name')}}";
             
    </script>

    


  


    <div id="app">

    <app/>

</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places"></script>

<script src="{{ asset('users/js/app.js') }}"></script>
@if(auth()->check())
<script uid="{{auth()->user()->uuid}}" src="https://dev28.onlinetestingserver.com/soachatcentralizedWeb/js/ocs.js"></script>
@endif
<script src="https://cdn.socket.io/socket.io-1.0.0.js"></script>



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

    <script src="{{ asset('users/app-assets/vendors/js/vendors.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/select/form-select2.js')}}" type="text/javascript"></script>
    <script src="{{ asset('users/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> 
    <script src="{{ asset('users/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{ asset('users/app-assets/vendors/js/extensions/moment.min.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script> 
    <script src="{{ asset('users/app-assets/js/scripts/modal/components-modal.js')}}"></script>
    <script src="{{ asset('assets/js/function.js" type="text/javascript')}}"></script> 


    <!-- <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> -->
    <!-- <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script> -->
    <!-- <script src="{{ asset('users/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script> -->
    <!-- <script src="{{ asset('users/app-assets/js/scripts/forms/form-repeater.js')}}"></script> -->

    <!-- <script src="{{ asset('users/app-assets/vendors/js/charts/echarts/echarts.js')}}"></script> -->
    <!-- <script src="{{ asset('users/assets/js/jquery.exzoom.js')}}"></script> -->
    <!-- <script src="{{ asset('users/assets/js/function.js')}}"></script> -->
    <!-- <script src="{{ asset('users/assets/js/chart.js')}}"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> -->

    <!-- dashboard -->
  
    <!-- <script src="{{ asset('assets/js/dash-chart.js')}}"></script> -->



    <script>
    if(!user){
       window.location.href = 'login';
    }
   $('.avatar-online').click(function(){
    // $('.dropdown-menu-right').toggle();
   });
    </script>

</body>

</html>

