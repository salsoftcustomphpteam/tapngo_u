<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>{{ config('app.name') }} - Reset Password</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/assets/css/responsive.css') }}">
</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <section class="register forget-pass">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-block d-none col-12"><img src="{{ asset('admins/images/login-left.png') }}" class="login-image"></div>
                <div class="col-lg-6 col-12">
                    <div class="register-main">

                        <form action="{{ route('admins.password.update') }}" method="POST">
                            <input type="hidden" name="token" value="{{ $token }}">
                            @csrf
                            <img src="{{ asset('admins/images/login-logo.png') }}" class="img-fluid" alt="logo">
                            <div class="form-main">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Password Recovery</h2>
                                    </div>
                                </div>

                                <!--fields start here-->

                                <div class="fields">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <i class="fa fa-key"></i>
                                            <input name="email" type="hidden" class="form-control @error('email') is-invalid @enderror" placeholder="Enter Email" value="{{ $email ?? old('email') }}">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <i class="fa fa-key"></i>
                                            <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter New Password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-12">
                                            <i class="fa fa-key"></i>
                                            <input name="password_confirmation" type="password" class="form-control" placeholder="Retype Password">
                                        </div>

                                    </div>
                                    <button type="submit" class="form-control">Update</button>

                                    <div class="new-user">
                                        <p>OR</p>
                                        <a href="{{ route('admins.login') }}" class="login form-control back"><i class="fa fa-long-arrow-left"></i>back to login</a>
                                    </div>
                                </div>


                                <!--fields end here-->

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
    <script>
        function togglePassword(handler){
            let elem = $('#password');
            if('password' == $(elem).attr('type')){
                $(elem).prop('type', 'text');
                handler.innerHTML = '<i class="fa fa-eye-slash"></i>';
            }else{
                $(elem).prop('type', 'password');
                handler.innerHTML = '<i class="fa fa-lock"></i>';
            }        
        }
    </script>
    </body>
    </html>
    