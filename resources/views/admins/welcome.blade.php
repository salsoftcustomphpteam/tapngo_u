<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset("app-assets/css/vendors.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admins/css/app.css') }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <style>
        .loading-indicator:before{content:'';background:#000000cc;position:fixed;width:100%;height:100%;top:0;left:0;z-index:10000000000}.loading-indicator:after{content:'Loading ...';position:fixed;width:100%;top:50%;left:0;z-index:10001;color:#fff;text-align:center;font-weight:700;font-size:1.5rem}.approve .dg-content:before{background:url("{{asset('/images/add-cat-popup.png')}}") no-repeat center !important}.reject .dg-content:before{background:url("{{asset('/images/block.png')}}") no-repeat center !important}.dg-content:before{background:url("{{asset('/images/block.png')}}") no-repeat center !important;}
        .owl-nav{
                display: flex;
                flex-direction: row-reverse;
                justify-content: center;
        }

    </style>
    <script type="text/javascript">
        window.base_url = "{{ url('/') }}";
        window.user = @json(auth()->guard('admin')->user());
        window.appname = "{{config('app.name')}}";
    </script>
</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar @yield('body-class')">

<div id="app">

    <app/>

</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places"></script>

<script src="{{ asset('admins/js/app.js') }}"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}" type="text/javascript"></script>
<script src="http://dev71.onlinetestingserver.com/chat_janus_v1/js/ocs.js"></script>
{{--<script src="{{ asset('assets/js/owl.carousel.min.js') }}" type="text/javascript"></script>--}}
<script>
    window.onload = () => {
        const elem = document.getElementsByClassName('header_drop_down_login')[0];
        // If the element is visible, hide it
        if (window.getComputedStyle(elem).display === 'block') {
            hide(elem);
            return;
        }
        // Otherwise, show it
        show(elem);
    }
</script>
</body>
</html>
