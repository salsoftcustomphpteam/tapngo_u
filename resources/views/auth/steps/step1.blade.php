<div id="step1" class="row sign-up-line">
    <div class="col-lg-7">
       
            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group">
                        <label>First name <span>*</span></label>
                        <input required name="first_name" type="text" class="new-input">
                    </div>
                </div>
                <!--col end-->
                <div class="col-lg-5">
                    <div class="form-group">
                        <label>Last name <span>*</span></label>
                        <input required name="last_name" type="text" class="new-input">
                    </div>
                </div>
                <!--col end-->

            </div>

       

            <div class="row">
                <div class="col-lg-7">
                    <div class="form-group">
                        <label>Password <span>*</span></label>
                        <input required id="password" name="password" type="password" class="new-input">
                        <span style="cursor: pointer" class="pass-eye-icon"><i  onclick="togglePassword(this, '#password')" class="fas fa-eye-slash"></i></span>
                    </div>
                </div>
                <!--col end-->
                <div class="col-lg-5">
                    <div class="form-group">
                        <label>Password Confirmation <span>*</span></label>
                        <input required id="password_confirmation" name="password_confirmation" type="password" class="new-input">
                        <span style="cursor: pointer" class="pass-eye-icon"><i  onclick="togglePassword(this, '#password_confirmation')" class="fas fa-eye-slash"></i></span>
                    </div>
                </div>
                <div class="col-lg-12">
                <div class="form-group">
                    <label>Phone Number <span>*</span></label>
                    <input required name="phone" min="9" type="number" id="number-mask2" class="new-input">
                </div>
            </div>
                <!--col end-->

                <div class="col-lg-12">
                <div class="form-group">
                    <label>Email Address<span>*</span></label>
                    <input required type="email" name="email" class="new-input">
                </div>
            </div>
            <!--col end-->

            <div class="col-lg-12">
                <div class="form-group">
                    <label>Recovery Email Address<span>*</span></label>
                    <input required name="recovery_email" type="email" class="new-input">
                </div>
                <p class="signup-rec-p">This email address will help you in case of lost or forgot your
                    primary email</p>
            </div>

            </div>            

       
        <!--signup line end-->



    </div>
    <!--left col end-->


    <div class="col-lg-5">
        <h6>Postal Address</h6>
        <div class="row">
        <div class="col-lg-12">
                  
                    <div class="form-group">
                        <label>Address line  <span>*</span></label>
                        <input required name="address" type="text" class="new-input">
                    </div>
                </div>
                
            <div class="col-lg-6">
                <div class="form-group">
                    <label>City <span>*</span></label>
                    <input required name="city" type="text" class="new-input">
                </div>
            </div>
            <!--col end-->
            <div class="col-lg-6">
                <div class="form-group">
                    <label>State <span>*</span></label>
                    <input required name="state" type="text" class="new-input">
                </div>
            </div>
            <!--col end-->

        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Zip Code <span>*</span></label>
                    <input required name="zip" type="text" class="new-input">
                </div>
            </div>
            <!--col end-->
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Country<span>*</span></label>
                    <input required name="country" type="text" class="new-input">
                </div>
            </div>
            <!--col end-->

           
            <!--col end-->

            
        </div>

    </div>
    <!--right col end-->
</div>
<!--main row start-->
