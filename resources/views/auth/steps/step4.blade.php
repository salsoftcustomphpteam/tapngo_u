<div id="step4">
    <div class="signup-text-main">
        <h3>Almost you there..
            <span>Card Detail</span>
        </h3>

    </div>
    <div class="row sign-up-line">
        <div class="col-lg-7">
            <div class="sign-up-line">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Cardholder Name <span>*</span></label>
                            <input name="card_name" required type="text" class="new-input">
                        </div>
                    </div>
                    <!--col end-->
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Card Number <span>*</span></label>
                            <input data-inputmask="'mask': '9999-9999-9999-9999'" name="card_number" required type="text" class="new-input">
                        </div>
                    </div>
                    <!--col end-->

                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>CVV Code <span>*</span></label>
                            <input data-inputmask="'mask': '999'"  name="cvv" required type="text" class="new-input">
                        </div>
                    </div>
                    <!--col end-->
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Expiry Month <span>*</span></label>
                            <input data-inputmask="'mask': '99'"  name="expiry_month" required type="text" class="new-input">
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Expiry Year <span>*</span></label>
                            <input data-inputmask="'mask': '9999'"  name="expiry_year" required type="text" class="new-input">
                        </div>
                    </div>
                    <!--col end-->

                </div>

            </div>
            <!--signup line end-->

        </div>
        <!--left col end-->
        <div class="col-lg-5">
            
        </div>

    </div>
</div>