<div id="step3">
    <div class="signup-text-main">
        <h3>Which Plan Suit you...
            <span>Choose your Plan.</span>
        </h3>
        <p>We may use security questions to verify you identity when you sign in to your account or if you need to reset
            your password.
            <br>Please select security questions from the dropdown menus.
        </p>
    </div>
    <div class="sign-up-line pb-5">

        <div class="row">

            @foreach(getPackages(0) as $package)
                <div class="col-lg-4">
                    <div class="signup-packge">
                        <h3>{{ $package->name }}</h3>
                        <h4>{{ $package->description }}</h4>
                        <h5>$ {{ number_format($package->amount) }} <span>/{{$package->month}} MONTHS</span></h5>
                        <i class="fas fa-chevron-circle-down"></i>
                        <p>It is a long established fact that
                            A reader will be distracted
                            Readable content of a page</p>
                        <button onclick="toNext(); currentPackage = {{$package->id}}" class="main-blue-btn">Select</button>
                    </div>

                </div>

            @endforeach

        </div>
        <!--row end-->




    </div>

</div>