<li class="nav-item active">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('about-us')}}">About Us</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('contact-us')}}">Contact Us</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">Sign In</a>
  </li>
  @if($join)
  <li class="nav-item join-cta">
    <a href="{{ route('signup.as') }}" class="nav-link">Join</a>
  </li>
  @endif
