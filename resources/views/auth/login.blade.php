
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{url('/')}}/">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('users/css/app.css') }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('users/images/favicon.ico')}}" />
 
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/css/plugins/calendars/fullcalendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/vendors/css/calendars/fullcalendar.min.css')}}">
    <!-- BEGIN Custom CSS-->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('users/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('users/assets/css/jquery.mCustomScrollbar.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('users/assets/css/CustomScrollbar.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('users/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- END Custom CSS-->

    <style>
    body {
        font-family: 'Poppins' !important;
    }
    </style>
</head>

<body>

<section class="login-main">
        <div class="container">
            <div class="login-inner">
                <div class="row ">
                    <div class="col-lg-6 col-12 p-0">
                        <div class="left"><img src="{{asset('users/images/login-left.png')}}" class="img-fluid" alt="">
                            <h5 class="accnt-sig-up">Don't Have an Account? <span><a href="{{ route('register') }}" class="sig-up">Sign Up</a></span></h5>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 ">
                        <a href="login" class="bcktologin"><i class="fas fa-chevron-left"></i>Back To Login</a>
                        <div class="right">
                            <!-- <img src="images/logo-login.png" class="login-logo" alt=""> -->
                            <h1>Login</h1>
                            <form action="{{ route('customlogin') }}" method="post">
                            @csrf
                            @if (session('status'))
                            <div class="row">
                              <div class="col">
                                <div class="alert alert-success" role="alert">
                                  {{ session('status') }}
                                </div>
                              </div>
                            </div>
                            @endif

                                <div class="row">
                                    <div class="col-12 form-group mb-2">
                                        <label for="exampleInputEmail1">Email Address</label>
                                        <input name = "email" type="email" class="form-control" placeholder="Enter Email Address">
                                        @error('email')
              <p class="m-1 red">{{ $message }} </i>
            @enderror
                                    </div>
                                    <div class="col-12 form-group position-relative mb-2">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input name ="password" type="password" class="form-control" placeholder="Password">
                                        @error('password')
              <p class="m-1 red">{{ $message }} </i>
            @enderror
                                    </div>
                                </div>

                                @if (session('error'))
          <div class="row">
            <div class="col">
              <div class="alert alert-danger" role="alert">
                {{ session('error') }}
              </div>
            </div>
          </div>
          @endif
                                <div class="row">
                                    <div class="col-12">
                                        <div class="d-flex justify-content-end mob_l">
                                            <div class=""><a href="#" class="" data-toggle="modal"
                                                    data-target="#exampleModalCenter"> Forgot Password?</a> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2 mt-md-2 ">
                                    <div class="col-12">
                                        <button type="sumit" class="yel-btn"> login</button>
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex align-items-baseline justify-content-between py-2">
                                <div>
                                    <img src="images/for-line.png" class="img-fluid" alt="">
                                </div>
                                <div>
                                    <h6>OR</h6>
                                </div>
                                <div>
                                    <img src="images/for-line.png" class="img-fluid" alt="">
                                </div>
                            </div>

                            <div class="text-center">
                                <a href="#_"><img src="images/fb.png" alt=""></a>
                                <a href="#_"><img src="images/a-logo.png" alt=""></a>
                                <a href="#_"><img src="images/logo-g.png" alt=""></a>
                            </div>
                            <!--login modal start here-->

                            <!-- Modal -->
                            <div class="modal fade modal-1" id="exampleModalCenter" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="forget-pass">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span> </button>
                                            <div class="modal-body">
                                                <h1>Password Recovery</h1>
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <input type="text" placeholder="Enter Email Address"
                                                                class="form-control">
                                                            <i class="fa fa-envelope"></i>
                                                        </div>
                                                        <div class="col-12">
                                                            <button type="button" class="form-control yel-btn" id="cont"
                                                                data-dismiss="modal" aria-label="Close"
                                                                data-toggle="modal" data-target=".modal-2"> Continue
                                                            </button>
                                                        </div>

                                                        <a href="#" class="bcktologin modala" data-dismiss="modal"
                                                            aria-label="Close"> <img src="images/right-arrow.png"
                                                                alt=""> back to login</a>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--login modal end here-->

                            <!--forgot step 2 start here-->

                            <!-- Button trigger modal -->

                            <!-- Modal -->
                            <div class="modal fade modal-2" id="exampleModal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="forget-pass">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span> </button>
                                            <div class="modal-body">
                                                <h1>Password Recovery</h1>
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <input type="number" placeholder="Enter verification code"
                                                                class="form-control">
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </div>
                                                        <div class="col-12 text-right">
                                                            <button type="button" class="code-txt">Resend Code </button>
                                                        </div>
                                                        <div class="col-12">
                                                            <button type="button" class="form-control yel-btn"
                                                                id="count-2" data-dismiss="modal" aria-label="Close"
                                                                data-toggle="modal" data-target=".modal-3"> Continue
                                                            </button>
                                                        </div>

                                                        <a href="#" class="bcktologin modala" data-dismiss="modal"
                                                            aria-label="Close"> <img src="images/right-arrow.png"
                                                                alt=""> back to login</a>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade modal-3" id="password-modal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="forget-pass">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span> </button>
                                            <div class="modal-body">
                                                <h1>Password Recovery</h1>
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <input type="password" placeholder="Enter New Password"
                                                                class="form-control">
                                                            <i class="fa fa-lock" aria-hidden="true"></i>
                                                            <button type="button" class="view-btn position-absolute"><i
                                                                    class="fa fa-eye"></i> </button>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <input type="password" placeholder="Retype Password"
                                                                class="form-control">
                                                            <i class="fa fa-lock" aria-hidden="true"></i>
                                                            <button type="button" class="view-btn position-absolute"><i
                                                                    class="fa fa-eye"></i> </button>
                                                        </div>
                                                        <div class="col-12">
                                                            <button type="button" class="form-control yel-btn"
                                                                data-dismiss="modal" aria-label="Close"> Update
                                                            </button>
                                                        </div>

                                                        <a href="#" class="bcktologin modala" data-dismiss="modal"
                                                            aria-label="Close"> <img src="images/right-arrow.png"
                                                                alt=""> back to login</a>

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--forgot step 2 end here-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <script src="{{ asset('users/app-assets/vendors/js/vendors.min.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
    <script src="{{ asset('users/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/scripts/forms/form-repeater.js')}}"></script>
    <script src="{{ asset('users/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{ asset('users/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"></script>
    <script src="{{ asset('users/app-assets/vendors/js/charts/echarts/echarts.js')}}"></script>
    <script src="{{ asset('users/app-assets/vendors/js/extensions/moment.min.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/core/app.js')}}"></script>
    <script src="{{ asset('users/assets/js/jquery.exzoom.js')}}"></script>
    <script src="{{ asset('users/assets/js/function.js')}}"></script>
    <script src="{{ asset('users/assets/js/chart.js')}}"></script>
    <script src="{{ asset('users/app-assets/js/scripts/modal/components-modal.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

  <script>
    function togglePassword(handler, element = '#password') {
      let elem = $(element);
      if ('password' == $(elem).attr('type')) {
        $(elem).prop('type', 'text');
        handler.classList.add("fa-eye");
        handler.classList.remove("fa-eye-slash");
      } else {
        $(elem).prop('type', 'password');
        handler.classList.add("fa-eye-slash");
        handler.classList.remove("fa-eye");
      }
    }
  </script>




</body>

</html>
