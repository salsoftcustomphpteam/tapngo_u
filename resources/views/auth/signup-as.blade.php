<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('users/css/owl.carousel.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <link rel="stylesheet" href="{{ asset('users/css/style.css') }}">
  <title>{{ config('app.name') }} - Login</title>
</head>

<body>
  <section class="navBar inner-nav">
    <div class="container-custom">
      <div class="row">
        <div class="col-12">
          <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
            <a class="navbar-brand" href="#">
              <img src="{{ asset('users/images/logo.png') }}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto">
                @include('auth.links', ['join' => true])
              </ul>

            </div>
          </nav>
        </div>
      </div>
    </div>
  </section>

  <section class="sign-up-main">
    <div class="container">
      <h2 class="heading-h2">Sign up as a...</h2>
      <ul>
        <li><a href="{{ route('register') }}"><i class="fas fa-thumbs-up"></i>Customer</a></li>
        <li><a href="{{ route('business.register') }}"><i class="fas fa-hotel"></i>Business Organization</a></li>
      </ul>


    </div>
    <!--container end-->
  </section>


  <footer class="container-fluid">
    <div class="top">
      <div class="footer-brand">
        <img src="images/logo.png" alt="">
      </div>
      <div class="links">
        <ul class="">
          @include('auth.links', ['join' => false])
        </ul>
      </div>
      <div class="socials">
        <ul>
          <li>
            <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="6.667" height="13.332"
                viewBox="0 0 6.667 13.332">
                <path id="facebook_1_" data-name="facebook (1)"
                  d="M11.637,2.214h1.217V.094A15.716,15.716,0,0,0,11.08,0,2.815,2.815,0,0,0,8.123,3.133V5H6.187v2.37H8.123v5.963H10.5V7.37h1.858L12.651,5H10.5V3.367c0-.685.185-1.154,1.139-1.154Z"
                  transform="translate(-6.187)" fill="#364b87" />
              </svg>
            </a>
          </li>
          <li>
            <a href="#">
              <svg id="instagram_1_" data-name="instagram (1)" xmlns="http://www.w3.org/2000/svg" width="13.332"
                height="13.332" viewBox="0 0 13.332 13.332">
                <path id="Path_109" data-name="Path 109"
                  d="M213.343,212.172A1.172,1.172,0,1,1,212.172,211,1.172,1.172,0,0,1,213.343,212.172Zm0,0"
                  transform="translate(-205.506 -205.506)" fill="#364b87" />
                <path id="Path_110" data-name="Path 110"
                  d="M125.52,120h-3.958A1.564,1.564,0,0,0,120,121.562v3.958a1.564,1.564,0,0,0,1.562,1.562h3.958a1.564,1.564,0,0,0,1.562-1.562v-3.958A1.564,1.564,0,0,0,125.52,120Zm-1.979,5.494a1.953,1.953,0,1,1,1.953-1.953A1.955,1.955,0,0,1,123.541,125.494Zm2.239-3.8a.391.391,0,1,1,.391-.391A.391.391,0,0,1,125.78,121.692Zm0,0"
                  transform="translate(-116.875 -116.875)" fill="#364b87" />
                <path id="Path_111" data-name="Path 111"
                  d="M9.816,0h-6.3A3.519,3.519,0,0,0,0,3.515v6.3a3.519,3.519,0,0,0,3.515,3.515h6.3a3.519,3.519,0,0,0,3.515-3.515v-6.3A3.519,3.519,0,0,0,9.816,0Zm1.172,8.645a2.346,2.346,0,0,1-2.343,2.343H4.687A2.346,2.346,0,0,1,2.343,8.645V4.687A2.346,2.346,0,0,1,4.687,2.343H8.645a2.346,2.346,0,0,1,2.343,2.343Zm0,0"
                  fill="#364b87" />
              </svg>
            </a>
          </li>
          <li>
            <a href="#">
              <svg xmlns="http://www.w3.org/2000/svg" width="14.667" height="12.275" viewBox="0 0 14.667 12.275">
                <path id="twitter_1_" data-name="twitter (1)"
                  d="M14.667,2l-1.324-.174L13.689.533l-1.55.415A3.223,3.223,0,0,0,7.276,3.723v.418A7.724,7.724,0,0,1,1.623,1.212L1.263.758l-.33.477a3.255,3.255,0,0,0-.329.6A3.208,3.208,0,0,0,.464,3.9a3.263,3.263,0,0,0,.544,1.121L.5,5.146l.1.418A3.229,3.229,0,0,0,2.725,7.881l-.5.588.327.279A3.218,3.218,0,0,0,4.284,9.5,6.492,6.492,0,0,1,0,11.109l.185.128A8.623,8.623,0,0,0,11.2,10.249a8.566,8.566,0,0,0,2.525-6.1v-.43a3.257,3.257,0,0,0-.032-.453Zm0,0"
                  transform="translate(-0.001 -0.5)" fill="#364b87" />
              </svg>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="bot">
      <p class="copyright">
        Copyright © 2021 Conekpro All rights reserved.
      </p>
    </div>
  </footer>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
  </script>
</body>

</html>

<script>
  function togglePassword(handler) {
    let elem = $('#password');
    if ('password' == $(elem).attr('type')) {
      $(elem).prop('type', 'text');
      handler.classList.add("fa-eye");
      handler.classList.remove("fa-eye-slash");
    } else {
      $(elem).prop('type', 'password');
      handler.classList.add("fa-eye-slash");
      handler.classList.remove("fa-eye");
    }
  }
</script>