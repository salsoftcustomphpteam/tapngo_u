<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "apply_before" => "required",
            "category" => "required",
            "city" => "required",
            "company_name" => "required",
            "country" => "required",
            "degree_requirment" => "required",
            "description" => "required",
            "experience" => "required",
            "hours_of_work" => "required",
            "period_of_employment" => "required",
            "salary_range" => "required",
            "skills" => "required",
            "title" => "required",            
            "company_name" => "required",
        ];
    }
}
