<?php

namespace App\Http\Controllers;

use App\Core\Filters\ProjectFilters;
use App\Http\Controllers\Controller;
use App\Models\Keyword;
use App\Models\Project;
use App\Models\ProjectBids;
use App\Models\ProjectJob;
use App\Models\ProjectRequest;
use App\Models\Task;
use App\Organization;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class ProjectBidController extends Controller
{

    public function outgoing(Request $request){
        $outgoing = ProjectRequest::whereUserId($request->user()->id)->with('requestable', 'project');
        
        if($request->query('id'))
            $outgoing->whereProjectId($request->query('id'));
        else
            $outgoing->groupBy('project_id');

        return $outgoing->get();
    }

    public function incomming(Request $request){

        $incomming = ProjectBids::with([ 'project' => function($q){
            $q->withCount('bids');
        }, 'job', 'bidable']);

        if($request->query('id'))
            $incomming->whereIn('project_id', [$request->query('id')]);
        else
            $incomming->whereIn('project_id', $request->user()->creatable()->pluck('id'))->groupBy('project_id');

        return $incomming->get();        
    }


    public function show($id){
        $bid = ProjectBids::with(['project', 'job', 'bidable'])->find($id);       
        
        return $bid;
    }


    public function acceptBid(ProjectBids $bid){
        $bid->status = 1;
        $bid->save();
        return $this->responseSuccess("Bid Status has been accepted successfully");
    }

    public function rejectBid(ProjectBids $bid){
        $bid->status = 2;
        $bid->reason = request()->query('text');
        $bid->save();
        return $this->responseSuccess("Bid Status has been rejected successfully");

    }



}