<?php

namespace App\Http\Controllers;

use App\Core\Http\Resources\EventResource;
use App\Organization;
use App\User;
use App\Models\Event as CalenderEvent;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

use App\CustomNotification;
use App\Models\Interaction;
use App\Models\Group;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use App\Organizaiton;


class EventController extends Controller
{    


    public function index(Request $request){
        $user = $request->user();

        // $events = $user->eventable()->with('participants.participantable')->get();
        $userId = $user->id;

        $events = $user->eventable()->with('participants.participantable')->get();
        $myEvents = array();
        $i = 0;
        if(count($events)){
            foreach($events as $event){
                $myEvents[$i]= $event->id;
                $i++;
            }
        }
        $participants = CalenderEvent::with(['participants'=>function($query) use($userId){
            $query->where('participantable_id', $userId);
        }])->get();
        $invited = array();
        if(count($participants)){
            $j=0;
            foreach($participants as $participant){
                if(!count($participant->participants)){
                    $j++;
                    continue;
                }
               
                foreach($participant->participants as $parts){
                    $invited[$j]=$parts->event_id;
                $j++;
                }               
            }
        }
        $events = CalenderEvent::with('participants.participantable')->where(function($query) use($invited, $myEvents){
            $query->where(function($query2) use($invited, $myEvents){
                    $query2->whereIn('id', $invited);
                })->orWhere(function($query3) use($invited, $myEvents){
                    $query3->whereIn('id', $myEvents);
                });
        })->get();

        return EventResource::collection($events);
    }


    /**
     * loopUpAllUsers
     *
     * @param  mixed $request
     * @return void
     */
    public function loopUpAllUsers(Request $request){
        $user = Auth()->user();
        $followings = $user->getAllFriendships()->where('status', "accepted");
        $usersArr = array();
        $orgArr = array();
        if(count($followings)){
            $i=0;
            $j=0;
            foreach($followings as $follow){
                if(($follow->sender_type == 'App\\Organization'  || $follow->sender_type == 'App\Organization' ) && $follow->sender_id == $user->id){   
                    if(($follow->recipient_type == 'App\\Organization'  || $follow->recipient_type == 'App\Organization' )){
                        $orgArr[$j]=$follow->recipient_id;
                        $j++;
                    }
                    else{
                        $usersArr[$i]=$follow->recipient_id;
                        $i++;
                    }
                }
                if(($follow->sender_type == 'App\\User'  || $follow->sender_type == 'App\User' ) && $follow->sender_id == $user->id){   
                    if(($follow->recipient_type == 'App\\Organization'  || $follow->recipient_type == 'App\Organization' )){
                        $orgArr[$j]=$follow->recipient_id;
                        $j++;
                    }
                    else{
                        $usersArr[$i]=$follow->recipient_id;
                        $i++;
                    }
                }

                if(($follow->recipient_type == 'App\\Organization'  || $follow->recipient_type == 'App\Organization' ) && $follow->recipient_id == $user->id){   
                    if(($follow->sender_type == 'App\\Organization'  || $follow->sender_type == 'App\Organization' )){
                        $orgArr[$j]=$follow->sender_id;
                        $j++;
                    }
                    else{
                        $usersArr[$i]=$follow->sender_id;
                        $i++;
                    }
                }

                if(($follow->recipient_type == 'App\\User'  || $follow->recipient_type == 'App\User' ) && $follow->recipient_id == $user->id){   
                    if(($follow->sender_type == 'App\\Organization'  || $follow->sender_type == 'App\Organization' )){
                        $orgArr[$j]=$follow->sender_id;
                        $j++;
                    }
                    else{
                        $usersArr[$i]=$follow->sender_id;
                        $i++;
                    }
                }

               
            }
        }

        $users = User::where('id', '!=', $request->user()->id)->whereIn('id', $usersArr)->get();
        $organizations = Organization::whereIn('id', $orgArr)->get();
        $peoples = [];
        foreach($users as $user){
            array_push($peoples, [ 
                "type" => get_class($user),
                "id" => $user->id,
                "name" => $user->name,
                "email" => $user->email,
                "avatar" => $user->image 
            ]);
        }
        foreach($organizations as $org){
            array_push($peoples, [ 
                "type" => get_class($org),
                "id" => $org->id ,
                "name" => $org->name." (Org)",
                "email" => $org->email,
                "avatar" => $org->image 
            ]);
        }
        return $peoples;
    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request){
        
        $event = $request->e;
        $authUser =  auth()->user();
        $event = $request->e;
        if(!$event){
            return array('error'=> 'error');
        }

        $request->validate([
            'e.data.title' => 'required',
            // 'e.data.description' => 'required',
            'e.schedule' => 'required',
            

        ]);

        $user = $request->user();   
        try{
            $date =  $event['schedule']['year'][0]."-".$event['schedule']['month'][0]."-".$event['schedule']['dayOfMonth'][0];
        }
        catch(Exception $e){
            $date = null;
        }

        // $calCheck = CalenderEvent::where(["title" => $event["data"]["title"],
        // "description" => $event["data"]["description"]])->first();
        
        // if($calCheck){
        //     return array('error'=> 'error');
        // }

        //check if edit clicked
        if (array_key_exists("id",$request->e??array())){

            $event = CalenderEvent::where('id', $request->e['id'])->update([            
                "title" => $event["data"]["title"],
                "description" => $event["data"]["description"],
                "event_date" => $date,
                "event_time" => $event["data"]["calendar"]??'00',
                "payload" => $event
            ]);
            return $this->responseSuccess(Lang::get('user.event.store'));
        }

        $request->validate([
            'e.data.calendar' => 'required',
        ]);

        $event = $user->eventable()->save(new CalenderEvent([            
            "title" => $event["data"]["title"],
            "description" => $event["data"]["description"],
            "event_date" => $date,
            "event_time" => $event["data"]["calendar"]??'00',
            "payload" => $event
        ]));

        foreach($request->user as $u){
            $participant = app()->make($u['type'])->find($u['id']);
            $event->addParticipant(0, $participant);

            if($u['type'] == 'App\User' || $u['type'] == 'App\\User'){
                $url = '/calendar';
                
            }else{
               $url = '/business/calendar';
            }

            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['sender_id'] = $authUser->id;
            $dataToAdd['recipient_id'] = $u['id'];
            $dataToAdd['sender_type'] = 'App\User';
            $dataToAdd['recipient_type'] =  $u['type'];
            $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
            $dataToAdd['body'] = 'has invited you to his event, See calender.';
            $dataToAdd['url'] = $url;
            $dataToAdd['status'] = 1;
            $dataToAdd['created_at'] = $currentDateAndTime;
            $dataToAdd['updated_at'] = $currentDateAndTime;
            CustomNotification::create($dataToAdd);
            //send twilio sms
            if($u['type'] == 'App\User' || $u['type'] == 'App\\User'){
                $user = User::find($u['id']);
                
            }else{
                $user = Organization::find($u['id']);
            }
            $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');


            // sendNotification([
            //     "subject" => "Event Participation",
            //     "name" => $participant->name,
            //     "type" => "calender_event_add_participant",
            //     "text" => "You have been invited to be part of an event",
            //     "user" => $participant
            // ]);
        }

        return $this->responseSuccess(Lang::get('user.event.store'));

    }
    private function sendMessage($message, $recipients)
    {

        try {
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);
        } catch (RestException $e) {
        }

    }

}
