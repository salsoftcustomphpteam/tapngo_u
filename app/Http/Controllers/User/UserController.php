<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserComplaint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Multicaret\Acquaintances\Status;
use Spatie\Searchable\ModelSearchAspect;
use Spatie\Searchable\Search;
use Newsletter;
use Mail;
use Illuminate\Support\Facades\Auth;
use App\Core\Chat\Soachat;

class UserController extends Controller
{

    /**
     * follow
     *
     * @var array
     */
    protected $follow = [
        'pending' => 0,
        'approved' => 1,
    ];
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            "firstname" => ['required'],
            "lastname" => ['required'],
            "address" => ['required'],
            "phone_number" => ['required', 'numeric', 'min:9'],
            "city" => ['required'],
            "state" => ['required'],
            // "zip" => ['required'],
            "country" => ['required'],
            // 'recovery_email' => ['required', 'string', 'email', 'max:255'],
            "gender" => ['required'],
        ]);
    }
    public function show(Request $request, User $user){

        $user->isFriendWith = $request->user()->isFriendWith($user);
        $user->hasSentFriendRequestTo = $request->user()->hasSentFriendRequestTo($user);

        return $user;

    }

    public function userDetail(Request $request, $id){
        $user = User::find($id);
        $user->isFriendWith = $request->user()->isFriendWith($user);
        $user->hasSentFriendRequestTo = $request->user()->hasSentFriendRequestTo($user);
        return $user;

    }
    public function changePassword(Request $request){
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'old_password' => ['required'],
        ]);

        extract($request->all());

        $user = $request->user();

        if(Hash::check($old_password, $user->password)){
            $user->password = Hash::make($password);
            $user->save();

            return $this->responseSuccess(Lang::get("passwords.reset"));
        }

        return $this->responseWithError(['password' => Lang::get("validation.password_match")]);
    }


    public function updateProfile(Request $request){
        $authUser = auth()->user();
        $this->validator($request->all())->validate();
        $user               = User::find($authUser->id);
        $user->firstname    = $request->firstname;
        $user->lastname     = $request->lastname;
        $user->gender       = $request->gender;
        $user->address      = $request->address;
        $user->city         = $request->city;
        $user->state        = $request->state;
        $user->country      = $request->country;

        // $user->fill(collect($request->all())->toArray());
        if($request->input('image')){
	       
            $data = $request->input('image');
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imageName = time() . '.png';
            $imageToSave = public_path()."/users/images/$imageName";
            
            //upload image to server
            file_put_contents($imageToSave, $data);
            $user->profile_pic = $imageName;
            
        }
        $user->save();
        return $this->responseSuccess(Lang::get('user.update'));

    }
    public function complaint(Request $request){
        $complaint = new UserComplaint;
        $complaint->title = 1;
        $complaint->description = $request->complaint;
        $complaint->userid = auth()->user()->id;
        $complaint->save();
        return $this->responseSuccess(Lang::get('user.contact'));

    }   
    public function contactUs(Request $request){
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $contact = new Contact();
        $data = $request->only($contact->getFillable());        
        $contact->fill($data);
        $contact->save();

        return $this->responseSuccess(Lang::get('user.contact'));
    }

    public function subscribe(Request $request){
        if ( ! Newsletter::isSubscribed($request->email) ) {
            $check = Newsletter::subscribe($request->email);
            
            if($check){
                $userEmail = $request->email;
                    Mail::raw("Thank you for subscribing ConekPro newsletters ", function ($message) use ($userEmail) {
                $message->to($userEmail)
                    ->subject('ConekPro Subscription - ConekPro')->from('salcogen2021@gmail.com');
            });
            }

        }
        return true;
    }
    public function customLogin(Request $request){
        $validator = $request->validate([
            'email'     => 'required',
            'password'  => 'required|min:6'
        ]);
        $email = $request->input('email');
        $password = $request->input('password');


        // if (Auth::guard('business')->attempt(['email' => $email, 'password' => $password])) {
        //     $user = Organization::where('email', $email)->first();
        //     $token = $user->createToken(config('app.name'))->accessToken;
        //     setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day  
            
        //     return redirect(url('/business/home'));
        // }
        // if (Auth::guard('employee')->attempt(['email' => $email, 'password' => $password])) {
        //     $user = Employee::where('email', $email)->first();
        //     $token = $user->createToken(config('app.name'))->accessToken;
        //     setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day  
            
        //     return redirect(url('/employee/dashboard'));
        // }

        if (Auth::attempt($validator)) {
            $user = Auth::user();
            $token = $user->createToken(config('app.name'))->accessToken;

        setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day  
            return redirect(url('/dashboard'));
        }
        return redirect()->back()->with('error', 'These credentials do not match our records.');
    }

}
