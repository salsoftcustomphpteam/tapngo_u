<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupUser;
use App\Models\Post;
use App\Organization;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Multicaret\Acquaintances\Interaction;
use Multicaret\Acquaintances\Models\Friendship;
use Multicaret\Acquaintances\Models\InteractionRelation;
use Multicaret\Acquaintances\Status;
use DB;
use App\Core\Chat\Soachat;
use App\CustomNotification;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;



class NetworkController extends Controller
{

    /**
     * follow
     *
     * @var array
     */
    protected $follow = [
        'pending' => 0,
        'approved' => 1,
    ];

    // PENDING
    // ACCEPTED
    // DENIED
    // BLOCKED

    /**
     * index
     *
     * @param  mixed $request
     * @return void
     */
    public function index(Request $request){
        $user = $request->user();

        if($request->type == "pending") 
            $pendingfollowings = $user->getFriendRequests()->where('status', Status::PENDING);;

        $followings = $user->getAllFriendships()->where('status', Status::ACCEPTED);

        $follow_request  = $user->getPendingFriendships();

        $my_connections  = $followings;
        $names= array();
        $i=0;
        if(count($follow_request)){
            foreach($follow_request as $request){
                if($user->name == $request->sender->name){
                    $names[$i] = $request->recipient->name;
                    $i++;
                }
                else{
                    $names[$i] = $request->sender->name;
                    $i++;
                }
            }
        }

        $people_you_may_now  = $user->getFriendsOfFriends($perPage = 9)->whereNotIn('name', $names);


        return [
            'activefollowings' => $followings,
            'pendingfollowings' => $pendingfollowings ?? [],
            'follow_request' => $follow_request,
            'my_connections' => $my_connections,
            'people_you_may_now' => $people_you_may_now 
        ];
    }    

    public function sendFollowRequest(Request $request, $id){
        if($request->query('type') == "user"){
            $resource = User::find($id);
            $authUser  = Auth()->user();
            $authUserId = $authUser->id;

            $url = '/networks/tab/1';
            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['sender_id'] = $authUserId;
            $dataToAdd['recipient_id'] = $resource->id;
            $dataToAdd['sender_type'] = 'App\User';
            $dataToAdd['recipient_type'] = 'App\User';
            $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
            $dataToAdd['body'] = 'has sent you a follow request';
            $dataToAdd['url'] = $url;
            $dataToAdd['status'] = 1;
            $dataToAdd['created_at'] = $currentDateAndTime;
            $dataToAdd['updated_at'] = $currentDateAndTime;
            CustomNotification::create($dataToAdd);
            
            //send twilio sms
            $user = User::find($resource->id);
            $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');
            
        }
        else{
            $resource = Organization::find($id);
            $authUser  = Auth()->user();
            $authUserId = $authUser->id;
            
            $url = '/business/networks/tab/1';
            $currentDateAndTime = date('Y-m-d H:i:s');
            $dataToAdd['sender_id'] = $authUserId;
            $dataToAdd['recipient_id'] = $resource->id;
            $dataToAdd['sender_type'] = 'App\User';
            $dataToAdd['recipient_type'] = 'App\Organization';
            $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
            $dataToAdd['body'] = 'has sent you a follow request';
            $dataToAdd['url'] = $url;
            $dataToAdd['status'] = 1;
            $dataToAdd['created_at'] = $currentDateAndTime;
            $dataToAdd['updated_at'] = $currentDateAndTime;
            CustomNotification::create($dataToAdd);

            //send twilio sms
            $user = Organization::find($resource->id);
            $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');
        }

        

        $request->user()->befriend($resource);

        // sendNotification([
        //     "subject" => "Follow Request Notification",
        //     "name" => $resource->name,
        //     "type" => "follow_request_notification",
        //     "text" => "{$request->user()->name} has sent you a follow request",
        //     "user" => $resource
        // ]);


        return $this->responseSuccess("You have sent Follow request to {$resource->name}");
    }


    public function removeFollower(Friendship $interaction){
        $name = request()->name;
        $interaction->delete();   
        
        return $this->responseSuccess(Lang::get('user.unfollow')." {$name}");
    }

    
    /**
     * acceptFollowRequest
     *
     * @param  mixed $interaction
     * @return void
     */
    public function acceptFollowRequest(Friendship $interaction){
        $interaction->update([
            'status' => Status::ACCEPTED,
        ]);
        $authUser  = Auth()->user();
        $authUserId = $authUser->id;
        if($interaction->sender_type=='App\User' || $interaction->sender_type == 'App\\User'){
            $url = '/networks/tab/2';
            $user = User::find($interaction->sender_id);
        }else{
            $url = '/business/networks/tab/2';
            $user = Organization::find($interaction->sender_id);
        }
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['sender_id'] = $authUserId;
        $dataToAdd['recipient_id'] = $interaction->sender_id;
        $dataToAdd['sender_type'] = 'App\User';
        $dataToAdd['recipient_type'] = $interaction->sender_type;
        $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
        $dataToAdd['body'] = 'has accepted your follow request';
        $dataToAdd['url'] = $url;
        $dataToAdd['status'] = 1;
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        CustomNotification::create($dataToAdd);

        //send twilio sms
        $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');

        Soachat::addFriends($interaction->sender->uuid, $interaction->recipient->uuid);
        Soachat::addFriends( $interaction->recipient->uuid, $interaction->sender->uuid);

        // $resource = ($interaction->sender_type == get_class(request()->user())) && request()->user()->id == $interaction->sender_id ? $interaction->recipient : $interaction->sender;
        // $name = request()->user()->name;
        // sendNotification([
        //     "subject" => "Follow Request Accepted",
        //     "name" => $resource->name,
        //     "type" => "follow_request_notification",
        //     "text" => "{$name} has accepted follow request",
        //     "user" => $resource
        // ]);

        return $this->responseSuccess(Lang::get('user.followaccepted'));
    }
    
    /**
     * rejectFollowRequest
     *
     * @param  mixed $interaction
     * @return void
     */
    public function rejectFollowRequest(Friendship $interaction){

        $interaction->update([
            'status' => Status::DENIED,
        ]);
        $authUser  = Auth()->user();
        $authUserId = $authUser->id;

        if($interaction->sender_type=='App\Organization' || $interaction->sender_type == 'App\\Organization'){
            $dataToAdd['url'] = "/business/users/$authUserId";
            $user = Organization::find($interaction->sender_id);
        }
        else{
            $dataToAdd['url'] = "/users/$authUserId";
            $user = User::find($interaction->sender_id);
        }
        
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['sender_id'] = $authUserId;
        $dataToAdd['recipient_id'] = $interaction->sender_id;
        $dataToAdd['sender_type'] = 'App\User';
        $dataToAdd['recipient_type'] = $interaction->sender_type;
        $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
        $dataToAdd['body'] = 'has rejected your follow request';
        $dataToAdd['status'] = 1;
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        CustomNotification::create($dataToAdd);

        //send twilio sms
        $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');

        // $resource = ($interaction->sender_type == get_class(request()->user())) && request()->user()->id == $interaction->sender_id ? $interaction->recipient : $interaction->sender;
        // $name = request()->user()->name;
        // sendNotification([
        //     "subject" => "Follow Rejected Accepted",
        //     "name" => $resource->name,
        //     "type" => "follow_rejected_notification",
        //     "text" => "{$name} has rejected follow request",
        //     "user" => $resource
        // ]);


        return $this->responseSuccess(Lang::get('user.followrejected'));
    }
    
    /**
     * createGroup
     *
     * @param  mixed $request
     * @return void
     */
    public function createGroup(Request $request){
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            // 'industry' => 'required',
        ]);

        $user = $request->user();

        if($request->image){
            $image = upload_base_64_image($request->image, "group-images/icon/");
        }

        if($request->cover){
            $cover = upload_base_64_image($request->cover, "group-images/cover/");
        }

        $group = $user->groupable()->save(new Group([
                "name"         => $request->name,
                "description"  => $request->description,
                // "industries"   => $request->industry,
                "image"        => $image ?? null,
                "cover"        => $cover ?? null,
        ]));
        
        $group->addUser(1, $user);

        return $this->responseSuccess(Lang::get('user.group.created'));

    }
    
    /**
     * getGroup
     *
     * @param  mixed $request
     * @return void
     */
    public function getGroup(Request $request){
        $user = $request->user();
        $keyword = $request->query('keyword');
        $type = $request->query('type');

        // Fetch My Groups
        $my_groups =  $user->groupable()->with('users.entity')->where(function ($query) use($keyword, $type){
            if($type=='myGroups'){
                $query->where('name', 'like', '%'.$keyword.'%');
            }
        })->latest()->get();

        $groupMembers =  GroupUser::where(['entity_type' => 'App\User', 'entity_id' => $user->id, 'status' => 1])->pluck('group_id');
        $my_joined_groups =  Group::whereIn('id',$groupMembers)->where('groupable_id', '!=', $user->id)->where(function ($query2) use($keyword, $type){
            if($type=='myJoinedGroups'){
                $query2->where('name', 'like', '%'.$keyword.'%');
            }
        })->latest()->get();


        // Fetch groups invited or Request to me
        $others   =  $user->getPendingGroups()->where(function ($query3) use($keyword, $type){
            if($type=='requested'){
                $query3->where('name', 'like', '%'.$keyword.'%');
            }
        });

        return [
            'my_groups' => $my_groups,
            'others' => $others,
            'joined_groups' => $my_joined_groups
        ];
    }

    
    /**
     * findGroup
     *
     * @param  mixed $request
     * @param  mixed $slug
     * @param  mixed $id
     * @return void
     */
    public function findGroup(Request $request, $slug, $id){
        $group = Group::with('users.entity')->whereSlug($slug)->whereId($id)->first();

        if(!$group) return "";

        $group->owner = $group->getOwnerAttribute();
        $group->isMember = false;
        $group->isRequestSent = false;
    
        $group->users->each(function(&$record) use ($request, $group) {
            $isMember = ($record->entity_type == get_class($request->user())) && $request->user()->id == $record->entity_id;
            if($isMember && $record->status == 1) {
                $group->isMember = true;
                $group->isRequestSent = false;
            }
            if($isMember && $record->status == 0) {
                $group->isMember = false;
                $group->isRequestSent = true;
            }
        });

        return $group;
    }

    public function deleteGroup(Request $request, $id){
        $user = $request->user();

        $group =  $user->groupable()->where('id', $id)->first();
        
        if(!$group) return $this->responseWithError(['error' => "You can not delete this group"]);

        $group->delete();

        Post::whereGroupId($id)->delete();

        return $this->responseSuccess("Group deleted successfully");
    }


    public function sendGroupRequest(Request $request, $slug, $id){
        $user = $request->user();
        $group = Group::find($id);
        $group->addUser(0, $user);


        if($group->groupable_type=='App\Organization' || $group->groupable_type == 'App\\Organization'){
            $url = "/business/networks/group/$group->slug/$group->id";
            $user = Organization::find($group->groupable_id);
        }else{
            $url = "/networks/group/$group->slug/$group->id";
            $user = User::find($group->groupable_id);
        }

        $authUser  = Auth()->user();
        $authUserId = $authUser->id;
        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['sender_id'] = $authUserId;
        $dataToAdd['recipient_id'] = $group->groupable_id;
        $dataToAdd['sender_type'] = 'App\User';
        $dataToAdd['recipient_type'] = $group->groupable_type;
        $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
        $dataToAdd['body'] = 'has sent you a group request';
        $dataToAdd['url'] = $url;
        $dataToAdd['status'] = 1;
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;
        CustomNotification::create($dataToAdd);
        $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');


        // $name = request()->user()->name;
        // sendNotification([
        //     "subject" => "Group Request Notification",
        //     "name" => $user->name,
        //     "type" => "group_request_notification",
        //     "text" => "{$name} has sent you a group request",
        //     "user" => $user
        // ]);

        return $this->responseSuccess("Request Sent");
    }

    public function handleGroupRequest($action, $id){
        $gUser      = GroupUser::findOrFail($id);
        $authUser   = Auth()->user();
        $authUserId = $authUser->id;
        $group = Group::find($gUser->group_id);

        if($gUser->entity_type=='App\Organization' || $gUser->entity_type == 'App\\Organization'){
            $url = "/business/networks/group/$group->slug/$group->id";
            $user = Organization::find($gUser->entity_id);
        }else{
            $url = "/networks/group/$group->slug/$group->id";
            $user = User::find($gUser->entity_id);
        }


        $currentDateAndTime = date('Y-m-d H:i:s');
        $dataToAdd['sender_id'] = $authUserId;
        $dataToAdd['recipient_id'] = $gUser->entity_id;
        $dataToAdd['sender_type'] = 'App\User';
        $dataToAdd['recipient_type'] = $gUser->entity_type;
        $dataToAdd['title'] = $authUser->first_name. " ". $authUser->last_name;
        $dataToAdd['url'] = $url;
        $dataToAdd['status'] = 1;
        $dataToAdd['created_at'] = $currentDateAndTime;
        $dataToAdd['updated_at'] = $currentDateAndTime;

        $this->sendMessage('ConekPro - You have a new notification, please login to your account to check.', '+'.$user->phone??'');

        
        switch($action){
            case 'withdraw':
                GroupUser::find($id)->delete();
                return $this->responseSuccess(Lang::get("user.group.withdraw"));

            case 'accept':
                GroupUser::find($id)->update([
                    'status' => 1,
                ]);
                $dataToAdd['body'] = 'has accepted your group request';
                CustomNotification::create($dataToAdd);

                return $this->responseSuccess(Lang::get("user.group.accept"));
    
            case 'reject':
                GroupUser::find($id)->update([
                    'status' => 2,
                ]);
                $dataToAdd['body'] = 'has rejected your group request';
                CustomNotification::create($dataToAdd);
                
                return $this->responseSuccess(Lang::get("user.group.reject"));
            case 'remove':
                GroupUser::find($id)->delete();
                $dataToAdd['body'] = 'has removed you from group';
                CustomNotification::create($dataToAdd);    
                return $this->responseSuccess(Lang::get("user.group.reject"));     

            default:
                return $this->responseWithError(Lang::get("user.group.undefined"));

        }
    }
    public function findUsers(Request $request){
        $gId =  request()->segment(count(request()->segments()));
        $search = $request->query('keyword');
        if(!$search){
            return array();
        }
        $users = User::whereNotIn('users.id', DB::table('group_users')->where(['group_users.group_id' => $gId, 'group_users.entity_type' => 'App\User'])->pluck('group_users.entity_id'))
        ->where(function ($query) use ($search) {
            $columns = ['first_name', 'last_name', DB::raw("concat(first_name, ' ', last_name)")];
            foreach ($columns  as $column) {
                $query->orWhere($column, 'like', "%{$search}%");
            }
        })->distinct()->select('users.id as user_id', DB::raw("concat(first_name, ' ', last_name) as user_name "))->get()->toArray();
        
        $organization = Organization::whereNotIn('organizations.id', DB::table('group_users')->where(['group_users.group_id' => $gId, 'group_users.entity_type' => 'App\Organization'])->pluck('group_users.entity_id'))
        ->where(function ($query) use ($search) {
            $columns = ['first_name', 'last_name', DB::raw("concat(first_name, ' ', last_name)")];
            foreach ($columns  as $column) {
                $query->orWhere($column, 'like', "%{$search}%");
            }
        })->distinct()->select('organizations.id as user_id', DB::raw("concat(first_name, ' ', last_name) as user_name "))->get()->toArray();


        $usersFound = array_merge($users, $organization);
        return $usersFound;
    }

    public function addMemberToGroup(Request $request){
       $userId = $request[0];
       $isOrg = $request[1];
       $groupId = $request[2];

       if($isOrg){
           $user = Organization::find($userId);
           $checkAlreadyExists = GroupUser::where(['group_id' => $groupId, 'entity_type' => 'App\Organization', 'entity_id' => $userId])->first();
           if($checkAlreadyExists){
               return array('message' => 'Already member');
           }
        $entityType = 'App\Organization';   
       }
       else{
           $user = User::find($userId);
           $checkAlreadyExists = GroupUser::where(['group_id' => $groupId, 'entity_type' => 'App\User', 'entity_id' => $userId])->first();
           if($checkAlreadyExists){
               return array('message' => 'Already member');
           }
           $entityType = 'App\User';  
       }
       $dataToAdd['group_id']       = $groupId;
       $dataToAdd['entity_type']    = $entityType;
       $dataToAdd['entity_id']      = $user->id;
       $dataToAdd['status']         = 1;
       $dataToAdd['type']           = 0;

       $add = GroupUser::create($dataToAdd);
       if($add){
        return array('message' => 'Member added successfully');

       }
       else{
            return array('message' => 'Error adding member');
       }


    
    }

    public function updateGroup(Request $request){

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            // 'industry' => 'required',
        ]);

        $user = $request->user();
        $group = Group::find($request->input('id'));
        $checkImage = str_contains('base64', $request->input('image')); 
        // var_dump(($request->input('image')));die;
        
        if($request->input('image')){
            if (strpos($request->input('image'), 'base64') !== false){

                $image = upload_base_64_image($request->input('image'), "group-images/icon/");
                $group->image = $image;
            }

        }

        if($request->input('cover')){
            if(strpos($request->input('cover'), 'base64') !== false){
                $cover = upload_base_64_image($request->input('cover'), "group-images/cover/");
                $group->cover = $cover;
            }
        }

        $group->name = $request->input('name');
        $group->description = $request->input('description');
        $group->save();


        return $this->responseSuccess(Lang::get('user.group.created'));
        
    }
    private function sendMessage($message, $recipients)
    {

        try {
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);
        } catch (RestException $e) {
        }

    }


}
