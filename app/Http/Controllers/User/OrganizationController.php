<?php

namespace App\Http\Controllers;

use App\Core\Filters\UserFilters;
use App\Employee;
use App\Organization;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class OrganizationController extends Controller
{
    
    /**
     * per_page
     *
     * @var int
     */
    protected $per_page = 10;
        
    /**
     * follow
     *
     * @var array
     */
    protected $follow = [
        'pending' => 0,
        'approved' => 1,
    ];

    /**
     * index
     *
     * @param  mixed $request
     * @return void
     */
    public function index(Request $request){

        $user = $request->user();
        $ids = $user->getAcceptedFriendships()->map->only('recipient', 'sender')->flatten()->pluck('uuid');

        // $ids = $request->user()->followings(Organization::class)->latest()->activefollow()->get()->pluck('id');

        return Organization::select('id', 'first_name', 'last_name', 'description', 'image')
                ->latest()
                ->whereNotIn('uuid', $ids)->paginate($request->per_page ?? $this->per_page);
    }
    
    /**
     * show
     *
     * @param  mixed $request
     * @param  mixed $organization
     * @return void
     */
    public function show(Request $request, Organization $organization){
        $organization->isFriendWith = $request->user()->isFriendWith($organization);
        $organization->hasSentFriendRequestTo = $request->user()->hasSentFriendRequestTo($organization);

        return $organization;

    }
    
    /**
     * toggleFollow
     *
     * @param  mixed $request
     * @param  mixed $organization
     * @param  mixed $type
     * @return void
     */
    public function toggleFollow(Request $request, Organization $organization, $type = 0){
        $user = $request->user();
        
        if($type == 0){
            $user->follow($organization);
            return $this->responseSuccess(Lang::get("user.follow").$organization->name);
        }
        else{
            $user->unfollow($organization);            
            return $this->responseSuccess(Lang::get("user.unfollow").$organization->name);
        }
            
    }

}