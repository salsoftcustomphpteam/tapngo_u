<?php

namespace App\Http\Controllers\Admin;

use App\Core\Filters\CategoryFilter;
use App\Core\Http\Resources\FeedbackResource;
use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{

    public function index(Request $request){
        return Feedback::with('user')->orderBy('id', 'desc')->get();
    }

    public function show(Request $request, $id){
        return Feedback::with('user')->whereId($id)->first();
    }
    
    public function markAsRead(Request $request, $id){
        return Feedback::whereId($id)->update(['status' => 1]);
        return $this->responseSuccess('Feedback is mark as replied');
    }



}
