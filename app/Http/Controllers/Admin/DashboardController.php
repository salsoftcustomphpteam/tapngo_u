<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Organization;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DashboardController extends Controller
{


    /**
     * @param Request $request
     * @return mixed
     */
    public function getMyNotifications(Request $request){

        if($request->type == 'all')
            return auth()->guard('admin_api')->user()->notifications;

        return auth()->guard('admin_api')->user()->unreadNotifications;
    }

    /**
     * Display a listing of the resource.
     *
     * @param TrainerFilters $filters
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $employees = Employee::whereStatus(1)->count();
        $client = User::whereStatus(1)->count();
        $org = Organization::whereStatus(1)->count();

        // $year = Carbon::now()->year;
        // if($request->year)
        //     $year = $request->year;

        // $orders = Order::query()
        //     ->select('id','total','created_at')
        //     ->whereYear('created_at', '=', $year)
        //     ->orderBy('created_at', 'DESC')
        //     ->get()->groupBy(function($d) {
        //         return Carbon::parse($d->created_at)->format('m');
        //     });

        // $graphs = $lgraphs = [];
        // for ($i=1; $i<=12; $i++){
        //     $graphs[str_pad($i, 2, '0', STR_PAD_LEFT)] = 0;
        // }
        // foreach ($orders as $index => $graph){
        //     $graphs[str_pad($index, 2, '0', STR_PAD_LEFT)] = count($graph);
        // }

        return [
            'employees' => $employees,
            'client' => $client,
            'org' => $org,
            // 'totalorders' => $totalorders,
            // 'orders' => $orders,
            // 'ordersGraphs' => array_values($graphs),
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Category
     */
    public function show(Category $category)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
    }
}
