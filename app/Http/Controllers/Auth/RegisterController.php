<?php

namespace App\Http\Controllers\Auth;

use App\Chat\Soachat;
use App\Helper\CreditCard;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use App\Models\Refferal;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // mashup
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:tab_User'],
            'password' => ['required', 'string', 'min:8'],
            "first_name" => ['required'],
            "last_name" => ['required'],
        ]);
    }

    public function createToken($cardHolderName, $creditCardNo, $expMonth, $expYear, $cvv)
    {
        try {
            $token = \Stripe\Token::create([
                'card' => [
                    'name' => $cardHolderName,
                    'number' => $creditCardNo,
                    'exp_month' => $expMonth,
                    'exp_year' => $expYear,
                    'cvc' => $cvv,
                ],
            ]);
        } catch (\Exception $ex) {
            return ['message' => $ex->getMessage(), 'status' => false, 'details' => []];
        }

        return ['message' => 'success', 'status' => true, 'id' => $token->id];
    }    


    public function addUser($data){
        $user = new User();
        $user->firstname    = $data['first_name'];
        $user->lastname     = $data['last_name'];
        $user->email        = $data['email'];
        $user->phone_number = time();
        $user->password     = Hash::make($data['password']);
            if(isset($data['profile_pic'])){
                // dd($data->file('file'));
                $image = $request->file('profile_pic');
                $extension = $image->getClientOriginalExtension();
                $ImgName = $image->getClientOriginalName();
                $fileNameWithoutEx = pathinfo($ImgName, PATHINFO_FILENAME);
                $destinationPath = public_path("/users/images");
                if (!file_exists($destinationPath)) {
                    //create folder
                    mkdir($destinationPath, 0755, true);
                }
                $time = time();
                $image->move($destinationPath, $fileNameWithoutEx . "_" . $time . "." . $extension);
                $fullImagePath = $fileNameWithoutEx . "_" . $time . "." . $extension;
                $profilePic = $fullImagePath;
                $user->profile_pic = $profilePic;       
            }   
        $user->save();
        if($user){
            $ref = new Refferal;
            $ref->user_id       = $user->id;
            $ref->code          = time();
            $ref->amount_earned = 0.00;
            $ref->amount_spent  = 0.00;
            $ref->amount_balance= 0.00;
            $ref->save();
        }

        return $user;
    }

    public function charge($email, $amount, $token)
    {

        $customer = \Stripe\Customer::create(array(
            'email' => $email,
            'source' => $token
        ));

        try {
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount' => $amount * 100,
                'currency' => 'USD',
                'description' => ""
            ));

            return $charge;
        } catch (\Exception $ex) {
            // return ['message' => $ex->getMessage(), 'status' => false, 'details' => []];
        }
    }

    protected function addSubscription($user, $data, $token){
        
        $package = Package::find($data['currentPackage']);

        $charge = $this->charge($data['email'], $package->amount, $token);

        $sub = $user->subscribeable()->save(new Subscription([
            'package_id' => $data['currentPackage'],
            'expiry_at' => Carbon::now()->addMonth($package->month),
            'status' => '1',
        ]));

        $sub->transactionable()->save(new Payment([
            'charge_id' => $charge->id,
            'status' => 1,
            'payload' => $charge,
        ]));

        return $user;
    
    }   

    protected function sanatizeCard($string){
        $searchReplaceArray = array(
            '-' => '', 
            '_' => ''
        );
        $result = str_replace(
            array_keys($searchReplaceArray), 
            array_values($searchReplaceArray), 
            $string
        );       
        return $result;  
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = $this->addUser($data);
        $token = $user->createToken(config('app.name'))->accessToken;
        setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day

        return $user;
    }

    private function sendMessage($message, $recipients)
    {

        try {
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message]);
        } catch (RestException $e) {
        }

    }
}