<?php

namespace App\Http\Controllers\Api\User;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Media;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BookingRequest;
use DB;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $authUser = auth()->user();
        $rides                  = BookingRequest::where('user_id', $authUser->id);
        $data['rides']          = $rides->orderBy('id', 'DESC')->limit(10)->get();
        $data['totalRides']     = $rides->count();
        $data['ridesCompleted'] = $rides->where('is_completed', 1)->count();
        $data['ridesCancelled'] = $rides->where('is_cancelled', 1)->count();

            $currentYear= date('Y');
            $year       = date('Y');
            $records    = $rides->select(DB::raw("Count(id) as count, MONTH(created_at) month"))
            ->groupBy(DB::raw("year(created_at), MONTH(created_at)"))
            ->where('created_at', 'like', '%'.$year.'%')
            ->get();
    
            $month = $rides->select(DB::raw("Count(id) as count"))
                    ->orderBy("created_at")
                    ->groupBy(DB::raw("month(created_at)"))
                    ->get()->where('created_at', 'like', '%'.$year.'%')->toArray();
            $month = array_column($month, 'count');
        
            $months = collect(['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);
        
            $chartData = array();
        
            for ($i = 1; $i <= 12; $i++){
                $chartData[$i] = $records->where('month', $i)->sum('count');
            }
        
            $chartData = array_values($chartData);
            $data['chartData'] = $chartData;

        return $data;

    }

}
