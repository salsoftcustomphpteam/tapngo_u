<?php

namespace App\Http\Controllers\Api\User;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BookingRequest;
use App\Models\BookingRequestPlace;


class BookingController extends Controller
{
    // const BookingRequest_STATUS_DELETE = "2";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type   = request('type');
        $search = request('search');
        $authUser = auth()->user();
       return $bookings = BookingRequest::where('user_id', $authUser->id)->where(function ($query) use($type, $search){
            if($type=='completed'){
                $query->where(['is_completed' => 1]);
            }
            if($type=='cancelled'){
                $query->where(['is_cancelled' => 1]);
            }
            if($type=='on-going'){
                $query->where(['is_trip_start' => 1, 'is_completed' => 0, 'is_cancelled'=>0]);
            }
            

        })->paginate($request->per_page ?? 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $authUser       = auth()->user();
        $distance       = $request[0]['distance'];
        $bookingRequest = new BookingRequest;
        $bookingRequest->request_id = 'WEB_'.time();
        $bookingRequest->user_id    = $authUser->id;
        $bookingRequest->distance   = $distance;
        $bookingRequest->save();
        
        if($bookingRequest){
            //locations
            $pickup     = $request[0]['pickup'];
            $dropoff    = $request[0]['dropoff'];
            $locations  = $request[0]['locations'];
            $i=0;
            $bookingRequestPlace = new BookingRequestPlace;

            foreach($locations as $location){
                if($i==0){
                    //pickup
                    $bookingRequestPlace->pick_latitude     = $location['lat'];
                    $bookingRequestPlace->pick_longitude    = $location['lng'];
                }else{
                    //dropoff
                    $bookingRequestPlace->drop_latitude     = $location['lat'];
                    $bookingRequestPlace->drop_longitude    = $location['lng'];
                }
                $i++;
            }
            $bookingRequestPlace->pick_location = $pickup;
            $bookingRequestPlace->drop_location = $dropoff;
            $bookingRequestPlace->request_id    = $bookingRequest->id;
            $bookingRequestPlace->save();
        }

      return true;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookingRequest  $BookingRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           return $bookingDetail = BookingRequest::find($id);
           

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookingRequest  $BookingRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(BookingRequest $BookingRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookingRequest  $BookingRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookingRequest $BookingRequest)
    {
       

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookingRequest  $BookingRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = BookingRequest::where('id', $id)->update(['is_cancelled'=> 1]);
    }
}
