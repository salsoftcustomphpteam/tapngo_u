<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Core\Filters\UserFilters;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Newsletter;
use Mail;
use Illuminate\Support\Facades\Auth;
use App\Chat\Soachat;

class UserController extends Controller
{
    // constants
    const USER_STATUS_ACTIVE    = "1";
    const USER_STATUS_INACTIVE  = "0";
    const USER_STATUS_BLOCKED   = "2";
    const USER_STATUS_DELETED   = "3";


    /**
     * follow
     *
     * @var array
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            "firstname" => ['required'],
            "lastname" => ['required'],
            "address" => ['required'],
            "phone_number" => ['required', 'numeric', 'min:9'],
            "city" => ['required'],
            "state" => ['required'],
            // "zip" => ['required'],
            "country" => ['required'],
            // 'recovery_email' => ['required', 'string', 'email', 'max:255'],
            "gender" => ['required'],
        ]);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'email'     => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        $userEmail      = $request->email;
        $userPassword   = $request->password;
        $deviceId       = $request->device_id;


        $checkUserExists = false;
        $UserInst           = new User;
        $checkUserExists    = $UserInst::whereEmail($userEmail)->first();
        if (!$checkUserExists) {
            return response()->json(['message' => 'User does not exist with this email address.'], 400);
        }
        // return $checkUserExists;

        if (Auth::attempt(['email' => $userEmail, 'password' => request('password')])) {
            $user  = Auth::user();
            $token = $user->createToken(config('app.name'))->accessToken;
            setcookie('p_token', $token, time() + (86400 * 30), "/"); // 86400 = 1 day

            $userStatus = $user->status;
            $userId     = $user->id;
            if ($userStatus == self::USER_STATUS_INACTIVE) {
                return response()->json(['message' => 'User status unverified.'], 400);
            }
            if ($userStatus == self::USER_STATUS_BLOCKED) {
                return response()->json(['message' => 'This account has been banned by the admin. Please contact admin.'], 400);
            }

            //update user info
            $updateUser         = array();
            $currentDateAndTime = $request->input('current_time') ?? date('Y-m-d H:i:s');
            if ($deviceId){
                $updateUser['device_id'] = $deviceId;
                $updateUser = $UserInst->update($updateUser);              
            }

            //user info
            $success = array(
                'message'           => 'Login successfull',
                'access_token'      => $token,
                'user'         =>  array(
                                        'user_id' => $userId,
                                        'name'=> $user->name, 
                                        'email'=> $user->email,
                                        )
                            );


            return response()->json($success, 200);
        } else {
            return response()->json(['message' => 'Login failed, please check your credentials and try again.'], 400);
        }

    }

    public function signUp(Request $request){
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'phone'     => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        $validator = Validator::make($request->all(), [
            'email'     => 'required|exists:users',
        ]);
        if (!$validator->fails()) {
            return response()->json(['message' => 'User already exists with this email address.'], 400);
        }
        //find roles
        $role = $this->findRole('user');
        $userInst = new User;
        $userInst->role_id      = $role->id;
        $userInst->name         = $request->name;
        $userInst->email        = $request->email;
        $userInst->password     = bcrypt($request->password);
        $userInst->phone_number = $request->phone;
        $userInst->save();
        return response()->json(['success' => true, "message"=>'User account created successfully.'], 201);


    }
    public function getProfile(){
        $user = auth()->user();
         //user info
         $success = array(
            'user'         =>  array(
                                    'user_id' => $user->id,
                                    'name'=> $user->name, 
                                    'email'=> $user->email,
                                    'phone'=> $user->phone_number,
                                    'tag_line'=> $user->tag_line,
                                    'work'=> $user->work,
                                    'favorite_food'=> $user->favorite_food,
                                    'favorite_song'=> $user->favorite_song,
                                    'favorite_color'=> $user->favorite_color,
                                    'highest_level_of_education'=> $user->highest_level_of_education,
                                    'profile_image'=> $user->profile_image,
                                    'favorite_food'=> $user->favorite_food,
                                    )
                        );


        return response()->json($success, 200);
    }

    public function resetPasswordRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        $userEmail  = $request->email;
        $userModel  = new User;
        $user       = $userModel->whereEmail($userEmail)->first();

        if (!$user){
            return response()->json(['message' => 'User does not exists.'], 400);
        }
        if ($user->status == self::USER_STATUS_INACTIVE) {
            return response()->json(['message' => 'User status unverified.'], 400);

        }
        if ($user->status == self::USER_STATUS_BLOCKED) {
            return response()->json(['message' => 'This account has been banned.'], 400);
        }
        if ($user->status == self::USER_STATUS_DELETED) {
            return response()->json(['message' => 'Account does not exists.'], 400);
        }

        $digits = 4;
        $verificationCode = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

        //email to user for code
        Mail::raw("SCHP password reset code is: $verificationCode", function ($message) use ($userEmail) {
            $message->to($userEmail)
                ->subject('Password reset Code - SCHP')->from('salcogen2021@gmail.com');
        });

        $startDate      = time();
        $resetExpiry    = date('Y-m-d H:i:s', strtotime('+1 day', $startDate));
        $dataToUpdate['verification_code_expiry'] = $resetExpiry;
        $dataToUpdate['verification_code']        = $verificationCode;
        $updateUserStatus   = $userModel->updateUser($user->id, $dataToUpdate);
        //update user veriy code
        if ($updateUserStatus) {
            return response()->json(['success' => true, "message"=>'Email sent, please check your email for security code.'], 200);
        }
        return response()->json(['message' => 'Error sending code.'], 400);

    }

    public function verifyCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email'             => ['required', 'email', 'regex:^([0-9a-zA-Z]+[.-_]{0,1})+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,}$^'],
            'code'              => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }
        $userEmail          = $request->email;
        $userGivenCode      = $request->code;
        $userModel          = new User;
        $userStoredCode     = $userModel->whereEmail($userEmail)->first();
        if (!$userStoredCode) {
            return response()->json(['message' => 'User does not exist with this email address.'], 400);
        }

        if ($userStoredCode->status == self::USER_STATUS_BLOCKED) {
            return response()->json(['message' => 'User status blocked.'], 400);
        }
        if ($userGivenCode == $userStoredCode->verification_code) {
            // $dataToUpdate['status']         = self::USER_STATUS_ACTIVE;
            $dataToUpdate['verification_code_expiry']   = "";
            $dataToUpdate['verification_code']    = "";
            $updateUserStatus     = $userModel->updateUser($userStoredCode->id, $dataToUpdate);
            return response()->json(['success' => true, "message"=>'Code matched.'], 200);
        } else {
            return response()->json(['message' => 'The code you have entered is invalid.'], 400);
        }
    }

    //reset password
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => ['required', 'email', 'regex:^([0-9a-zA-Z]+[.-_]{0,1})+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,}$^'],
            'password' => ['required','min:6', 'max:16'],
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $email      = $request->email;
        $password   = $request->password;
        $userModel  = new User;
        $checkUser  = $userModel::whereEmail($email)->first();

        if (!$checkUser) {
            return response()->json(['message' => 'User does not exist with this email address.'], 400);
        }

        //update user password
        $password                = bcrypt($request->password);
        $dataToUpdate['password']= $password;
        $updateUserPassword      = $userModel->updateUser($checkUser->id, $dataToUpdate);

        if ($updateUserPassword) {
            return response()->json(['success' => true, 'message'=>'Password updated successfully.'], 200);
            
        } else {
            return response()->json(['message' => 'Error updating password.'], 400);
        }
    }
    //change password
    public function changePassword(Request $request){
        $validator = Validator::make($request->all(), [      
            
            'new_password' => [
                'required',
                'min:6', 'max:16',
                // 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@!$#&*%^]).*$/'
            ],
            'old_password'  => [
                'required',
                'min:6', 'max:16',
                // 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@!$#&*%^]).*$/',
            ],
        ]);
		if ($validator->fails()) { 
		    return response()->json(['message' => $validator->errors()->first()], 400);            
        }
               
        $oldPassword        = $request->old_password;
        $newPassword        = $request->new_password;
        $user               = auth()->user();
        
        if($oldPassword == $newPassword){
            return response()->json(['message' => 'New password cannot be same as current password.'], 400);            
        }
        $userOldPassword    = $user->password;
        $passwordIsSame     = Hash::check($oldPassword, $userOldPassword);        
        if(!$passwordIsSame){
            return response()->json(['message' => 'Old password is incorrect.'], 400);            
        }
        
        //update user password
        $userModel = new User;
        $dataToUpdate['password']    =  bcrypt($newPassword);
        $updateUserInfo = $userModel->updateUser($user->id, $dataToUpdate);
        if($updateUserInfo){
            return response()->json(['success' => true, 'message' => 'Your password is changed successfully.' ], 200);
        }
        else{
            return response()->json(['message' => 'Error updating password'], 400);            
        }

    }

    public function updateProfile(Request $request){
        $this->validator($request->all())->validate();
        
        $user = $request->user();        
        
        $user->fill(collect($request->all())->toArray());
        if($request->input('image')){
	       
            $data = $request->input('image');
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imageName = time() . '.png';
            $imageToSave = public_path()."/users/images/$imageName";
            
            //upload image to server
            file_put_contents($imageToSave, $data);
            $user->image = $imageName;
            
        }
        
        $user->save();
        $soachat = new Soachat();        
        $soachat->updateUser($user->uuid, $user->name, $user->image, 0);

        return $this->responseSuccess(Lang::get('user.update'));

    }   

    public function logout()
    {
        $authUser  = auth()->user();
        $accessTokenModel = new Token;
        $destroySessions  = Token::whereDestroyTokenByUserId($authUser->id)->delete();
        //remove device token so that user fcm push notifications does not work
        // $dataToUpdate['device_id']   =  "";
        // $updateUserPassword     = $userModel->updateUser($userId, $dataToUpdate);
        return response()->json(['success' => true, 'message' => 'logged out.' ], 200);
    }
    private function findRole($type){
        return $find = Role::where('name', 'like', '%'.$type.'%')->first();
    }


}