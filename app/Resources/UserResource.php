<?php

namespace App\Core\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'name' => $this->name,
            'email' => $this->email,
            'contact' => $this->contact,
            // 'ads_count' => $this->ads_count ?? 0,
            'dob' => $this->dob,
            'dob_formatted' =>  $this->dob ? Carbon::parse($this->dob)->format(config('app.date_format')) : null,
            'image' => $this->image,
            'city' => $this->city,
            'address' => $this->address,
            'country' => $this->country,
            'phone' => $this->phone,
            'state' => $this->state,
            'zipcode' => $this->zipcode ?? $this->zip,
            'subscription' => $this->subscription ?? [],
            'created_date' => $this->created_at->format(config('app.date_format')),
        ];
    }
}
