<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Models\User;

class Refferal extends Model{

    protected $fillable = [
        'user_id', 'amount_earned', 'amount_balance', 'amount_spent'
    ];
    protected $table = 'tab_referral';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
