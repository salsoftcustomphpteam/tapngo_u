<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Feedback extends Model
{

    protected $fillable = [
        'user_id', 'name', 'email', 'subject', 'message'
    ];

    protected $appends = ['created_date'];

    public function getCreatedDateAttribute(){
        return $this->created_at->format(config('app.date_format'));
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}