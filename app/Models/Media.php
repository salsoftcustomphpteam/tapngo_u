<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media as BaseMedia;

class Media extends BaseMedia
{
    protected $appends = ['full_url', 's'];

    public function getFullUrlAttribute(){
        return $this[0]->getFullUrl();
    }
    
    public function getSAttribute(){
        return $this->getFullUrl();
    }
}
