<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Event;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Notifications\ResetPasswordNotification;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Str;

class BookingRequest extends Model{
    protected $with = ['bookingplaces'];
    protected $appends = ['created_date', 'created_time', 'status'];
    protected $table = 'tab_request';    
    protected $fillable = [
        "request_id",
        "later",
        "user_id",
        "driver_id",
        "trip_start_time",
        "is_driver_started",
        "is_driver_arrived",
        "is_trip_start",
        "is_completed",
        "is_cancelled",
        "reason",
        "cancel_method",
        "distance",
        "time",
        "total",
        "payment_opt",
        "is_paid",
        "user_rated",
        "driver_rated",
        "payment_id",
        "card_id",
        "type",
        "promo_id",
        "timezone",
        "unit",
        "if_dispatch",
        "dispatch_reference",
        "driver_eta",
     
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function bookingplaces(){
        return $this->hasOne(BookingRequestPlace::class, 'request_id');
    }
    public function getCreatedDateAttribute(){
        return $this->created_at->format(config('app.date_format'));        
    }
    public function getCreatedTimeAttribute(){
        return $this->created_at->format(config('app.time_format'));        
    }
    public function getStatusAttribute(){
        if($this->is_completed==1){
            return 'completed';
        }
        if($this->is_cancelled==1){
            return 'cancelled';
        }
        if($this->is_trip_start==1 && $this->is_cancelled==0 && $this->is_completed ==0 ){
            return 'ongoing';
        }
        else{
            return 'pending';
        }

    }


}
