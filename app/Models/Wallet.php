<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Models\User;

class Wallet extends Model{

    protected $fillable = [
        'user_id', 'code', 'amount_earned', 'amount_spent', 'amount_balance'
    ];
    protected $table = 'tab_wallet';
    protected $appends = ['created_date'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function getCreatedDateAttribute(){
        return $this->created_at->format(config('app.date_format'));
    }

}
