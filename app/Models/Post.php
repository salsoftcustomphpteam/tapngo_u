<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Models\User;

class Post extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'user_id', 'title', 'body', 'is_shared', 'post_author', 'original_post_id', 'status', 'privacy'
    ];

    protected $appends = ['created_date'];

    public function getCreatedDateAttribute(){
        return $this->created_at->format(config('app.date_format'));
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
