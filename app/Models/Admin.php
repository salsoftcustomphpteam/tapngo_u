<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Notifications\AdminResetPasswordNotification;
use Str;
class Admin extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        "name",
        "email",
        "password",
        "profile_image",
        "phone"
    ];
   
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }    
}
