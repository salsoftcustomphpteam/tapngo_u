<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Event;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Notifications\ResetPasswordNotification;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Str;

class BookingRequestPlace extends Model {

    protected $table = 'tab_request_place';
    protected $fillable = [
        "pick_latitude",
        "pick_longitude",
        "drop_latitude",
        "drop_longitude",
        "pick_location",
        "drop_location",
        "request_id",
    ];

}
