<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Event;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Notifications\ResetPasswordNotification;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Str;

class User extends Authenticatable implements HasMedia {

    use HasApiTokens, Notifiable, HasMediaTrait;

    protected $table = 'tab_User';
    protected $fillable = [
        "first_name",
        "email",
        "last_name",
        "profile_pic",
        "password",
        "gender",
        "phone_number",
        "is_active",
    ];

    public function getProfilePicAttribute($value){
        if(!$value){
            return "";
        }
    }
   
    public function rides(){
        return $this->hasMany(BookingRequest::class);
    }
    public function sendPasswordResetNotification($token)
    {
        $url =$token;
        //$url =$token."?type=".request('type').'&';
        $this->notify(new ResetPasswordNotification($url));
    }

}
