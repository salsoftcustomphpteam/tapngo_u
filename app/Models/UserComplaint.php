<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserComplaint extends Model
{
    protected $table = 'tab_User_Compliant';
    protected $fillable = [
        'admin_reference', 'title', 'description', 'admin_comments', 'status', 'userid'
    ];


    public function getCreatedDateAttribute(){
        return $this->created_at->format(config('app.date_format'));
    }

    public function user(){
        return $this->belongsTo(User::class, 'userid');
    }
}
