<?php

namespace App\Models;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'brand_title', 'brand_description', 'call_to_action_link', 'expiry', 'status'
    ];
    protected $with = ['media'];
    
    protected $appends = ['created_date'];

    public function getCreatedDateAttribute(){
        return $this->created_at->format(config('app.date_format'));
    }
    
}
