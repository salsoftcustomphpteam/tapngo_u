<?php

namespace App\Models;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use HasApiTokens, Notifiable;
    protected 	$table 			= 'oauth_access_tokens';
    protected 	$primaryKey 	= 'id';
    public 		$timestamps 	= false;
    protected 	$fillable 		= 
    [
    'user_id',
    'client_id',
    'name',
    'scopes',
    'revoked',
    'created_at',
    'updated_at',
    ];
    
    public function scopeDestroyTokenByUserId($query, $userId)
    {
        return $query->where("user_id", $userId);
    } 
}
