<?php

namespace App\Filters;

class JobFilters extends Filters {

    protected $filters = ['from', 'to', 'city', 'country'];

    public function from($value){
        if($value)
            $this->builder->whereDate('apply_before', '>=', $value);
    }

    public function to($value){
        if($value)
            $this->builder->whereDate('apply_before', '<=', $value);
    }

    public function city($value) {
        if(!$value) return ;

        $this->builder->where('city', 'LIKE', "%{$value}%");

    }
    
    public function country($value) {
        if(!$value) return ;

        $this->builder->where('country', 'LIKE', "%{$value}%");

    }   
     

}
