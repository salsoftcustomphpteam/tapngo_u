<?php

namespace App\Filters;

class FeedbackFilters extends Filters {

    protected $filters = ['search_term'];

    public function search_term($term)
    {
        $this->builder->where(function($q)use($term){
            $q
                ->orWhereHas('owner', function($q) use($term){
                    $q->where('name', "LIKE", '%' . $term . '%');
                })
//            ->orWhere('description', "LIKE", '%' . $term . '%')
                ->orWhere('email', "LIKE", '%' . $term . '%')
                ->orWhere('message', "LIKE", '%' . $term . '%');
        });
    }

}
