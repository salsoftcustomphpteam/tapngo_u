<?php

namespace App\Filters;

class ProjectFilters extends Filters {

    protected $filters = ['only_board', 'name', 'keyword'];

    public function only_board($only_board)
    {
        return $this->builder->where('add_board', $only_board);
    }
    
    public function status($status)
    {
        $this->builder->where("status", $status);
    }

    public function name($name){
        if($name !== ""){
            return $this->builder->where("name", "LIKE", "%{$name}%");
        }
    }

    public function keyword($keywords){
        if($keywords !== ""){
            $terms = explode(',',$keywords);
            $this->builder->where(function($query) use($terms){
                foreach($terms as $term){
                    if($term !== "" && $term !== null){
                        $query->orWhere("skills", "LIKE", "%{$term}%");
                    }
                }

            });
        }
    }    

}
