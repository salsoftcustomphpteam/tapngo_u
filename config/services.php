<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'stripe' => [
        'publish' => 'pk_test_HAorAMBpZdnSy3XeSoEc7EHZ00GmySthxL',
        'secret' => 'sk_test_6mb3q2e2DC5qsrTVlkX82lkQ00RqzFBxvP',
    ],

    'soachat' => [
        "appid" => "b03368fe82bbb4edc476e469b7d92e27",
        "appkey" => "729b5b845beeddba469b00adf0864ec4",
        "domain" => "dev28.onlinetestingserver.com",
        "secretkey" => '$2y$10$v3IHmzBMffOlQncGR0GzL.FNWcatXtzmvj8Vhg5fgQgCcoHpdjISa',
        "endpoint" => 'http://dev28.onlinetestingserver.com/soachatcentralizedWeb',
    ]



];
