const mix = require('laravel-mix');
var webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

if(process.env.NODE_ENV === "development" || process.env.NODE_ENV === "production"){
    mix.js('resources/js/admin/app.js', 'public/admin/js');
        mix.sass('resources/sass/admin/app.scss', 'public/admin/css')
        .webpackConfig({
            plugins: [
                new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            ],
            output: {
                publicPath: `${process.env.MIX_BASE_URL}/`,
                chunkFilename: `js/admin-chunks/[name].js`,
            }
        })
        .options({
            processCssUrls: false
        });
}

if(process.env.NODE_ENV === "userdevelopment" || process.env.NODE_ENV === "userproduction"){
    let base = process.env.NODE_ENV === "userproduction" ? process.env.MIX_PRODUCTION_FULL : process.env.MIX_STAGING_FULL;
    let public_path = `${ process.env.NODE_ENV === "userproduction" ? '/'+process.env.MIX_PRODUCTION_BASE+'/' : process.env.MIX_BASE_URL}/`;
    mix.js('resources/js/user/app.js', 'public/users/js');
        mix.sass('resources/sass/user/app.scss', 'public/users/css')
        .webpackConfig({
            plugins: [
                new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            ],        
            resolve: {
                extensions: ['.js', '.vue', '.json'],
                alias: {
                  'vue$': 'vue/dist/vue.esm.js',
                  '@':  base + '/public/',
                }
            },                
            output: {
                publicPath: public_path,
                chunkFilename: `js/user-chunks/[name].js`,
            }
        })
        .options({
            processCssUrls: false
        });
}




if(process.env.NODE_ENV === "adminproduction") {
    let base = process.env.NODE_ENV === "adminproduction" ? process.env.MIX_PRODUCTION_FULL : process.env.MIX_STAGING_FULL;
    let public_path = `${ process.env.NODE_ENV === "adminproduction" ? '/'+process.env.MIX_PRODUCTION_BASE+'/' : process.env.MIX_BASE_URL}/`;
    mix.js('resources/js/admin/app.js', 'public/admin/js');
    mix.sass('resources/sass/admin/app.scss', 'public/admin/css')
    .webpackConfig({
        plugins: [
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        ],        
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
              'vue$': 'vue/dist/vue.esm.js',
              '@':  base + '/public/',
            }
        },                      
        output: {
            publicPath: public_path,
            chunkFilename: `js/admin-chunks/[name].js`,
        }
    })
    .options({
        processCssUrls: false
    });
}

mix.disableSuccessNotifications();

