 /*editor start here*/

try{
    
    CKEDITOR.replace( 'editorr', {
        customConfig: '/ckeditor_settings/config.js',
    });
	CKEDITOR.editorConfig = function( config )
    {
		extraPlugins: 'placeholder',
        config.height = '100px';
		
    };
	
    
}
catch(e){
    console.log(e);
}

 /*editor end here*/
/*accordian start here*/

$(document).ready(function() {
            $(".accordion_head").click(function() {
                if ($('.accordion_body').is(':visible')) {
                    $(".accordion_body").slideUp(300);
                    $(".plusminus").innerHTML('<i class="fa fa-plus-circle"></i>');
                    $(".plusminus").removeClass('customMinus');
                }
                if ($(this).next(".accordion_body").is(':visible')) {
                    $(this).next(".accordion_body").slideUp(300);
                    $(this).children(".plusminus").innerHTML('<i class="fa fa-minus-circle"></i>');
                    $(this).children(".plusminus").removeClass('customMinus');
                }
				else {
                    $(this).next(".accordion_body").slideDown(300);
                    $(this).children(".plusminus").innerHTML('<i class="fa fa-minus-circle"></i>');
                    $(this).children(".plusminus").addClass('customMinus');
                }
            });
        });


/*accordian end here*/


/*cropper start here*/


$(function () {

	var croppie = null;

	var el = document.getElementById('resizer');

	$.base64ImageToBlob = function (str) {

		var pos = str.indexOf(';base64,');
		var type = str.substring(5, pos);
		var b64 = str.substr(pos + 8);

		var imageContent = atob(b64);

		var buffer = new ArrayBuffer(imageContent.length);
		var view = new Uint8Array(buffer);

		for (var n = 0; n < imageContent.length; n++) {
			view[n] = imageContent.charCodeAt(n);
		}

		var blob = new Blob([buffer], {
			type: type
		});

		return blob;
	}

	$.getImage = function (input, croppie) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				croppie.bind({
					url: e.target.result,
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}


	$("#upload").on("change", function (event) {

		$("#et-profile-image-modal").modal();

		croppie = new Croppie(el, {
			viewport: {
				width: 200,
				height: 200,
				type: 'circle'
			},
			boundary: {
				width: 250,
				height: 250
			},
			enableOrientation: true
		});

		$.getImage(event.target, croppie);

	});


	$("#et-crop-and-upload").on("click", function () {

		croppie.result('base64').then(function (base64) {
			$("#et-profile-image-modal").modal("hide");
			$("#et-profile-preview-image").attr("src", base64);
			$("#et-field-avatar").val(base64);
		});

	});

	$('#et-profile-image-modal').on('hidden.bs.modal', function (e) {
		// This function will call immediately after model close
		// To ensure that old croppie instance is destroyed on every model close
		setTimeout(function () {
			croppie.destroy();
		}, 100);
	})

	$(".rotate").on("click", function () {
		croppie.rotate(parseInt($(this).data('deg')));
	});



});



/*cropper end here*/
  

/*modal click change start here*/

$(function(){
	
    $('#delete-menu').on('click', function(){
        $('.delete-modal').modal('hide');
        $('.confirm-delete-modal').modal('show');
    });
	
	$('#login-2').on('click', function(){
        $('.bd-example-delete-modal-lg').modal('hide');
        $('.login-2-modal').modal('show');
    }); 
	
    $('#assign-com').on('click', function(){
        $('.assign-company').modal('hide');
        $('.assign-company-2').modal('show');
    });
	      

	 $('#create-tick').on('click', function(){    
        $('.create-ticket').modal('hide');
        $('.create-ticket-2').modal('show');
    });  
	
	 $('#ticket-1-btn').on('click', function(){    
        $('.ticket-1').modal('hide');
        $('.ticket-2').modal('show');
    });
	
	
	 $('#ticket-2-btn').on('click', function(){    
        $('.ticket-2').modal('hide');
        $('.ticket-3').modal('show');
    });
	
	 $('#ticket-3-btn').on('click', function(){    
        $('.ticket-3').modal('hide');
        $('.ticket-4').modal('show');
    });
	
	 $('#reg-1-btn').on('click', function(){    
        $('.reg-1').modal('hide');
        $('.reg-2').modal('show');
    });
	
	
	 $('#reg-2-btn').on('click', function(){    
        $('.reg-2').modal('hide');
        $('.reg-3').modal('show');
    });
	  $('#assign-tech').on('click', function(){    
        $('.accept-register').modal('hide');
        $('.assign-company').modal('show');
    });
	  

	
}); 


/*modal click change end here*/



/*date picker start here*/

//
//$('#timepicker-1').timepicker({
//	uiLibrary: 'bootstrap4'
//});
//$('#timepicker-2').timepicker({
//	uiLibrary: 'bootstrap4'
//});
 $('.main-header__search__toggle').click(function(){ $('body').addClass('search-open'); }); $('.main-header__searchbar__close').click(function(){ $('body').removeClass('search-open'); });

   $( document ).ready(function() {
     $(".dataTables_filter input").attr("placeholder", "Search");
   });


$('#datepicker-1').datepicker({
	uiLibrary: 'bootstrap4'
});
$('#datepicker-2').datepicker({
	uiLibrary: 'bootstrap4'
});
$('#datepicker-3').datepicker({
	uiLibrary: 'bootstrap4'
});
 $('#datepicker-4').datepicker({
            uiLibrary: 'bootstrap4'
});


 $('#datepicker-5').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-6').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-7').datepicker({
            uiLibrary: 'bootstrap4'
});
$('#datepicker-8').datepicker({
            uiLibrary: 'bootstrap4'
});
$('#datepicker-9').datepicker({
            uiLibrary: 'bootstrap4'
});
$('#datepicker-10').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-11').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-12').datepicker({
            uiLibrary: 'bootstrap4'
});
$('#datepicker-13').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-14').datepicker({
            uiLibrary: 'bootstrap4'
});

/*




 $('#datepicker-15').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-16').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-17').datepicker({
            uiLibrary: 'bootstrap4'
}); 
$('#datepicker-18').datepicker({
            uiLibrary: 'bootstrap4'
});

 $('#datepicker-19').datepicker({
            uiLibrary: 'bootstrap4'
});*/
/*date picker end here*/
