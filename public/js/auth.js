//set current_index  = 1, 0 is given for temporary hidding subscription plan ids.   mashup
current_index = 0;
currentPackage = '';

if(current_index == 0){
    $('.next-step').show();   
    $('.next-step').text("Submit");   
    $('.next-step').click(submitForm);   
}

$(function() {
    var stripe = Stripe(stripeKey);    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json',
        }
    });
    // var elements = stripe.elements();
    // var card = elements.create('card');
    // card.mount('#card-element');
});


function togglePassword(handler) {
    let elem = $('#password');
    if ('password' == $(elem).attr('type')) {
        $(elem).prop('type', 'text');
        handler.classList.add("fa-eye");
        handler.classList.remove("fa-eye-slash");
    } else {
        $(elem).prop('type', 'password');
        handler.classList.add("fa-eye-slash");
        handler.classList.remove("fa-eye");
    }
}

var loadFile = function(event) {
	$('.signup-dp-img').attr('src', URL.createObjectURL(event.target.files[0]));
};

function clearError(index){
    $(`#step${index} .error`).remove();
}

function toNext(){
    let old = current_index;
    $('#step'+current_index).hide();
    $('#step'+current_index+'-img').hide();
    ++current_index;
    console.log("current_index", current_index);
    if(current_index == 5){
        $('#step'+old).show();
    }
    else{
        $('#step'+current_index).show();        
    }
    if(old == 3){
        $('.next-step').show();   
        $('.next-step').text("Submit");   
        $('.next-step').click(submitForm);   
        // $('.prev-step').hide();   
    }

    if(old == 1) $('.prev-step').show();   
    if(old == 2) $('.next-step').hide();   
}

function submitForm(){
    console.log("Form submitted");
    $.blockUI();
    var formData = new FormData($('#regForm')[0]);
    formData.append('currentPackage', currentPackage);
    $.ajax({
        url : $('#regForm').attr('action'),
        data: formData,
        type: 'post',
        cache:false,
        contentType: false,
        processData: false,        
        success: function(response){
            toastr.success("User is registered successfully", "Success");
            if(redirectTo)
                window.location.href = redirectTo;
            window.location.reload();
            
            $.unblockUI();
        },
        error: function(error){
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                let arr = ["card_number", "expiry_month", "expiry_year", "cvv"];
                if(arr.includes(key))     
                    toastr.error(key.replace("_"," ") + " is invalid", "Error!");
                else
                    toastr.error(errors[key], "Error!");
            });
            $.unblockUI();
        }
    })
}

function toPrev(){
    let old = current_index;
    $('#step'+current_index).hide();
    --current_index;
    $('#step'+current_index).show();
    if(old == 2){
        $('.prev-step').hide();   
    }    
}


$('.next-step').on('click', function() {
    clearError(current_index);
    let error_count = 0;
    $('#step'+current_index).find('input, select, textarea').each((index, item) => {
        if($(item).is("[required]")){
            console.log(typeof $(item).attr('min'), ($(item).val()+"").length, $(item).attr('min'));
            if($(item).val() == ""){
                renderError(item);
                error_count++;           
            }
            else if(typeof $(item).attr('min') == "string" && (($(item).val()+"").length < $(item).attr('min'))){
                renderError(item, "The phone number should be atleast "+$(item).attr('min')+" characters");
                error_count++;           
            }
        }
    }); 
    if(error_count == 0){
        clearError(current_index);
        toNext();
    }
});

function renderError(item, error=""){
    let lengthCheck = (current_index == 1) ? 2 : 3;
    let errorText;
    if(current_index == 1 || current_index == 4){
        if(error) errorText = error;
        else errorText = $(item).prev('label').text().replace(/[^a-zA-Z ]/g, "") + " is required";
    }    
    else
        errorText = "Security Question answers are required";
    let error_tag = $('<p />').attr('class', 'error red').html(errorText);
    if($(item).parent().children().length == lengthCheck){
        $(item).parent().append(error_tag);         
    }
}

$('.prev-step').on('click', function() {

});