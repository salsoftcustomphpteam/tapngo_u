current_index = 0;
last_index = 6;
currentPackage = '';
if(current_index == 0){
    $('.next-step').show();   
    $('.next-step').text("Submit");   
    $('.next-step').click(submitForm);   
}



$(function() {
    var stripe = Stripe(stripeKey);    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json',
        }
    });
});


function togglePassword(handler) {
    let elem = $('#password');
    if ('password' == $(elem).attr('type')) {
        $(elem).prop('type', 'text');
        handler.classList.add("fa-eye");
        handler.classList.remove("fa-eye-slash");
    } else {
        $(elem).prop('type', 'password');
        handler.classList.add("fa-eye-slash");
        handler.classList.remove("fa-eye");
    }
}

var loadFile = function(event) {
	$('.signup-dp-img').attr('src', URL.createObjectURL(event.target.files[0]));
};

function clearError(index){
    $(`#step${index} .error`).remove();
}

function toNext(){
    let old = current_index;
    $('#step'+current_index).hide();
    $('#step'+current_index+'-img').hide();
    ++current_index;
    console.log("current_index", current_index);
    $('#step'+current_index).show();     
    if(current_index !== 1 && current_index <= last_index) 
        $('.prev-step').show();
    else
        $('.prev-step').hide();    
        
    if(current_index == last_index){
        $('.next-step').show();   
        $('.next-step').text("Submit");   
        $('.next-step').click(submitForm);   
    }        
}

function submitForm(){
    console.log("Form submitted");
    $.blockUI();

    var formData = new FormData($('#regForm')[0]);
    formData.append('currentPackage', currentPackage);
    $.ajax({
        url : $('#regForm').attr('action'),
        data: formData,
        type: 'post',
        cache:false,
        contentType: false,
        processData: false,        
        success: function(response){
            toastr.success("User is registered successfully", "Success");
            window.location.href = redirectTo;
            $.unblockUI();
        },
        error: function(error){
            console.log(error);
            let errors = error.responseJSON.errors;
            Object.keys(errors).forEach(key=>{
                $.unblockUI();
                let arr = ["card_number", "expiry_month", "expiry_year", "cvv"];
                if(arr.includes(key)) {
                    toastr.error(key.replace("_"," ") + " is invalid", "Error!");
                }else{
                    toastr.error(errors[key], "Error!");
                }
            });
        }
    })
}

function toPrev(){
    let old = current_index;
    $('#step'+current_index).hide();
    --current_index;
    $('#step'+current_index).show();
    if(current_index !== 1 && current_index <= last_index) 
        $('.prev-step').show();
    else
        $('.prev-step').hide();           
}


$('.next-step').on('click', function() {
    clearError(current_index);
    let error_count = 0;
    $('#step'+current_index).find('input, select, textarea').each((index, item) => {
        if($(item).is("[required]")){
            if($(item).val() == ""){
                renderError(item);
                error_count++;           
            }
            else if(typeof $(item).attr('min') == "string" && (($(item).val()+"").length < $(item).attr('min'))){
                renderError(item, "The phone number should be atleast "+$(item).attr('min')+" characters");
                error_count++;           
            }
            else{
                if(current_index == 4){
                    let arr = ($('#step4').find('select').map(function(index, item){  return $(item).val() })).get();
                    if(checkDuplicate(arr)){
                        error_count++;     
                        renderError(item, "Security Question should be unique");      
                    }
                }                
            }
        }
    }); 
    if(error_count == 0){
        clearError(current_index);
        toNext();
    }
});

function checkDuplicate(arr) {
    let map = {};
    let result = false;
    for(let i = 0; i < arr.length; i++) {
       // check if object contains entry with this element as key
       if(map[arr[i]]) {
          result = true;
          // terminate the loop
          break;
       }
       // add entry in object with the element as key
       map[arr[i]] = true;
    }

    return result;
}

function renderError(item, error=""){
    let lengthCheck = (current_index == 1) ? 2 : 3;
    let errorText;
    if(current_index == 4){
        if(error) errorText = error;
        else errorText = "Security Question answers are required";
    }
    else{
        if(error) errorText = error;
        else errorText = $(item).prev('label').text().replace(/[^a-zA-Z ]/g, "") + " is required";
    }
    let error_tag = $('<p />').attr('class', 'error red').html(errorText);
    $(item).parent().append(error_tag);         
}

function logoHandler(event){
    var reader = new FileReader();
    console.log(event.target)
    reader.onload = function(){
        function getFile(filePath) {
            return filePath.substr(filePath.lastIndexOf('\\') + 1).split('.')[0];
        }    
        if($('#thumb-container').children().length == 2) $('#thumb-container').children()[1].remove();
        $('#thumb-container').append(`
        <div class="col" id="thumb-box">
            <div class="thumbnail">
                <img src="${reader.result}">
                <p>${getFile(event.target.value)}</p>
            </div>
        </div>
        `);
    }
    reader.readAsDataURL(event.target.files[0]);      

}

function handleOtherDoc(){
    let document_name = $('#document_name').val();
    let other_doc = $('#other-doc').val();
    if(document_name == "" || other_doc == ""){
        toastr.error("File and Its name are required to proceed", "Error");
    }
    else{
        toastr.info("Proceed to next section", "Success");
        $("#uploadOther").modal('hide');
    }
}