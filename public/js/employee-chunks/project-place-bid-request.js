(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-place-bid-request"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Project.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/projects/Bid/Project.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['projects'],
  data: function data() {
    return {};
  },
  components: {},
  mounted: function mounted() {
    console.log(this.projects);
  },
  methods: {},
  watch: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Request.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/projects/Bid/Request.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['project'],
  data: function data() {
    return {};
  },
  components: {},
  mounted: function mounted() {
    console.log(this.project);
  },
  methods: {},
  watch: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Bid_Project__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Bid/Project */ "./resources/js/employee/views/projects/Bid/Project.vue");
/* harmony import */ var _Bid_Request__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Bid/Request */ "./resources/js/employee/views/projects/Bid/Request.vue");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var _bid;

    return {
      bid: (_bid = {
        project_id: this.$route.params.id,
        job_id: this.$route.params.jobid,
        amount: '',
        timeline: '',
        description: ''
      }, _defineProperty(_bid, "amount", ''), _defineProperty(_bid, "milestones", [{
        text: '',
        amount: ''
      }]), _bid),
      user: window.user,
      project: undefined,
      type: "outgoing"
    };
  },
  components: {
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_0__["TheMask"],
    Project: _Bid_Project__WEBPACK_IMPORTED_MODULE_1__["default"],
    Request: _Bid_Request__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  computed: {
    maxAmount: function maxAmount() {
      var sum = 0;
      if (this.bid && this.bid.milestones) this.bid.milestones.map(function (item) {
        return sum += Number(item.amount);
      });
      return sum;
    },
    job: function job() {
      var _this = this;

      if (this.project) {
        if (this.project.type == 1) {
          return this.project.jobs.filter(function (item) {
            return item.id == _this.$route.params.jobid;
          })[0];
        }
      }

      return {};
    }
  },
  mounted: function mounted() {
    this.getProject();
  },
  methods: {
    removeMilestone: function removeMilestone(index) {
      this.bid.milestones.splice(index, 1);
    },
    addMilestone: function addMilestone() {
      this.bid.milestones.push({
        text: '',
        amount: ''
      });
    },
    getProject: function getProject() {
      var _this2 = this;

      axios.get("/projects/".concat(this.$route.params.id)).then(function (_ref) {
        var data = _ref.data;
        _this2.project = data.project;
      });
    },
    placeBid: function placeBid() {
      var _this3 = this;

      axios.post("/projectsbid/", this.bid).then(function (_ref2) {
        var data = _ref2.data;

        _this3.$toastr.success(data.message, "Success !");

        _this3.$router.push({
          name: 'projects.show',
          params: {
            id: _this3.$route.params.id
          }
        });
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this3.$toastr.error(errors[key], "Error!");
        });
      });
    }
  },
  watch: {
    '$route': function $route() {
      this.projectBidRequest();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Project.vue?vue&type=template&id=67d460b4&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/projects/Bid/Project.vue?vue&type=template&id=67d460b4& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col-lg-12" }, [
      _c("div", { staticClass: "maain-tabble table-responsive" }, [
        _c(
          "table",
          {
            staticClass:
              "table text-center table-striped table-bordered zero-configuration"
          },
          [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.projects, function(request, index) {
                return _c("tr", { key: index }, [
                  _c("td", [_vm._v(_vm._s(++index))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.id))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.name))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.mini_description))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.created_date))]),
                  _vm._v(" "),
                  _c("td", [
                    _c("div", { staticClass: "btn-group mr-1 mb-1" }, [
                      _vm._m(1, true),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "dropdown-menu",
                          staticStyle: {
                            position: "absolute",
                            transform: "translate3d(0px, 21px, 0px)",
                            top: "0px",
                            left: "0px",
                            "will-change": "transform"
                          },
                          attrs: { "x-placement": "bottom-start" }
                        },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dropdown-item l-grey uppercase",
                              attrs: {
                                to: {
                                  name: "projects.bid.internal",
                                  params: { id: request.id }
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fas fa-eye" }),
                              _vm._v("View")
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ])
                ])
              }),
              0
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S.No")]),
        _vm._v(" "),
        _c("th", [_vm._v("PROJECT ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("PROJECT NAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("DESCRIPTION")]),
        _vm._v(" "),
        _c("th", [_vm._v("CREATED ON")]),
        _vm._v(" "),
        _c("th", [_vm._v("ACTION")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn dropdown-toggle btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Request.vue?vue&type=template&id=48a7cc1c&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/projects/Bid/Request.vue?vue&type=template&id=48a7cc1c& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col-lg-12" }, [
      _c("div", { staticClass: "maain-tabble table-responsive" }, [
        _c(
          "table",
          {
            staticClass:
              "table text-center table-striped table-bordered zero-configuration"
          },
          [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.project.request, function(request, index) {
                return _c("tr", { key: index }, [
                  _c("td", [_vm._v(_vm._s(++index))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.project.id))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm.project.name))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.id))]),
                  _vm._v(" "),
                  _c("td", [
                    _vm._v(_vm._s(request.requestable_type.substr(4)))
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.requestable.name))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.created_date))]),
                  _vm._v(" "),
                  Number(request.status) == 0
                    ? _c("td", [_vm._v("Pending")])
                    : Number(request.status) == 1
                    ? _c("td", [_vm._v("Accepted")])
                    : _c("td", [_vm._v("Rejected")]),
                  _vm._v(" "),
                  _c("td", [
                    _c("div", { staticClass: "btn-group mr-1 mb-1" }, [
                      _vm._m(1, true),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "dropdown-menu",
                          staticStyle: {
                            position: "absolute",
                            transform: "translate3d(0px, 21px, 0px)",
                            top: "0px",
                            left: "0px",
                            "will-change": "transform"
                          },
                          attrs: { "x-placement": "bottom-start" }
                        },
                        [
                          !_vm.id
                            ? _c(
                                "router-link",
                                {
                                  staticClass: "dropdown-item l-grey uppercase",
                                  attrs: {
                                    to: {
                                      name: "projects.bid.internal",
                                      params: { id: request.id }
                                    }
                                  }
                                },
                                [
                                  _c("i", { staticClass: "fas fa-eye" }),
                                  _vm._v("View")
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.id
                            ? _c(
                                "router-link",
                                {
                                  staticClass: "dropdown-item l-grey uppercase",
                                  attrs: {
                                    to: {
                                      name: "projects.bid.detail",
                                      params: { id: _vm.project.id }
                                    }
                                  }
                                },
                                [
                                  _c("i", { staticClass: "fas fa-eye" }),
                                  _vm._v("Detail")
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ])
                  ])
                ])
              }),
              0
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S.No")]),
        _vm._v(" "),
        _c("th", [_vm._v("PROJECT ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("PROJECT NAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("REQUEST ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("USER TYPE")]),
        _vm._v(" "),
        _c("th", [_vm._v("USER NAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("REQUESTED ON")]),
        _vm._v(" "),
        _c("th", [_vm._v("STATUS")]),
        _vm._v(" "),
        _c("th", [_vm._v("ACTION")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn dropdown-toggle btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=template&id=6c6f6da6&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=template&id=6c6f6da6& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "cstm-job-view-card" }, [
        _vm.project
          ? _c("div", { staticClass: "cstm-job-view-card-inner" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-lg-6" }, [
                  _c("h5", [_vm._v(_vm._s(_vm.project.name))])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-6" }, [
                  _vm.$route.params.jobid && _vm.job
                    ? _c("h5", { staticClass: "text-right" }, [
                        _vm._v(_vm._s(_vm.formatAmount(_vm.job.budget)))
                      ])
                    : _c("h5", { staticClass: "text-right" }, [
                        _vm._v(_vm._s(_vm.formatAmount(_vm.project.budget)))
                      ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12 mt-3" }, [
                  _c("p", [_vm._v(_vm._s(_vm.project.description))])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12 mt-3" }, [
                  _c("h5", [_vm._v("Skills Required")]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "popup-tags" },
                    _vm._l(_vm.project.skills.split(","), function(
                      skill,
                      sindex
                    ) {
                      return _c("span", { key: sindex }, [
                        _vm._v(_vm._s(skill))
                      ])
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "Project Duration: " +
                        _vm._s(_vm.project.duration) +
                        " Month"
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-lg-12 mt-3" },
                  [
                    _c("h5", [_vm._v("Attachements")]),
                    _vm._v(" "),
                    _vm._l(_vm.project.media, function(media, mindex) {
                      return _c(
                        "span",
                        {
                          key: mindex,
                          staticClass: "project-attach-cv bg-light",
                          staticStyle: { cursor: "pointer" },
                          on: {
                            click: function($event) {
                              return _vm.openLink(media.getFullUrl)
                            }
                          }
                        },
                        [
                          _c("img", {
                            attrs: {
                              src: _vm.getIconByMimeType(media.mime_type),
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                            " +
                              _vm._s(media.file_name) +
                              "\n                        "
                          )
                        ]
                      )
                    })
                  ],
                  2
                ),
                _vm._v(" "),
                _vm.$route.params.jobid && _vm.job
                  ? _c("div", { staticClass: "col-lg-12 mt-3 d-flex" }, [
                      _c("div", { staticClass: "mr-3" }, [
                        _c("h5", [
                          _vm._v("Job ID: " + _vm._s(_vm.$route.params.jobid))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mr-3" }, [
                        _c("h5", [
                          _vm._v("Job Title : " + _vm._s(_vm.job.title))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mr-3" }, [
                        _c("h5", [
                          _vm._v("Job Category: " + _vm._s(_vm.job.category))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mr-3" }, [
                        _c("h5", [
                          _vm._v(
                            "Job Duration: " +
                              _vm._s(_vm.job.duration) +
                              " Month"
                          )
                        ])
                      ])
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "row" },
                [
                  _c("div", { staticClass: "col-lg-4" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Bid Amount")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.bid.amount,
                            expression: "bid.amount"
                          }
                        ],
                        staticClass: "new-input",
                        attrs: { type: "number", placeholder: "$" },
                        domProps: { value: _vm.bid.amount },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.bid, "amount", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-4" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [
                        _vm._v("This project will be delievered in")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.bid.timeline,
                            expression: "bid.timeline"
                          }
                        ],
                        staticClass: "new-input",
                        attrs: { type: "number", placeholder: "In Days" },
                        domProps: { value: _vm.bid.timeline },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.bid, "timeline", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-12" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Describe your proposal")]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.bid.description,
                            expression: "bid.description"
                          }
                        ],
                        staticClass: "txtara",
                        attrs: {
                          placeholder:
                            "What makes you the best candidate for this project?"
                        },
                        domProps: { value: _vm.bid.description },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.bid,
                              "description",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.bid.milestones, function(bid, bidindex) {
                    return _c("fragment", { key: bidindex }, [
                      _c("div", { staticClass: "col-lg-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Add Milestones")]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: bid.text,
                                expression: "bid.text"
                              }
                            ],
                            staticClass: "new-input",
                            attrs: {
                              type: "text",
                              placeholder: "Add Milestones"
                            },
                            domProps: { value: bid.text },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(bid, "text", $event.target.value)
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-4" }, [
                        _c("div", { staticClass: "form-group mt-3" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: bid.amount,
                                expression: "bid.amount"
                              }
                            ],
                            staticClass: "new-input  mb-2",
                            attrs: { type: "number", placeholder: "$" },
                            domProps: { value: bid.amount },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(bid, "amount", $event.target.value)
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-4" }, [
                        _c("div", { staticClass: "form-group mt-3" }, [
                          _c(
                            "a",
                            {
                              staticClass: "add-project",
                              attrs: { href: "javascript:;" },
                              on: {
                                click: function($event) {
                                  return _vm.removeMilestone(bidindex)
                                }
                              }
                            },
                            [
                              _vm._v(
                                "Remove\n                                    Milestone\n                                    "
                              ),
                              _c("i", { staticClass: "fas fa-plus-circle" })
                            ]
                          )
                        ])
                      ])
                    ])
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-4 text-right" }),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-4" }, [
                    _vm.bid.amount > 0
                      ? _c("label", { staticClass: "pl-2" }, [
                          _vm._v("Milestone Total: $" + _vm._s(_vm.maxAmount))
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-12" }, [
                    _c(
                      "a",
                      {
                        staticClass: "add-project",
                        attrs: { href: "javascript:;" },
                        on: {
                          click: function($event) {
                            return _vm.addMilestone()
                          }
                        }
                      },
                      [
                        _vm._v("Add Milestone\n                            "),
                        _c("i", { staticClass: "fas fa-plus-circle" })
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-12" }, [
                    _c(
                      "button",
                      {
                        staticClass: "main-blue-btn fa-pull-right",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.placeBid()
                          }
                        }
                      },
                      [_vm._v("Place\n                            Bid")]
                    )
                  ])
                ],
                2
              )
            ])
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-the-mask/dist/vue-the-mask.js":
/*!********************************************************!*\
  !*** ./node_modules/vue-the-mask/dist/vue-the-mask.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function(e,t){ true?module.exports=t():undefined})(this,function(){return function(e){function t(r){if(n[r])return n[r].exports;var a=n[r]={i:r,l:!1,exports:{}};return e[r].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,r){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:r})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p=".",t(t.s=10)}([function(e,t){e.exports={"#":{pattern:/\d/},X:{pattern:/[0-9a-zA-Z]/},S:{pattern:/[a-zA-Z]/},A:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleUpperCase()}},a:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleLowerCase()}},"!":{escape:!0}}},function(e,t,n){"use strict";function r(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!0),t}var a=n(2),o=n(0),i=n.n(o);t.a=function(e,t){var o=t.value;if((Array.isArray(o)||"string"==typeof o)&&(o={mask:o,tokens:i.a}),"INPUT"!==e.tagName.toLocaleUpperCase()){var u=e.getElementsByTagName("input");if(1!==u.length)throw new Error("v-mask directive requires 1 input, found "+u.length);e=u[0]}e.oninput=function(t){if(t.isTrusted){var i=e.selectionEnd,u=e.value[i-1];for(e.value=n.i(a.a)(e.value,o.mask,!0,o.tokens);i<e.value.length&&e.value.charAt(i-1)!==u;)i++;e===document.activeElement&&(e.setSelectionRange(i,i),setTimeout(function(){e.setSelectionRange(i,i)},0)),e.dispatchEvent(r("input"))}};var s=n.i(a.a)(e.value,o.mask,!0,o.tokens);s!==e.value&&(e.value=s,e.dispatchEvent(r("input")))}},function(e,t,n){"use strict";var r=n(6),a=n(5);t.a=function(e,t){var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=arguments[3];return Array.isArray(t)?n.i(a.a)(r.a,t,i)(e,t,o,i):n.i(r.a)(e,t,o,i)}},function(e,t,n){"use strict";function r(e){e.component(s.a.name,s.a),e.directive("mask",i.a)}Object.defineProperty(t,"__esModule",{value:!0});var a=n(0),o=n.n(a),i=n(1),u=n(7),s=n.n(u);n.d(t,"TheMask",function(){return s.a}),n.d(t,"mask",function(){return i.a}),n.d(t,"tokens",function(){return o.a}),n.d(t,"version",function(){return c});var c="0.11.1";t.default=r,"undefined"!=typeof window&&window.Vue&&window.Vue.use(r)},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=n(1),a=n(0),o=n.n(a),i=n(2);t.default={name:"TheMask",props:{value:[String,Number],mask:{type:[String,Array],required:!0},masked:{type:Boolean,default:!1},tokens:{type:Object,default:function(){return o.a}}},directives:{mask:r.a},data:function(){return{lastValue:null,display:this.value}},watch:{value:function(e){e!==this.lastValue&&(this.display=e)},masked:function(){this.refresh(this.display)}},computed:{config:function(){return{mask:this.mask,tokens:this.tokens,masked:this.masked}}},methods:{onInput:function(e){e.isTrusted||this.refresh(e.target.value)},refresh:function(e){this.display=e;var e=n.i(i.a)(e,this.mask,this.masked,this.tokens);e!==this.lastValue&&(this.lastValue=e,this.$emit("input",e))}}}},function(e,t,n){"use strict";function r(e,t,n){return t=t.sort(function(e,t){return e.length-t.length}),function(r,a){for(var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=0;i<t.length;){var u=t[i];i++;var s=t[i];if(!(s&&e(r,s,!0,n).length>u.length))return e(r,u,o,n)}return""}}t.a=r},function(e,t,n){"use strict";function r(e,t){var n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],r=arguments[3];e=e||"",t=t||"";for(var a=0,o=0,i="";a<t.length&&o<e.length;){var u=t[a],s=r[u],c=e[o];s&&!s.escape?(s.pattern.test(c)&&(i+=s.transform?s.transform(c):c,a++),o++):(s&&s.escape&&(a++,u=t[a]),n&&(i+=u),c===u&&o++,a++)}for(var f="";a<t.length&&n;){var u=t[a];if(r[u]){f="";break}f+=u,a++}return i+f}t.a=r},function(e,t,n){var r=n(8)(n(4),n(9),null,null);e.exports=r.exports},function(e,t){e.exports=function(e,t,n,r){var a,o=e=e||{},i=typeof e.default;"object"!==i&&"function"!==i||(a=e,o=e.default);var u="function"==typeof o?o.options:o;if(t&&(u.render=t.render,u.staticRenderFns=t.staticRenderFns),n&&(u._scopeId=n),r){var s=u.computed||(u.computed={});Object.keys(r).forEach(function(e){var t=r[e];s[e]=function(){return t}})}return{esModule:a,exports:o,options:u}}},function(e,t){e.exports={render:function(){var e=this,t=e.$createElement;return(e._self._c||t)("input",{directives:[{name:"mask",rawName:"v-mask",value:e.config,expression:"config"}],attrs:{type:"text"},domProps:{value:e.display},on:{input:e.onInput}})},staticRenderFns:[]}},function(e,t,n){e.exports=n(3)}])});

/***/ }),

/***/ "./resources/js/employee/views/projects/Bid/Project.vue":
/*!**************************************************************!*\
  !*** ./resources/js/employee/views/projects/Bid/Project.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Project_vue_vue_type_template_id_67d460b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Project.vue?vue&type=template&id=67d460b4& */ "./resources/js/employee/views/projects/Bid/Project.vue?vue&type=template&id=67d460b4&");
/* harmony import */ var _Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Project.vue?vue&type=script&lang=js& */ "./resources/js/employee/views/projects/Bid/Project.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Project_vue_vue_type_template_id_67d460b4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Project_vue_vue_type_template_id_67d460b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/employee/views/projects/Bid/Project.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/employee/views/projects/Bid/Project.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/employee/views/projects/Bid/Project.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Project.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Project.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/employee/views/projects/Bid/Project.vue?vue&type=template&id=67d460b4&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/employee/views/projects/Bid/Project.vue?vue&type=template&id=67d460b4& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_template_id_67d460b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Project.vue?vue&type=template&id=67d460b4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Project.vue?vue&type=template&id=67d460b4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_template_id_67d460b4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_template_id_67d460b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/employee/views/projects/Bid/Request.vue":
/*!**************************************************************!*\
  !*** ./resources/js/employee/views/projects/Bid/Request.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Request_vue_vue_type_template_id_48a7cc1c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Request.vue?vue&type=template&id=48a7cc1c& */ "./resources/js/employee/views/projects/Bid/Request.vue?vue&type=template&id=48a7cc1c&");
/* harmony import */ var _Request_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Request.vue?vue&type=script&lang=js& */ "./resources/js/employee/views/projects/Bid/Request.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Request_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Request_vue_vue_type_template_id_48a7cc1c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Request_vue_vue_type_template_id_48a7cc1c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/employee/views/projects/Bid/Request.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/employee/views/projects/Bid/Request.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/employee/views/projects/Bid/Request.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Request_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Request.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Request.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Request_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/employee/views/projects/Bid/Request.vue?vue&type=template&id=48a7cc1c&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/employee/views/projects/Bid/Request.vue?vue&type=template&id=48a7cc1c& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Request_vue_vue_type_template_id_48a7cc1c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Request.vue?vue&type=template&id=48a7cc1c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/Bid/Request.vue?vue&type=template&id=48a7cc1c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Request_vue_vue_type_template_id_48a7cc1c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Request_vue_vue_type_template_id_48a7cc1c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/employee/views/projects/PlaceBidComponent.vue":
/*!********************************************************************!*\
  !*** ./resources/js/employee/views/projects/PlaceBidComponent.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PlaceBidComponent_vue_vue_type_template_id_6c6f6da6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PlaceBidComponent.vue?vue&type=template&id=6c6f6da6& */ "./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=template&id=6c6f6da6&");
/* harmony import */ var _PlaceBidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PlaceBidComponent.vue?vue&type=script&lang=js& */ "./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PlaceBidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PlaceBidComponent_vue_vue_type_template_id_6c6f6da6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PlaceBidComponent_vue_vue_type_template_id_6c6f6da6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/employee/views/projects/PlaceBidComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlaceBidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PlaceBidComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlaceBidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=template&id=6c6f6da6&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=template&id=6c6f6da6& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlaceBidComponent_vue_vue_type_template_id_6c6f6da6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PlaceBidComponent.vue?vue&type=template&id=6c6f6da6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/projects/PlaceBidComponent.vue?vue&type=template&id=6c6f6da6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlaceBidComponent_vue_vue_type_template_id_6c6f6da6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlaceBidComponent_vue_vue_type_template_id_6c6f6da6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);