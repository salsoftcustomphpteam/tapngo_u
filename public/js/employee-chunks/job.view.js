(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["job.view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/job/ViewComponent.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/job/ViewComponent.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_moments_ago__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-moments-ago */ "./node_modules/vue-moments-ago/src/main.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VueMomentsAgo: vue_moments_ago__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      baseUrl: window.base_url,
      user: window.user,
      job: ""
    };
  },
  mounted: function mounted() {
    this.fetchJob();
  },
  methods: {
    fetchJob: function fetchJob() {
      var _this = this;

      axios.get("/jobs/".concat(this.$route.params.id)).then(function (_ref) {
        var data = _ref.data;
        _this.job = data.task;
        _this.user = data.me;
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/job/ViewComponent.vue?vue&type=template&id=109079a2&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/job/ViewComponent.vue?vue&type=template&id=109079a2& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _vm.job
        ? _c("div", { staticClass: "cstm-job-view-card" }, [
            _c("div", { staticClass: "cstm-job-view-card-inner" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-lg-7" }, [
                  _c("h3", { staticClass: "table-h3" }, [
                    _vm._v(_vm._s(_vm.job.title))
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-5" }, [
                  _c("p", { staticClass: "text-right" }, [
                    _vm._v("STATUS REQUEST:\n                            "),
                    _vm.job.status == 0
                      ? _c("span", { staticClass: "alert-success" }, [
                          _vm._v("Pending")
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.job.status == 1
                      ? _c("span", { staticClass: "alert-success" }, [
                          _vm._v("Completed")
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.job.status == 2
                      ? _c("span", { staticClass: "alert-success" }, [
                          _vm._v("Rejected")
                        ])
                      : _vm._e()
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-lg-2" }, [
                  _c("div", { staticClass: "d-flex align-items-center mb-4" }, [
                    _c("img", {
                      staticClass: "common-pro-img-small",
                      attrs: { src: _vm.user.organization.image, alt: "" }
                    }),
                    _vm._v(" "),
                    _c("h5", [
                      _vm._v(
                        "Assigned By: " + _vm._s(_vm.user.organization.name)
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12" }, [
                  _c("h5", [_vm._v("Due Date")]),
                  _vm._v(" "),
                  _c(
                    "p",
                    [
                      _c("vue-moments-ago", {
                        attrs: {
                          prefix: "",
                          suffix: "ago",
                          date: _vm.job.due,
                          lang: "en"
                        }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12" }, [
                  _c("h5", [_vm._v("Project")]),
                  _vm._v(" "),
                  _c("p", [_vm._v(_vm._s(_vm.job.project.name))])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12" }, [
                  _c("p", [_vm._v(_vm._s(_vm.job.description))])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-lg-12" },
                  [
                    _c("h5", [_vm._v("Attachements")]),
                    _vm._v(" "),
                    _vm._l(_vm.job.media, function(med, mindex) {
                      return _c(
                        "span",
                        { key: mindex, staticClass: "project-attach-cv" },
                        [
                          _c("img", {
                            attrs: {
                              src: _vm.getIconByMimeType(med.mime_type),
                              alt: ""
                            }
                          }),
                          _vm._v(
                            _vm._s(med.file_name) +
                              "\n                            "
                          ),
                          _c(
                            "a",
                            { attrs: { href: med.full_url, download: "" } },
                            [_c("i", { staticClass: "fas fa-eye" })]
                          )
                        ]
                      )
                    })
                  ],
                  2
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-12 comment-section" },
                  [
                    _c("h5", [_vm._v("Comments: ")]),
                    _vm._v(" "),
                    _vm._l(_vm.job.comments, function(com, cindex) {
                      return _c(
                        "div",
                        { key: cindex, staticClass: "mt-2 cstm-job-view-card" },
                        [
                          _c("h5", [_vm._v(_vm._s(com.commentaddable.name))]),
                          _vm._v(" "),
                          _c(
                            "small",
                            [
                              _c("vue-moments-ago", {
                                attrs: {
                                  prefix: "posted at ",
                                  suffix: "ago",
                                  date: com.created_at,
                                  lang: "en"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("p", { staticClass: "mt-2" }, [
                            _vm._v(_vm._s(com.comment))
                          ])
                        ]
                      )
                    })
                  ],
                  2
                )
              ])
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/employee/views/job/ViewComponent.vue":
/*!***********************************************************!*\
  !*** ./resources/js/employee/views/job/ViewComponent.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ViewComponent_vue_vue_type_template_id_109079a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ViewComponent.vue?vue&type=template&id=109079a2& */ "./resources/js/employee/views/job/ViewComponent.vue?vue&type=template&id=109079a2&");
/* harmony import */ var _ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ViewComponent.vue?vue&type=script&lang=js& */ "./resources/js/employee/views/job/ViewComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ViewComponent_vue_vue_type_template_id_109079a2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ViewComponent_vue_vue_type_template_id_109079a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/employee/views/job/ViewComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/employee/views/job/ViewComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/employee/views/job/ViewComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ViewComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/job/ViewComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/employee/views/job/ViewComponent.vue?vue&type=template&id=109079a2&":
/*!******************************************************************************************!*\
  !*** ./resources/js/employee/views/job/ViewComponent.vue?vue&type=template&id=109079a2& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_109079a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ViewComponent.vue?vue&type=template&id=109079a2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/job/ViewComponent.vue?vue&type=template&id=109079a2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_109079a2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_109079a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);