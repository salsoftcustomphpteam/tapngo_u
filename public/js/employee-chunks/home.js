(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/IndexComponent.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/home/IndexComponent.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Landing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Landing */ "./resources/js/employee/views/home/Landing.vue");
/* harmony import */ var _Timeline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Timeline */ "./resources/js/employee/views/home/Timeline.vue");
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Landing: _Landing__WEBPACK_IMPORTED_MODULE_0__["default"],
    Timeline: _Timeline__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      baseUrl: window.base_url,
      user: window.user
    };
  },
  mounted: function mounted() {},
  methods: {},
  watch: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/Landing.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/home/Landing.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      baseUrl: window.base_url,
      email: ''
    };
  },
  mounted: function mounted() {},
  methods: {
    sendQuery: function sendQuery() {
      var _this = this;

      var formData = new FormData();
      formData.append("email", this.email);
      axios.post('subscribe/store', formData).then(function (_ref) {
        var data = _ref.data;

        _this.$toastr.success('Subscribed to News Letter', "Success !");

        $('.email-subscribe').val('');
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this.$toastr.error(errors[key], "Error!");
        });
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/IndexComponent.vue?vue&type=template&id=755d9e52&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/home/IndexComponent.vue?vue&type=template&id=755d9e52& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("Landing")
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/Landing.vue?vue&type=template&id=bee8ef8a&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/employee/views/home/Landing.vue?vue&type=template&id=bee8ef8a& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", [
    _c("section", { staticClass: "banner" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row align-items-center" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-6" }, [
            _c(
              "div",
              { staticClass: "image-wrapper" },
              [
                _c("Img", {
                  staticClass: "banner-image",
                  attrs: { src: "/users/images/banner-img.png", alt: "" }
                })
              ],
              1
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "section-2" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-10 col-md-12 offset-lg-2" }, [
            _c("div", { staticClass: "inner-wrapper-sec" }, [
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "tab-content-wrapper" }, [
                _c(
                  "div",
                  {
                    staticClass: "tab-content",
                    attrs: { id: "v-pills-tabContent" }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "tab-pane fade show active",
                        attrs: {
                          id: "v-pills-home",
                          role: "tabpanel",
                          "aria-labelledby": "v-pills-home-tab"
                        }
                      },
                      [
                        _c("Img", {
                          staticClass: "tab-pic",
                          attrs: { src: "/users/images/tab-pic.png", alt: "" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "tab-pane fade",
                        attrs: {
                          id: "v-pills-profile",
                          role: "tabpanel",
                          "aria-labelledby": "v-pills-profile-tab"
                        }
                      },
                      [
                        _c("Img", {
                          staticClass: "tab-pic",
                          attrs: { src: "/users/images/tab-pic.png", alt: "" }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "tab-pane fade",
                        attrs: {
                          id: "v-pills-messages",
                          role: "tabpanel",
                          "aria-labelledby": "v-pills-messages-tab"
                        }
                      },
                      [
                        _c("Img", {
                          staticClass: "tab-pic",
                          attrs: { src: "/users/images/tab-pic.png", alt: "" }
                        })
                      ],
                      1
                    )
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "slider-section" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12 p-0" }, [
            _c("div", { staticClass: "wrap-slider" }, [
              _c("h1", { staticClass: "heading white-txt" }, [
                _vm._v(
                  "\n                            Popular Professional Services\n                        "
                )
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "popular-services owl-carousel owl-loaded owl-drag"
                },
                [
                  _c("div", { staticClass: "owl-stage-outer" }, [
                    _c(
                      "div",
                      {
                        staticClass: "owl-stage",
                        staticStyle: {
                          transform: "translate3d(-2655px, 0px, 0px)",
                          transition: "all 0.25s ease 0s",
                          width: "4552px"
                        }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-4.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(2)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-3.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(3)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-1.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(4)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-1.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(5)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-2.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(6)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-3.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(7)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-4.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(8)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-3.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(9)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item active",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-1.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(10)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned active",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-1.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(11)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-2.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(12)
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "owl-item cloned",
                            staticStyle: { width: "379.333px" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "item-service" },
                              [
                                _c("Img", {
                                  attrs: {
                                    src: "/users/images/slider-3.png",
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(13)
                              ],
                              1
                            )
                          ]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _vm._m(14),
                  _vm._v(" "),
                  _vm._m(15)
                ]
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "newsletter" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12" }, [
            _c(
              "div",
              { staticClass: "newsletter-wrapper" },
              [
                _c("div", { staticClass: "inner-w" }, [
                  _c("h1", { staticClass: "heading" }, [
                    _vm._v(
                      "\n                                Subscribe to our News Letter\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "banner-text" }, [
                    _vm._v(
                      "\n                                It is a long established fact that a reader will be distracted by the readable\n                                content of a page when looking at its layout The point of using.\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("form", { attrs: { action: "" } }, [
                    _c("div", { staticClass: "form-wr" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.email,
                            expression: "email"
                          }
                        ],
                        staticClass: "email-subscribe",
                        attrs: { name: "email", type: "email", required: "" },
                        domProps: { value: _vm.email },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.email = $event.target.value
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "news-send",
                          attrs: { type: "button" },
                          on: { click: _vm.sendQuery }
                        },
                        [_vm._v("Subscribe")]
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("Img", {
                  staticClass: "newsgirl",
                  attrs: { src: "/users/images/newsletter-girl.png", alt: "" }
                })
              ],
              1
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-6 p-0 bg-color-1" }, [
      _c("div", { staticClass: "banner-content" }, [
        _c("h1", { staticClass: "heading" }, [
          _vm._v("Find the Perfect Service "),
          _c("br"),
          _vm._v("\n                            for Your Business")
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "banner-text" }, [
          _vm._v(
            "\n                            It is a long established fact that a reader will be distracted\n                            by the readable content of a page when looking at its layout.\n                            The point of using.\n                        "
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "wrapper-content-section-2" }, [
      _c("h1", { staticClass: "heading" }, [
        _vm._v("Welcome to Our Community")
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "banner-text" }, [
        _vm._v(
          "It is a long established fact that a reader will be distracted by the\n                                readable\n                                content of a page when looking at its layout The point of using."
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "nav flex-column nav-pills",
          attrs: {
            id: "v-pills-tab",
            role: "tablist",
            "aria-orientation": "vertical"
          }
        },
        [
          _c(
            "a",
            {
              staticClass: "nav-link tab-linkz active",
              attrs: {
                id: "v-pills-home-tab",
                "data-toggle": "pill",
                href: "#v-pills-home",
                role: "tab",
                "aria-controls": "v-pills-home",
                "aria-selected": "true"
              }
            },
            [_vm._v("Search for a Job")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "nav-link tab-linkz",
              attrs: {
                id: "v-pills-profile-tab",
                "data-toggle": "pill",
                href: "#v-pills-profile",
                role: "tab",
                "aria-controls": "v-pills-profile",
                "aria-selected": "false"
              }
            },
            [
              _vm._v("Connect with Business "),
              _c("br"),
              _vm._v("\n                                    Organizations.")
            ]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "nav-link tab-linkz",
              attrs: {
                id: "v-pills-messages-tab",
                "data-toggle": "pill",
                href: "#v-pills-messages",
                role: "tab",
                "aria-controls": "v-pills-messages",
                "aria-selected": "false"
              }
            },
            [
              _vm._v("Initiate your Own "),
              _c("br"),
              _vm._v("\n                                    Projects.")
            ]
          )
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "black" }, [_vm._v("Web Designing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "yellow" }, [_vm._v("Project Management")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "red" }, [_vm._v("Logo Designing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "red" }, [_vm._v("Logo Designing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "light-blue" }, [_vm._v("Digital Marketing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "yellow" }, [_vm._v("Project Management")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "black" }, [_vm._v("Web Designing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "yellow" }, [_vm._v("Project Management")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "red" }, [_vm._v("Logo Designing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "red" }, [_vm._v("Logo Designing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "light-blue" }, [_vm._v("Digital Marketing")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "serv-wrapper" }, [
      _c("p", { staticClass: "yellow" }, [_vm._v("Project Management")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "owl-nav" }, [
      _c(
        "button",
        {
          staticClass: "owl-prev",
          attrs: { type: "button", role: "presentation" }
        },
        [_c("i", { staticClass: "fas fa-long-arrow-alt-left" })]
      ),
      _c(
        "button",
        {
          staticClass: "owl-next",
          attrs: { type: "button", role: "presentation" }
        },
        [_c("i", { staticClass: "fas fa-long-arrow-alt-right" })]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "owl-dots" }, [
      _c("button", { staticClass: "owl-dot", attrs: { role: "button" } }, [
        _c("span")
      ]),
      _c(
        "button",
        { staticClass: "owl-dot active", attrs: { role: "button" } },
        [_c("span")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/employee/views/home/IndexComponent.vue":
/*!*************************************************************!*\
  !*** ./resources/js/employee/views/home/IndexComponent.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IndexComponent_vue_vue_type_template_id_755d9e52___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IndexComponent.vue?vue&type=template&id=755d9e52& */ "./resources/js/employee/views/home/IndexComponent.vue?vue&type=template&id=755d9e52&");
/* harmony import */ var _IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./IndexComponent.vue?vue&type=script&lang=js& */ "./resources/js/employee/views/home/IndexComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _IndexComponent_vue_vue_type_template_id_755d9e52___WEBPACK_IMPORTED_MODULE_0__["render"],
  _IndexComponent_vue_vue_type_template_id_755d9e52___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/employee/views/home/IndexComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/employee/views/home/IndexComponent.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/employee/views/home/IndexComponent.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/IndexComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/employee/views/home/IndexComponent.vue?vue&type=template&id=755d9e52&":
/*!********************************************************************************************!*\
  !*** ./resources/js/employee/views/home/IndexComponent.vue?vue&type=template&id=755d9e52& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_template_id_755d9e52___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexComponent.vue?vue&type=template&id=755d9e52& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/IndexComponent.vue?vue&type=template&id=755d9e52&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_template_id_755d9e52___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_template_id_755d9e52___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/employee/views/home/Landing.vue":
/*!******************************************************!*\
  !*** ./resources/js/employee/views/home/Landing.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Landing_vue_vue_type_template_id_bee8ef8a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Landing.vue?vue&type=template&id=bee8ef8a& */ "./resources/js/employee/views/home/Landing.vue?vue&type=template&id=bee8ef8a&");
/* harmony import */ var _Landing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Landing.vue?vue&type=script&lang=js& */ "./resources/js/employee/views/home/Landing.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Landing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Landing_vue_vue_type_template_id_bee8ef8a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Landing_vue_vue_type_template_id_bee8ef8a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/employee/views/home/Landing.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/employee/views/home/Landing.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/employee/views/home/Landing.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Landing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Landing.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/Landing.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Landing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/employee/views/home/Landing.vue?vue&type=template&id=bee8ef8a&":
/*!*************************************************************************************!*\
  !*** ./resources/js/employee/views/home/Landing.vue?vue&type=template&id=bee8ef8a& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Landing_vue_vue_type_template_id_bee8ef8a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Landing.vue?vue&type=template&id=bee8ef8a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/employee/views/home/Landing.vue?vue&type=template&id=bee8ef8a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Landing_vue_vue_type_template_id_bee8ef8a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Landing_vue_vue_type_template_id_bee8ef8a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);