(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-profile-business-request-verification"],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973&":
/*!*************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973& ***!
  \*************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { attrs: { id: "step3" } }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-8" }, [
            _c("div", { staticClass: "row sign-up-line" }, [
              _vm._m(1),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-12" }, [
                _c("div", { staticClass: "form-group" }, [
                  _vm._m(3),
                  _vm._v(" "),
                  _c("ul", { staticClass: "col-12" }, [
                    _vm._v(
                      "\n                            @foreach(config('app.documents') as $doc)\n                            "
                    ),
                    _c("li", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-4" }, [
                        _c(
                          "button",
                          {
                            staticClass: "main-blue-btn2 mr-2",
                            attrs: { type: "button", onclick: "" }
                          },
                          [_vm._v(_vm._s(_vm.$doc))]
                        )
                      ]),
                      _vm._v(" "),
                      _vm._m(4)
                    ]),
                    _vm._v(
                      "\n                            @endforeach\n                        "
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "pb-4 add-other-rqst-vari",
                    attrs: {
                      href: "javascript:;",
                      "data-toggle": "modal",
                      "data-target": "#uploadOther"
                    }
                  },
                  [_vm._v("Add Another")]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "clearfix" })
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _vm._m(5)
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "signup-text-main" }, [
      _c("h3", [_vm._v("Request Verification")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-12" }, [
      _c("p", [_vm._v("Apply for Conek Pro Verification:")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "It is a long established fact that a reader will be distracted by the readable content of a page\n                        when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal\n                        distribution of letters, as opposed to using 'Content here, content here', making it look like\n                        readable English."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-12" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", [
          _vm._v("Company Official Name: "),
          _c("span", [_vm._v("*")])
        ]),
        _vm._v(" "),
        _c("input", {
          staticClass: "new-input",
          attrs: { type: "text", name: "company_name", required: "" }
        })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Please attach following documents:"),
      _c("span", [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-8" }, [
      _c("input", {
        staticClass: "pt-4",
        attrs: { required: "", type: "file", name: "document" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "uploadOther",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "uploadOther",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered ",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _c(
                "button",
                {
                  staticClass: "close text-right mr-1 mt-1",
                  attrs: {
                    type: "button",
                    "data-dismiss": "modal",
                    "aria-label": "Close"
                  }
                },
                [
                  _c("span", { attrs: { "aria-hidden": "true" } }, [
                    _vm._v("×")
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "p-4" }, [
                _c("h5", [_vm._v("Upload Document")]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group mt-4 mb-0" }, [
                  _c("input", {
                    staticClass: "new-input",
                    attrs: {
                      type: "text",
                      placeholder: "Enter Document Name",
                      id: "document_name",
                      name: "document_name"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c(
                    "button",
                    {
                      staticClass: "other-doc-btn main-blue-btn2 mr-2",
                      attrs: {
                        type: "button",
                        onclick: "document.getElementById('other-doc').click()"
                      }
                    },
                    [_vm._v("Upload File")]
                  ),
                  _vm._v(" "),
                  _c("input", {
                    staticStyle: { display: "none" },
                    attrs: {
                      onchange:
                        "$(this).next('p').text($(this).val().replace(/C:\\\\fakepath\\\\/i, ''))",
                      type: "file",
                      name: "document[other]",
                      id: "other-doc"
                    }
                  }),
                  _vm._v(" "),
                  _c("p")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "pull-right col-12 text-right" }, [
                  _c(
                    "button",
                    {
                      staticClass: "main-blue-btn text-right",
                      attrs: { type: "button", onclick: "handleOtherDoc()" }
                    },
                    [_vm._v("Upload")]
                  )
                ])
              ])
            ])
          ]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue":
/*!************************************************************************************************!*\
  !*** ./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditProfileBusinessRequestVerificationComponent_vue_vue_type_template_id_5bc3a973___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973& */ "./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _EditProfileBusinessRequestVerificationComponent_vue_vue_type_template_id_5bc3a973___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditProfileBusinessRequestVerificationComponent_vue_vue_type_template_id_5bc3a973___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973& ***!
  \*******************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileBusinessRequestVerificationComponent_vue_vue_type_template_id_5bc3a973___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/static/EditProfileBusinessRequestVerificationComponent.vue?vue&type=template&id=5bc3a973&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileBusinessRequestVerificationComponent_vue_vue_type_template_id_5bc3a973___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileBusinessRequestVerificationComponent_vue_vue_type_template_id_5bc3a973___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);