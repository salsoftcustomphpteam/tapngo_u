(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-bid"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id'],
  data: function data() {
    return {
      projects: []
    };
  },
  components: {},
  mounted: function mounted() {
    this.getIncommingProjectBidRequest();
  },
  methods: {
    getIncommingProjectBidRequest: function getIncommingProjectBidRequest() {
      var _this$id,
          _this = this;

      axios.get("projectsbid/incomming?id=".concat((_this$id = this.id) !== null && _this$id !== void 0 ? _this$id : '')).then(function (_ref) {
        var data = _ref.data;
        _this.projects = data;
      });
    },
    performAction: function performAction(action, projectId) {
      var _this2 = this;

      axios.get("projects/incoming/".concat(action, "/").concat(projectId)).then(function (_ref2) {
        var data = _ref2.data;

        if (action == 'accept') {
          $('#status').text('');
          $('#status').text('Accepted');
          $('.action-td').html('');
          $('.action-data-td').html('');

          _this2.$toastr.success('Project accepted.');
        } else {
          $('#status').text('');
          $('#status').text('Rejected');
          $('.action-td').html('');
          $('.action-data-td').html('');

          _this2.$toastr.success('Project rejected.');
        }

        console.log(data); // window.location.reload();
        //this.$emit('refetch');
      });
    }
  },
  watch: {
    "$route": function $route() {
      this.getIncommingProjectBidRequest();
    },
    "id": function id() {
      this.getIncommingProjectBidRequest();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id'],
  data: function data() {
    return {
      projects: []
    };
  },
  components: {},
  mounted: function mounted() {
    this.getOutgoingProjectBidRequest();
  },
  methods: {
    getOutgoingProjectBidRequest: function getOutgoingProjectBidRequest() {
      var _this$id,
          _this = this;

      axios.get("projectsbid/outgoing?id=".concat((_this$id = this.id) !== null && _this$id !== void 0 ? _this$id : '')).then(function (_ref) {
        var data = _ref.data;
        _this.projects = data;
      });
    }
  },
  watch: {
    "$route": function $route() {
      this.getOutgoingProjectBidRequest();
    },
    "id": function id() {
      this.getOutgoingProjectBidRequest();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/BidComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/BidComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Bid_Incomming__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Bid/Incomming */ "./resources/js/business/views/projects/Bid/Incomming.vue");
/* harmony import */ var _Bid_Outgoing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Bid/Outgoing */ "./resources/js/business/views/projects/Bid/Outgoing.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // import Project from './Bid/Project';
// import Request from './Bid/Request';

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      id: this.$route.params.id,
      projects: [],
      project: undefined,
      type: this.$route.query.type == "incomming" ? "incomming" : "outgoing"
    };
  },
  components: {
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_0__["TheMask"],
    Incomming: _Bid_Incomming__WEBPACK_IMPORTED_MODULE_1__["default"],
    Outgoing: _Bid_Outgoing__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  mounted: function mounted() {
    this.projectBidRequest();
  },
  methods: {
    projectBidRequest: function projectBidRequest() {// let url = `/projects/bid-requests/all?type=${this.type}`;
      // if(this.$route.params.id)
      //     url = `/projects/bid-requests/${this.$route.params.id}/all?type=${this.type}`;
      // axios.get(url)
      //     .then(data => {
      //         if(this.$route.params.id) 
      //             this.project = data.data;
      //         else
      //             this.projects = data.data;
      //     });
    }
  },
  watch: {
    'type': function type() {
      this.projectBidRequest();
    },
    '$route': function $route() {
      this.projectBidRequest();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      projects: [],
      status: 1,
      from: '',
      to: ''
    };
  },
  components: {
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_0__["TheMask"]
  },
  mounted: function mounted() {
    this.getJob();
  },
  methods: {
    getJob: function getJob() {
      var _this = this;

      axios.get("/projects/logs/all?status=".concat(this.status, "&from=").concat(this.from, "&to=").concat(this.to)).then(function (_ref) {
        var data = _ref.data;
        _this.projects = data;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=template&id=34a21d0c&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=template&id=34a21d0c& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _vm.id && _vm.projects.length
      ? _c(
          "h3",
          {
            staticStyle: {
              "font-size": "20px !important",
              color: "#364b87",
              padding: "0px 0px 20px 20px"
            }
          },
          [_vm._v("\n        Project: " + _vm._s(_vm.projects[0].project.name))]
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "col-lg-12" }, [
      _c("div", { staticClass: "maain-tabble table-responsive" }, [
        _c(
          "table",
          {
            staticClass:
              "table text-center table-striped table-bordered zero-configuration"
          },
          [
            _c("thead", [
              _c("tr", [
                _c("th", [_vm._v("S.No")]),
                _vm._v(" "),
                _c("th", [_vm._v("PROJECT NAME")]),
                _vm._v(" "),
                _c("th", [_vm._v("Project Description")]),
                _vm._v(" "),
                _vm.id ? _c("th", [_vm._v("Request To")]) : _vm._e(),
                _vm._v(" "),
                _vm.id ? _c("th", [_vm._v("Status")]) : _vm._e(),
                _vm._v(" "),
                _c("th", [_vm._v("CREATED ON")]),
                _vm._v(" "),
                _c("th", { staticClass: "action-td" }, [_vm._v("ACTION")])
              ])
            ]),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.projects, function(request, index) {
                return _c("tr", { key: index }, [
                  _c("td", [_vm._v(_vm._s(++index))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.project.name))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.project.mini_description))]),
                  _vm._v(" "),
                  _vm.id
                    ? _c("td", [_vm._v(_vm._s(request.requestable.name))])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.id
                    ? _c("td", { attrs: { id: "status" } }, [
                        request.status == 0
                          ? _c("span", [_vm._v("Pending")])
                          : _vm._e(),
                        _vm._v(" "),
                        request.status == 1
                          ? _c("span", [_vm._v("Accepted")])
                          : _vm._e(),
                        _vm._v(" "),
                        request.status == 2
                          ? _c("span", [_vm._v("Rejected")])
                          : _vm._e()
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.project.created_date))]),
                  _vm._v(" "),
                  !_vm.id
                    ? _c("td", [
                        _c(
                          "div",
                          { staticClass: "btn-group mr-1 mb-1" },
                          [
                            !_vm.id
                              ? _c(
                                  "router-link",
                                  {
                                    staticClass:
                                      "dropdown-item l-grey uppercase",
                                    attrs: {
                                      to: {
                                        name: "projects.bid.internal",
                                        query: { type: "incomming" },
                                        params: { id: request.project.id }
                                      }
                                    }
                                  },
                                  [
                                    _c("i", { staticClass: "fas fa-eye" }),
                                    _vm._v("View")
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.id
                              ? _c(
                                  "router-link",
                                  {
                                    staticClass:
                                      "dropdown-item l-grey uppercase",
                                    attrs: {
                                      to: {
                                        name: "projects.bid.detail",
                                        query: { type: "incomming" },
                                        params: { id: request.id }
                                      }
                                    }
                                  },
                                  [
                                    _c("i", { staticClass: "fas fa-eye" }),
                                    _vm._v("Detail")
                                  ]
                                )
                              : _vm._e()
                          ],
                          1
                        )
                      ])
                    : _c("td", { staticClass: "action-data-td" }, [
                        request.status == 0
                          ? _c("span", [
                              _c(
                                "a",
                                {
                                  attrs: { href: "javascript:;" },
                                  on: {
                                    click: function($event) {
                                      return _vm.performAction(
                                        "accept",
                                        request.id
                                      )
                                    }
                                  }
                                },
                                [_vm._v("Accept")]
                              ),
                              _vm._v(" |\n                                "),
                              _c(
                                "a",
                                {
                                  attrs: { href: "javascript:;" },
                                  on: {
                                    click: function($event) {
                                      return _vm.performAction(
                                        "reject",
                                        request.id
                                      )
                                    }
                                  }
                                },
                                [_vm._v("Reject")]
                              )
                            ])
                          : _vm._e()
                      ])
                ])
              }),
              0
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=template&id=b97bad7e&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=template&id=b97bad7e& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _vm.id && _vm.projects.length
      ? _c(
          "h3",
          {
            staticStyle: {
              "font-size": "20px !important",
              color: "#364b87",
              padding: "0px 0px 20px 20px"
            }
          },
          [_vm._v("\n        Project: " + _vm._s(_vm.projects[0].project.name))]
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "col-lg-12" }, [
      _c("div", { staticClass: "maain-tabble table-responsive" }, [
        _c(
          "table",
          {
            staticClass:
              "table text-center table-striped table-bordered zero-configuration"
          },
          [
            _c("thead", [
              _c("tr", [
                _c("th", [_vm._v("S.No")]),
                _vm._v(" "),
                _c("th", [_vm._v("PROJECT NAME")]),
                _vm._v(" "),
                _c("th", [_vm._v("Project Description")]),
                _vm._v(" "),
                !_vm.id ? _c("th", [_vm._v("Bid Count")]) : _vm._e(),
                _vm._v(" "),
                _vm.id ? _c("th", [_vm._v("Bid Type")]) : _vm._e(),
                _vm._v(" "),
                _vm.id ? _c("th", [_vm._v("Bid By")]) : _vm._e(),
                _vm._v(" "),
                _vm.id ? _c("th", [_vm._v("Status")]) : _vm._e(),
                _vm._v(" "),
                _c("th", [_vm._v("CREATED ON")]),
                _vm._v(" "),
                _c("th", [_vm._v("ACTION")])
              ])
            ]),
            _vm._v(" "),
            _c(
              "tbody",
              _vm._l(_vm.projects, function(request, index) {
                return _c("tr", { key: index }, [
                  _c("td", [_vm._v(_vm._s(++index))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.project.name))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.project.mini_description))]),
                  _vm._v(" "),
                  !_vm.id
                    ? _c("td", [_vm._v(_vm._s(request.project.bids_count))])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.id
                    ? _c("td", [
                        _vm._v(
                          "\n                            " +
                            _vm._s(request.job ? "Job" : "Project") +
                            "\n                        "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.id
                    ? _c("td", [_vm._v(_vm._s(request.bidable.name))])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.id
                    ? _c("td", [
                        request.status == 0
                          ? _c("span", [_vm._v("Pending")])
                          : _vm._e(),
                        _vm._v(" "),
                        request.status == 1
                          ? _c("span", [_vm._v("Accepted")])
                          : _vm._e(),
                        _vm._v(" "),
                        request.status == 2
                          ? _c("span", [_vm._v("Rejected")])
                          : _vm._e()
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(request.project.created_date))]),
                  _vm._v(" "),
                  _c("td", [
                    _c(
                      "div",
                      { staticClass: "btn-group mr-1 mb-1" },
                      [
                        !_vm.id
                          ? _c(
                              "router-link",
                              {
                                staticClass: "dropdown-item l-grey uppercase",
                                attrs: {
                                  to: {
                                    name: "projects.bid.internal",
                                    query: { type: "outgoing" },
                                    params: { id: request.project.id }
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-eye" }),
                                _vm._v("View")
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.id || _vm.$route.params.id
                          ? _c(
                              "router-link",
                              {
                                staticClass: "dropdown-item l-grey uppercase",
                                attrs: {
                                  to: {
                                    name: "projects.bid.detail",
                                    query: { type: "outgoing" },
                                    params: { id: request.id }
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-eye" }),
                                _vm._v("Detail")
                              ]
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                ])
              }),
              0
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/BidComponent.vue?vue&type=template&id=2ac88c63&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/BidComponent.vue?vue&type=template&id=2ac88c63& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c("h3", { staticClass: "table-h3 mb-4" }, [
          _vm.$route.params.id
            ? _c(
                "a",
                {
                  attrs: { href: "javascript:;" },
                  on: {
                    click: function($event) {
                      return _vm.$router.go(-1)
                    }
                  }
                },
                [_c("i", { staticClass: "fa fa-angle-left" })]
              )
            : _vm._e(),
          _vm._v(
            "\n                    Project Bid Request\n                    "
          ),
          _vm.$route.params.id && _vm.project
            ? _c("span", [_vm._v(_vm._s(_vm.project.name))])
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row " }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "type text-right" }, [
              _c("h6", { staticClass: "mb-2 bold" }, [_vm._v("Select Type")]),
              _vm._v(" "),
              _c("label", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.type,
                      expression: "type"
                    }
                  ],
                  attrs: { name: "bid_type", value: "outgoing", type: "radio" },
                  domProps: { checked: _vm._q(_vm.type, "outgoing") },
                  on: {
                    click: function($event) {
                      return _vm.$router.replace({
                        name: "projects.bid",
                        query: { type: "outgoing" }
                      })
                    },
                    change: function($event) {
                      _vm.type = "outgoing"
                    }
                  }
                }),
                _vm._v(
                  "\n                                Outgoing\n                            "
                )
              ]),
              _vm._v(" "),
              _c("label", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.type,
                      expression: "type"
                    }
                  ],
                  attrs: {
                    name: "bid_type",
                    value: "incomming",
                    type: "radio"
                  },
                  domProps: { checked: _vm._q(_vm.type, "incomming") },
                  on: {
                    click: function($event) {
                      return _vm.$router.replace({
                        name: "projects.bid",
                        query: { type: "incomming" }
                      })
                    },
                    change: function($event) {
                      _vm.type = "incomming"
                    }
                  }
                }),
                _vm._v(
                  "\n                                Incomming\n                            "
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _vm.type == "incomming" || _vm.$route.query.type == "incomming"
          ? _c("Incomming", { attrs: { id: _vm.$route.params.id } })
          : _c("Outgoing", { attrs: { id: _vm.$route.params.id } })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-6" }, [
      _c("div", { staticClass: "d-flex mt-1 mb-2" }, [
        _c("div", { staticClass: "form-group" }, [
          _c("label", [_vm._v("From")]),
          _vm._v(" "),
          _c("input", { staticClass: "new-input", attrs: { type: "date" } })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("label", [_vm._v("to")]),
          _vm._v(" "),
          _c("input", { staticClass: "new-input", attrs: { type: "date" } })
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=template&id=30548c82&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=template&id=30548c82& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "col-lg-3 " }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", [_vm._v("Project Status")]),
            _vm._v(" "),
            _c(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.status,
                    expression: "status"
                  }
                ],
                staticClass: "new-input",
                on: {
                  change: function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.status = $event.target.multiple
                      ? $$selectedVal
                      : $$selectedVal[0]
                  }
                }
              },
              [
                _c("option", { attrs: { value: "1" } }, [_vm._v("Ongoing")]),
                _vm._v(" "),
                _c("option", { attrs: { value: "3" } }, [_vm._v("Completed")]),
                _vm._v(" "),
                _c("option", { attrs: { value: "4" } }, [_vm._v("Cancelled")])
              ]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "d-flex mt-2 mb-2" }, [
        _c("div", { staticClass: "form-group mr-3" }, [
          _c("label", [_vm._v("From")]),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.from,
                expression: "from"
              }
            ],
            staticClass: "new-input",
            attrs: { max: _vm.to, type: "date" },
            domProps: { value: _vm.from },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.from = $event.target.value
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("label", [_vm._v("to")]),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.to,
                expression: "to"
              }
            ],
            staticClass: "new-input",
            attrs: { min: _vm.from, type: "date" },
            domProps: { value: _vm.to },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.to = $event.target.value
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group ml-2" }, [
          _c(
            "button",
            {
              staticClass: "main-blue-btn",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  return _vm.getJob()
                }
              }
            },
            [_vm._v("Filter")]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "main-blue-btn",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  ;(_vm.from = ""), (_vm.to = "")
                }
              }
            },
            [_vm._v("Reset")]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("div", { staticClass: "maain-tabble table-responsive" }, [
            _c(
              "table",
              {
                staticClass:
                  "table text-center table-striped table-bordered zero-configuration"
              },
              [
                _vm._m(1),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.projects, function(log, lindex) {
                    return _c("tr", { key: lindex }, [
                      _c("td", [_vm._v(_vm._s(++lindex))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(log.project.creatable.name))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(log.project.name))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(log.updated_date))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(log.amount))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(log.timeline))]),
                      _vm._v(" "),
                      _c("td", [
                        log.job_id
                          ? _c("span", [_vm._v("Job")])
                          : _c("span", [_vm._v("Project")])
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        log.status == 1
                          ? _c("span", [_vm._v("Accepted")])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _vm._m(2, true)
                    ])
                  }),
                  0
                )
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-3" }, [
      _c("h3", { staticClass: "table-h3" }, [
        _vm._v("Project Log\n                     "),
        _c("span", [_vm._v("Admin Commission: 10%")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S.No")]),
        _vm._v(" "),
        _c("th", [_vm._v("Customer Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("PROJECT NAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("ASSIGNED DATE")]),
        _vm._v(" "),
        _c("th", [_vm._v("AMOUNT")]),
        _vm._v(" "),
        _c("th", [_vm._v("Timeline")]),
        _vm._v(" "),
        _c("th", [_vm._v("Type")]),
        _vm._v(" "),
        _c("th", [_vm._v("STATUS")]),
        _vm._v(" "),
        _c("th", [_vm._v("ACTION")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("div", { staticClass: "btn-group mr-1 mb-1" }, [
        _c(
          "button",
          {
            staticClass: "btn dropdown-toggle btn-drop-table btn-sm",
            attrs: {
              type: "button",
              "data-toggle": "dropdown",
              "aria-haspopup": "true",
              "aria-expanded": "false"
            }
          },
          [_c("i", { staticClass: "fa fa-ellipsis-v" })]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "dropdown-menu",
            staticStyle: {
              position: "absolute",
              transform: "translate3d(0px, 21px, 0px)",
              top: "0px",
              left: "0px",
              "will-change": "transform"
            },
            attrs: { "x-placement": "bottom-start" }
          },
          [
            _c(
              "a",
              {
                staticClass: "dropdown-item l-grey uppercase",
                attrs: { href: "#" }
              },
              [
                _c("i", { staticClass: "fas fa-money-bill-wave" }),
                _vm._v("Detail")
              ]
            )
          ]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-the-mask/dist/vue-the-mask.js":
/*!********************************************************!*\
  !*** ./node_modules/vue-the-mask/dist/vue-the-mask.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function(e,t){ true?module.exports=t():undefined})(this,function(){return function(e){function t(r){if(n[r])return n[r].exports;var a=n[r]={i:r,l:!1,exports:{}};return e[r].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,r){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:r})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p=".",t(t.s=10)}([function(e,t){e.exports={"#":{pattern:/\d/},X:{pattern:/[0-9a-zA-Z]/},S:{pattern:/[a-zA-Z]/},A:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleUpperCase()}},a:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleLowerCase()}},"!":{escape:!0}}},function(e,t,n){"use strict";function r(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!0),t}var a=n(2),o=n(0),i=n.n(o);t.a=function(e,t){var o=t.value;if((Array.isArray(o)||"string"==typeof o)&&(o={mask:o,tokens:i.a}),"INPUT"!==e.tagName.toLocaleUpperCase()){var u=e.getElementsByTagName("input");if(1!==u.length)throw new Error("v-mask directive requires 1 input, found "+u.length);e=u[0]}e.oninput=function(t){if(t.isTrusted){var i=e.selectionEnd,u=e.value[i-1];for(e.value=n.i(a.a)(e.value,o.mask,!0,o.tokens);i<e.value.length&&e.value.charAt(i-1)!==u;)i++;e===document.activeElement&&(e.setSelectionRange(i,i),setTimeout(function(){e.setSelectionRange(i,i)},0)),e.dispatchEvent(r("input"))}};var s=n.i(a.a)(e.value,o.mask,!0,o.tokens);s!==e.value&&(e.value=s,e.dispatchEvent(r("input")))}},function(e,t,n){"use strict";var r=n(6),a=n(5);t.a=function(e,t){var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=arguments[3];return Array.isArray(t)?n.i(a.a)(r.a,t,i)(e,t,o,i):n.i(r.a)(e,t,o,i)}},function(e,t,n){"use strict";function r(e){e.component(s.a.name,s.a),e.directive("mask",i.a)}Object.defineProperty(t,"__esModule",{value:!0});var a=n(0),o=n.n(a),i=n(1),u=n(7),s=n.n(u);n.d(t,"TheMask",function(){return s.a}),n.d(t,"mask",function(){return i.a}),n.d(t,"tokens",function(){return o.a}),n.d(t,"version",function(){return c});var c="0.11.1";t.default=r,"undefined"!=typeof window&&window.Vue&&window.Vue.use(r)},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=n(1),a=n(0),o=n.n(a),i=n(2);t.default={name:"TheMask",props:{value:[String,Number],mask:{type:[String,Array],required:!0},masked:{type:Boolean,default:!1},tokens:{type:Object,default:function(){return o.a}}},directives:{mask:r.a},data:function(){return{lastValue:null,display:this.value}},watch:{value:function(e){e!==this.lastValue&&(this.display=e)},masked:function(){this.refresh(this.display)}},computed:{config:function(){return{mask:this.mask,tokens:this.tokens,masked:this.masked}}},methods:{onInput:function(e){e.isTrusted||this.refresh(e.target.value)},refresh:function(e){this.display=e;var e=n.i(i.a)(e,this.mask,this.masked,this.tokens);e!==this.lastValue&&(this.lastValue=e,this.$emit("input",e))}}}},function(e,t,n){"use strict";function r(e,t,n){return t=t.sort(function(e,t){return e.length-t.length}),function(r,a){for(var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=0;i<t.length;){var u=t[i];i++;var s=t[i];if(!(s&&e(r,s,!0,n).length>u.length))return e(r,u,o,n)}return""}}t.a=r},function(e,t,n){"use strict";function r(e,t){var n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],r=arguments[3];e=e||"",t=t||"";for(var a=0,o=0,i="";a<t.length&&o<e.length;){var u=t[a],s=r[u],c=e[o];s&&!s.escape?(s.pattern.test(c)&&(i+=s.transform?s.transform(c):c,a++),o++):(s&&s.escape&&(a++,u=t[a]),n&&(i+=u),c===u&&o++,a++)}for(var f="";a<t.length&&n;){var u=t[a];if(r[u]){f="";break}f+=u,a++}return i+f}t.a=r},function(e,t,n){var r=n(8)(n(4),n(9),null,null);e.exports=r.exports},function(e,t){e.exports=function(e,t,n,r){var a,o=e=e||{},i=typeof e.default;"object"!==i&&"function"!==i||(a=e,o=e.default);var u="function"==typeof o?o.options:o;if(t&&(u.render=t.render,u.staticRenderFns=t.staticRenderFns),n&&(u._scopeId=n),r){var s=u.computed||(u.computed={});Object.keys(r).forEach(function(e){var t=r[e];s[e]=function(){return t}})}return{esModule:a,exports:o,options:u}}},function(e,t){e.exports={render:function(){var e=this,t=e.$createElement;return(e._self._c||t)("input",{directives:[{name:"mask",rawName:"v-mask",value:e.config,expression:"config"}],attrs:{type:"text"},domProps:{value:e.display},on:{input:e.onInput}})},staticRenderFns:[]}},function(e,t,n){e.exports=n(3)}])});

/***/ }),

/***/ "./resources/js/business/views/projects/Bid/Incomming.vue":
/*!****************************************************************!*\
  !*** ./resources/js/business/views/projects/Bid/Incomming.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Incomming_vue_vue_type_template_id_34a21d0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Incomming.vue?vue&type=template&id=34a21d0c& */ "./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=template&id=34a21d0c&");
/* harmony import */ var _Incomming_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Incomming.vue?vue&type=script&lang=js& */ "./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Incomming_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Incomming_vue_vue_type_template_id_34a21d0c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Incomming_vue_vue_type_template_id_34a21d0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/projects/Bid/Incomming.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Incomming_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Incomming.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Incomming_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=template&id=34a21d0c&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=template&id=34a21d0c& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Incomming_vue_vue_type_template_id_34a21d0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Incomming.vue?vue&type=template&id=34a21d0c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Incomming.vue?vue&type=template&id=34a21d0c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Incomming_vue_vue_type_template_id_34a21d0c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Incomming_vue_vue_type_template_id_34a21d0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/business/views/projects/Bid/Outgoing.vue":
/*!***************************************************************!*\
  !*** ./resources/js/business/views/projects/Bid/Outgoing.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Outgoing_vue_vue_type_template_id_b97bad7e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Outgoing.vue?vue&type=template&id=b97bad7e& */ "./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=template&id=b97bad7e&");
/* harmony import */ var _Outgoing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Outgoing.vue?vue&type=script&lang=js& */ "./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Outgoing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Outgoing_vue_vue_type_template_id_b97bad7e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Outgoing_vue_vue_type_template_id_b97bad7e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/projects/Bid/Outgoing.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Outgoing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Outgoing.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Outgoing_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=template&id=b97bad7e&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=template&id=b97bad7e& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Outgoing_vue_vue_type_template_id_b97bad7e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Outgoing.vue?vue&type=template&id=b97bad7e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/Bid/Outgoing.vue?vue&type=template&id=b97bad7e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Outgoing_vue_vue_type_template_id_b97bad7e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Outgoing_vue_vue_type_template_id_b97bad7e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/business/views/projects/BidComponent.vue":
/*!***************************************************************!*\
  !*** ./resources/js/business/views/projects/BidComponent.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BidComponent_vue_vue_type_template_id_2ac88c63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BidComponent.vue?vue&type=template&id=2ac88c63& */ "./resources/js/business/views/projects/BidComponent.vue?vue&type=template&id=2ac88c63&");
/* harmony import */ var _BidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BidComponent.vue?vue&type=script&lang=js& */ "./resources/js/business/views/projects/BidComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BidComponent_vue_vue_type_template_id_2ac88c63___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BidComponent_vue_vue_type_template_id_2ac88c63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/projects/BidComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/projects/BidComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/business/views/projects/BidComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BidComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/BidComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BidComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/projects/BidComponent.vue?vue&type=template&id=2ac88c63&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/business/views/projects/BidComponent.vue?vue&type=template&id=2ac88c63& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BidComponent_vue_vue_type_template_id_2ac88c63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BidComponent.vue?vue&type=template&id=2ac88c63& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/BidComponent.vue?vue&type=template&id=2ac88c63&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BidComponent_vue_vue_type_template_id_2ac88c63___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BidComponent_vue_vue_type_template_id_2ac88c63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/business/views/projects/ProjectLogComponent.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/business/views/projects/ProjectLogComponent.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProjectLogComponent_vue_vue_type_template_id_30548c82___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProjectLogComponent.vue?vue&type=template&id=30548c82& */ "./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=template&id=30548c82&");
/* harmony import */ var _ProjectLogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProjectLogComponent.vue?vue&type=script&lang=js& */ "./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProjectLogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProjectLogComponent_vue_vue_type_template_id_30548c82___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProjectLogComponent_vue_vue_type_template_id_30548c82___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/projects/ProjectLogComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectLogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectLogComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectLogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=template&id=30548c82&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=template&id=30548c82& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectLogComponent_vue_vue_type_template_id_30548c82___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectLogComponent.vue?vue&type=template&id=30548c82& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ProjectLogComponent.vue?vue&type=template&id=30548c82&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectLogComponent_vue_vue_type_template_id_30548c82___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectLogComponent_vue_vue_type_template_id_30548c82___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);