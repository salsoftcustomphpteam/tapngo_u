(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["job.view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/ViewComponent.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/job/ViewComponent.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      baseUrl: window.base_url,
      user: window.user,
      job: ""
    };
  },
  mounted: function mounted() {
    this.fetchJob();
  },
  methods: {
    fetchJob: function fetchJob() {
      var _this = this;

      axios.get("/jobs/".concat(this.$route.params.id)).then(function (_ref) {
        var data = _ref.data;
        _this.job = data.data;
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/ViewComponent.vue?vue&type=template&id=49e0e3b4&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/job/ViewComponent.vue?vue&type=template&id=49e0e3b4& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "cstm-job-view-card" }, [
        _c("h3", { staticClass: "table-h3" }, [_vm._v("View Job")]),
        _vm._v(" "),
        _c("div", { staticClass: "cstm-job-view-card-inner" }, [
          _c("h5", { staticClass: "text-right" }, [
            _vm._v("Status: "),
            _c("span", { staticClass: "blue-txt" }, [
              _vm._v(_vm._s(_vm.job.status_value))
            ])
          ]),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("JOB CATEGORY")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.category_id))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("JOB TITLE")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.title))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("COMPANY NAME")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.company_name))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("SALARY RANGE")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.salary))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("POSTED ON")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.created_date))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("APPLY BEFORE")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.apply_before_date))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("PERIOD OF EMPLOYMENT")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.period_of_employment))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("HOURS OF WORK")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.hours_of_work))])
              ])
            ])
          ]),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("COUNTRY:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.country))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("CITY:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.city))])
              ])
            ])
          ]),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("JOB DESCRIPTION:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.description))])
              ])
            ])
          ]),
          _vm._v(" "),
          _vm._m(3),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("Skills:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.skills))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("DEGREE REQUIREMENT:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.minimum_requirment))])
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-lg-12" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "main-blue-btn fa-pull-right",
                    attrs: { to: { name: "jobs.edit" } }
                  },
                  [_vm._v("Edit")]
                )
              ],
              1
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("JOB DETAILS")]),
      _vm._v(" "),
      _c("i", { staticClass: "fas fa-arrow-up" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("ADDRESS DETAILS")]),
      _vm._v(" "),
      _c("i", { staticClass: "fas fa-arrow-up" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("JOB RESPONSIBILITIES")]),
      _vm._v(" "),
      _c("i", { staticClass: "fas fa-arrow-up" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("EXPERIENCE INFORMATION")]),
      _vm._v(" "),
      _c("i", { staticClass: "fas fa-arrow-up" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/business/views/job/ViewComponent.vue":
/*!***********************************************************!*\
  !*** ./resources/js/business/views/job/ViewComponent.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ViewComponent_vue_vue_type_template_id_49e0e3b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ViewComponent.vue?vue&type=template&id=49e0e3b4& */ "./resources/js/business/views/job/ViewComponent.vue?vue&type=template&id=49e0e3b4&");
/* harmony import */ var _ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ViewComponent.vue?vue&type=script&lang=js& */ "./resources/js/business/views/job/ViewComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ViewComponent_vue_vue_type_template_id_49e0e3b4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ViewComponent_vue_vue_type_template_id_49e0e3b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/job/ViewComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/job/ViewComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/business/views/job/ViewComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ViewComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/ViewComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/job/ViewComponent.vue?vue&type=template&id=49e0e3b4&":
/*!******************************************************************************************!*\
  !*** ./resources/js/business/views/job/ViewComponent.vue?vue&type=template&id=49e0e3b4& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_49e0e3b4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ViewComponent.vue?vue&type=template&id=49e0e3b4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/ViewComponent.vue?vue&type=template&id=49e0e3b4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_49e0e3b4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_49e0e3b4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);