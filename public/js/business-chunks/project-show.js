(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-show"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ShowComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/ShowComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_typeahead_bootstrap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-typeahead-bootstrap */ "./node_modules/vue-typeahead-bootstrap/src/components/VueTypeaheadBootstrap.vue");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_1__);
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      project: undefined,
      tags: [],
      my_bids: [],
      query: '',
      selected: []
    };
  },
  components: {
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_1__["TheMask"],
    VueTypeaheadBootstrap: vue_typeahead_bootstrap__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.getProject();
  },
  methods: {
    openLink: function openLink(url) {
      window.open(url, "_blank");
    },
    getProject: function getProject() {
      var _this = this;

      axios.get("/projects/".concat(this.$route.params.id)).then(function (_ref) {
        var data = _ref.data;
        data.project.hasBid = data.my_bids.filter(function (b) {
          return b.job_id == null;
        }).length > 0;
        data.project.jobs.forEach(function (j) {
          j.hasBid = data.my_bids.filter(function (b) {
            return b.job_id == j.id;
          }).length > 0;
        });
        _this.project = data.project;
      });
    },
    setTag: function setTag(event) {
      console.log('setTag', event);
      this.selected = [event.name].concat(_toConsumableArray(this.selected));
      this.query = '';
    },
    removeTag: function removeTag(event) {
      console.log('removeTag', event);
      var removeIndex = this.selected.map(function (item) {
        return item.name;
      }).indexOf(event.name);
      this.selected.splice(removeIndex, 1);
    },
    lookupKeywords: function lookupKeywords() {
      var _this2 = this;

      axios.get("/get-keyword?disbloadr=true&query=".concat(this.query)).then(function (data) {
        _this2.tags = data.data;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ShowComponent.vue?vue&type=template&id=04867d2d&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/projects/ShowComponent.vue?vue&type=template&id=04867d2d& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "cstmr-project-activity-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-3 cstmr-project-activity-left" }, [
          _c("h3", { staticClass: "table-h3" }, [_vm._v("My Projects")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "csmtr-project-listing-srch" },
            [
              _c("vue-typeahead-bootstrap", {
                attrs: {
                  data: _vm.tags,
                  placeholder: "Search by Tags",
                  serializer: function(item) {
                    return item.name
                  }
                },
                on: {
                  hit: function($event) {
                    return _vm.setTag($event)
                  },
                  input: _vm.lookupKeywords
                },
                scopedSlots: _vm._u([
                  {
                    key: "suggestion",
                    fn: function(ref) {
                      var data = ref.data
                      var htmlText = ref.htmlText
                      return [
                        _c(
                          "div",
                          { staticClass: "d-flex align-items-center" },
                          [
                            _c("span", {
                              staticClass: "ml-4",
                              domProps: { innerHTML: _vm._s(htmlText) }
                            })
                          ]
                        )
                      ]
                    }
                  }
                ]),
                model: {
                  value: _vm.query,
                  callback: function($$v) {
                    _vm.query = $$v
                  },
                  expression: "query"
                }
              }),
              _vm._v(" "),
              _vm._m(0)
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "csm-listing-left-tags" },
            [
              _vm._l(_vm.selected, function(tag, ti) {
                return _c("p", { key: ti }, [
                  _vm._v(_vm._s(tag) + "\n                        "),
                  _c(
                    "a",
                    {
                      attrs: { href: "javascript:;" },
                      on: {
                        click: function($event) {
                          return _vm.removeTag(tag)
                        }
                      }
                    },
                    [_c("i", { staticClass: "fas fa-times" })]
                  )
                ])
              }),
              _vm._v(" "),
              _vm.selected.length == 0
                ? _c("p", { staticClass: "pl-1" }, [
                    _vm._v("No Tag selected yet")
                  ])
                : _vm._e()
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "main-blue-btn fa-pull-left mt-0",
              attrs: { href: "javascript:;" }
            },
            [_vm._v("Filter")]
          )
        ]),
        _vm._v(" "),
        _vm.project
          ? _c(
              "div",
              { staticClass: "col-lg-9 cstmr-project-activity-right" },
              [
                _c("h3", [_vm._v(_vm._s(_vm.project.name))]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "mt-2 mb-2" },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "main-blue-btn",
                        attrs: { href: "javascript:;" }
                      },
                      [_vm._v("Details")]
                    ),
                    _vm._v(" "),
                    _vm.project.type == 0 && _vm.project.hasBid == false
                      ? _c(
                          "router-link",
                          {
                            staticClass: "main-blue-btn",
                            attrs: {
                              to: {
                                name: "projects.placebid",
                                params: { id: _vm.project.id }
                              }
                            }
                          },
                          [_vm._v("Place Bid")]
                        )
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "cstm-pro-job-inner" }, [
                  _c("h5", [_vm._v("Project Description")]),
                  _vm._v(" "),
                  _c("p", [_vm._v(_vm._s(_vm.project.description))])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "cstm-pro-job-inner" },
                  [
                    _c("h5", [_vm._v("Attachements")]),
                    _vm._v(" "),
                    _vm._l(_vm.project.media, function(media, mindex) {
                      return _c(
                        "span",
                        {
                          key: mindex,
                          staticClass: "project-attach-cv",
                          staticStyle: { cursor: "pointer" },
                          on: {
                            click: function($event) {
                              return _vm.openLink(media.getFullUrl)
                            }
                          }
                        },
                        [
                          _c("img", {
                            attrs: {
                              src: _vm.getIconByMimeType(media.mime_type),
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                        " +
                              _vm._s(media.file_name) +
                              "\n                    "
                          )
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _c("p", { staticClass: "mb-2" }, [
                      _vm._v("REQUIRED SKILLS")
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "col-12" }, [
                      _c(
                        "ul",
                        { staticStyle: { "list-style": "circle" } },
                        _vm._l(_vm.project.skills.split(","), function(
                          skill,
                          sindex
                        ) {
                          return _c("li", { key: sindex }, [
                            _vm._v(_vm._s(skill))
                          ])
                        }),
                        0
                      )
                    ])
                  ],
                  2
                ),
                _vm._v(" "),
                _vm.project.type == 0
                  ? _c("div", { staticClass: "activty-jobs-gry" }, [
                      _c("p", [
                        _vm._v("ESTIMATED DURATION: "),
                        _c("span", [
                          _vm._v(_vm._s(_vm.project.duration) + " Month")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v("ESTIMATED BUDGET: "),
                        _c("span", [
                          _vm._v(_vm._s(_vm.formatAmount(_vm.project.budget)))
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.project.type == 1
                  ? _c("div", { staticClass: "activty-jobs-gry" }, [
                      _c("table", { staticClass: "table" }, [
                        _vm._m(1),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.project.jobs, function(job, jobindex) {
                            return _c("tr", { key: jobindex }, [
                              _c("td", [_vm._v(_vm._s(++jobindex))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(job.title))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(job.category))]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(_vm._s(job.duration) + " Month")
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(_vm._s(_vm.formatAmount(job.budget)))
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                job.hasBid == false
                                  ? _c(
                                      "div",
                                      { staticClass: "btn-group mr-1 mb-1" },
                                      [
                                        _vm._m(2, true),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass: "dropdown-menu",
                                            staticStyle: {
                                              position: "absolute",
                                              transform:
                                                "translate3d(0px, 21px, 0px)",
                                              top: "0px",
                                              left: "0px",
                                              "will-change": "transform"
                                            },
                                            attrs: {
                                              "x-placement": "bottom-start"
                                            }
                                          },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                staticClass:
                                                  "dropdown-item l-grey uppercase w-100 rounded-0 ",
                                                staticStyle: {
                                                  background: "transparent"
                                                },
                                                attrs: {
                                                  to: {
                                                    name: "projects.placebid",
                                                    params: {
                                                      id: _vm.project.id,
                                                      jobid: job.id
                                                    }
                                                  },
                                                  href: "javascipt:;"
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass: "fas fa-eye"
                                                }),
                                                _vm._v("Place Bid")
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ]
                                    )
                                  : _vm._e()
                              ])
                            ])
                          }),
                          0
                        )
                      ])
                    ])
                  : _vm._e()
              ]
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", [_c("i", { staticClass: "fas fa-search" })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("th", [_vm._v("S.no")]),
      _vm._v(" "),
      _c("th", [_vm._v("Job Title")]),
      _vm._v(" "),
      _c("th", [_vm._v("Category")]),
      _vm._v(" "),
      _c("th", [_vm._v("Duration")]),
      _vm._v(" "),
      _c("th", [_vm._v("Budget")]),
      _vm._v(" "),
      _c("th")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn dropdown-toggle btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-the-mask/dist/vue-the-mask.js":
/*!********************************************************!*\
  !*** ./node_modules/vue-the-mask/dist/vue-the-mask.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function(e,t){ true?module.exports=t():undefined})(this,function(){return function(e){function t(r){if(n[r])return n[r].exports;var a=n[r]={i:r,l:!1,exports:{}};return e[r].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,r){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:r})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p=".",t(t.s=10)}([function(e,t){e.exports={"#":{pattern:/\d/},X:{pattern:/[0-9a-zA-Z]/},S:{pattern:/[a-zA-Z]/},A:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleUpperCase()}},a:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleLowerCase()}},"!":{escape:!0}}},function(e,t,n){"use strict";function r(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!0),t}var a=n(2),o=n(0),i=n.n(o);t.a=function(e,t){var o=t.value;if((Array.isArray(o)||"string"==typeof o)&&(o={mask:o,tokens:i.a}),"INPUT"!==e.tagName.toLocaleUpperCase()){var u=e.getElementsByTagName("input");if(1!==u.length)throw new Error("v-mask directive requires 1 input, found "+u.length);e=u[0]}e.oninput=function(t){if(t.isTrusted){var i=e.selectionEnd,u=e.value[i-1];for(e.value=n.i(a.a)(e.value,o.mask,!0,o.tokens);i<e.value.length&&e.value.charAt(i-1)!==u;)i++;e===document.activeElement&&(e.setSelectionRange(i,i),setTimeout(function(){e.setSelectionRange(i,i)},0)),e.dispatchEvent(r("input"))}};var s=n.i(a.a)(e.value,o.mask,!0,o.tokens);s!==e.value&&(e.value=s,e.dispatchEvent(r("input")))}},function(e,t,n){"use strict";var r=n(6),a=n(5);t.a=function(e,t){var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=arguments[3];return Array.isArray(t)?n.i(a.a)(r.a,t,i)(e,t,o,i):n.i(r.a)(e,t,o,i)}},function(e,t,n){"use strict";function r(e){e.component(s.a.name,s.a),e.directive("mask",i.a)}Object.defineProperty(t,"__esModule",{value:!0});var a=n(0),o=n.n(a),i=n(1),u=n(7),s=n.n(u);n.d(t,"TheMask",function(){return s.a}),n.d(t,"mask",function(){return i.a}),n.d(t,"tokens",function(){return o.a}),n.d(t,"version",function(){return c});var c="0.11.1";t.default=r,"undefined"!=typeof window&&window.Vue&&window.Vue.use(r)},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=n(1),a=n(0),o=n.n(a),i=n(2);t.default={name:"TheMask",props:{value:[String,Number],mask:{type:[String,Array],required:!0},masked:{type:Boolean,default:!1},tokens:{type:Object,default:function(){return o.a}}},directives:{mask:r.a},data:function(){return{lastValue:null,display:this.value}},watch:{value:function(e){e!==this.lastValue&&(this.display=e)},masked:function(){this.refresh(this.display)}},computed:{config:function(){return{mask:this.mask,tokens:this.tokens,masked:this.masked}}},methods:{onInput:function(e){e.isTrusted||this.refresh(e.target.value)},refresh:function(e){this.display=e;var e=n.i(i.a)(e,this.mask,this.masked,this.tokens);e!==this.lastValue&&(this.lastValue=e,this.$emit("input",e))}}}},function(e,t,n){"use strict";function r(e,t,n){return t=t.sort(function(e,t){return e.length-t.length}),function(r,a){for(var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=0;i<t.length;){var u=t[i];i++;var s=t[i];if(!(s&&e(r,s,!0,n).length>u.length))return e(r,u,o,n)}return""}}t.a=r},function(e,t,n){"use strict";function r(e,t){var n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],r=arguments[3];e=e||"",t=t||"";for(var a=0,o=0,i="";a<t.length&&o<e.length;){var u=t[a],s=r[u],c=e[o];s&&!s.escape?(s.pattern.test(c)&&(i+=s.transform?s.transform(c):c,a++),o++):(s&&s.escape&&(a++,u=t[a]),n&&(i+=u),c===u&&o++,a++)}for(var f="";a<t.length&&n;){var u=t[a];if(r[u]){f="";break}f+=u,a++}return i+f}t.a=r},function(e,t,n){var r=n(8)(n(4),n(9),null,null);e.exports=r.exports},function(e,t){e.exports=function(e,t,n,r){var a,o=e=e||{},i=typeof e.default;"object"!==i&&"function"!==i||(a=e,o=e.default);var u="function"==typeof o?o.options:o;if(t&&(u.render=t.render,u.staticRenderFns=t.staticRenderFns),n&&(u._scopeId=n),r){var s=u.computed||(u.computed={});Object.keys(r).forEach(function(e){var t=r[e];s[e]=function(){return t}})}return{esModule:a,exports:o,options:u}}},function(e,t){e.exports={render:function(){var e=this,t=e.$createElement;return(e._self._c||t)("input",{directives:[{name:"mask",rawName:"v-mask",value:e.config,expression:"config"}],attrs:{type:"text"},domProps:{value:e.display},on:{input:e.onInput}})},staticRenderFns:[]}},function(e,t,n){e.exports=n(3)}])});

/***/ }),

/***/ "./resources/js/business/views/projects/ShowComponent.vue":
/*!****************************************************************!*\
  !*** ./resources/js/business/views/projects/ShowComponent.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowComponent_vue_vue_type_template_id_04867d2d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=template&id=04867d2d& */ "./resources/js/business/views/projects/ShowComponent.vue?vue&type=template&id=04867d2d&");
/* harmony import */ var _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=script&lang=js& */ "./resources/js/business/views/projects/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowComponent_vue_vue_type_template_id_04867d2d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowComponent_vue_vue_type_template_id_04867d2d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/projects/ShowComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/projects/ShowComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/business/views/projects/ShowComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/projects/ShowComponent.vue?vue&type=template&id=04867d2d&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/business/views/projects/ShowComponent.vue?vue&type=template&id=04867d2d& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_04867d2d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=template&id=04867d2d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/projects/ShowComponent.vue?vue&type=template&id=04867d2d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_04867d2d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_04867d2d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);