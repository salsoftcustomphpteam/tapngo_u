(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["emp.view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/employee/ViewComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/employee/ViewComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      baseUrl: window.base_url,
      user: window.user,
      employee: undefined
    };
  },
  mounted: function mounted() {
    this.getEmployee();
  },
  methods: {
    getEmployee: function getEmployee() {
      var _this = this;

      axios.get("/employees/".concat(this.$route.params.id)).then(function (_ref) {
        var data = _ref.data;
        _this.employee = data.data;
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/employee/ViewComponent.vue?vue&type=template&id=7d9c108e&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/employee/ViewComponent.vue?vue&type=template&id=7d9c108e& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customr-following-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-lg-6" },
          [
            _c("router-link", { attrs: { to: { name: "employee" } } }, [
              _c("h3", { staticClass: "table-h3" }, [
                _c("i", { staticClass: "fa fa-caret-left" }, [_vm._v(" ")]),
                _vm._v("View Employee\n                    ")
              ])
            ])
          ],
          1
        )
      ]),
      _vm._v(" "),
      _vm.employee
        ? _c("div", { staticClass: "cret-project-card clearfix" }, [
            _c("div", { staticClass: "emply-view-task p-5 clearfix" }, [
              _c("h2", [_vm._v("Employee Personal Details:")]),
              _vm._v(" "),
              _c("p", [
                _vm._v("Employee Id: "),
                _c("span", { staticClass: "blue-txt2" }, [
                  _vm._v("Emp-" + _vm._s(_vm.employee.ref_id))
                ])
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v("Employee Name: "),
                _c("span", { staticClass: "blue-txt2" }, [
                  _vm._v(_vm._s(_vm.employee.name) + " ")
                ])
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v("Email: "),
                _c("span", { staticClass: "blue-txt2" }, [
                  _vm._v(_vm._s(_vm.employee.email) + " ")
                ])
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v(" Gender: "),
                _c("span", { staticClass: "blue-txt2" }, [
                  _vm._v(_vm._s(_vm.employee.gender) + " ")
                ])
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v("Phone Number: "),
                _c("span", { staticClass: "blue-txt2" }, [
                  _vm._v(" " + _vm._s(_vm.employee.phone) + " ")
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c(
                  "div",
                  { staticClass: "col-lg-12" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "main-blue-btn mt-0 fa-pull-right",
                        attrs: {
                          to: {
                            name: "employee.edit",
                            params: { id: _vm.$route.params.id }
                          }
                        }
                      },
                      [_vm._v("Edit Details")]
                    )
                  ],
                  1
                )
              ])
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/business/views/employee/ViewComponent.vue":
/*!****************************************************************!*\
  !*** ./resources/js/business/views/employee/ViewComponent.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ViewComponent_vue_vue_type_template_id_7d9c108e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ViewComponent.vue?vue&type=template&id=7d9c108e& */ "./resources/js/business/views/employee/ViewComponent.vue?vue&type=template&id=7d9c108e&");
/* harmony import */ var _ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ViewComponent.vue?vue&type=script&lang=js& */ "./resources/js/business/views/employee/ViewComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ViewComponent_vue_vue_type_template_id_7d9c108e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ViewComponent_vue_vue_type_template_id_7d9c108e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/employee/ViewComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/employee/ViewComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/business/views/employee/ViewComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ViewComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/employee/ViewComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/employee/ViewComponent.vue?vue&type=template&id=7d9c108e&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/business/views/employee/ViewComponent.vue?vue&type=template&id=7d9c108e& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_7d9c108e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ViewComponent.vue?vue&type=template&id=7d9c108e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/employee/ViewComponent.vue?vue&type=template&id=7d9c108e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_7d9c108e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ViewComponent_vue_vue_type_template_id_7d9c108e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);