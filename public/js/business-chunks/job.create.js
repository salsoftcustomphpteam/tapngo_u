(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["job.create"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/CreateComponent.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/job/CreateComponent.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      baseUrl: window.base_url,
      user: window.user,
      categories: window.job_category,
      job: {
        title: '',
        category: '',
        company_name: '',
        salary_range: '',
        apply_before: '',
        period_of_employment: '',
        hours_of_work: '',
        country: '',
        city: '',
        description: '',
        experience: '',
        degree_requirment: '',
        skills: ''
      }
    };
  },
  mounted: function mounted() {},
  methods: {
    saveJob: function saveJob() {
      var _this = this;

      axios.post("/jobs", this.job).then(function (data) {
        _this.$toastr.success(data.data.message, 'Success', {});

        _this.$router.push({
          name: 'jobs'
        });
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this.$toastr.error(errors[key], "Error!");
        });
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/CreateComponent.vue?vue&type=template&id=a77efc86&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/business/views/job/CreateComponent.vue?vue&type=template&id=a77efc86& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-8 mx-auto" }, [
          _c("h5", { staticClass: "blue-h5" }, [_vm._v("Create Job")]),
          _vm._v(" "),
          _c("div", { staticClass: "cret-project-card" }, [
            _c("div", { staticClass: "row crt-project-bottom-line" }, [
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Job Title")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.title,
                        expression: "job.title"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: {
                      type: "text",
                      placeholder: "e.g. Senior Team lead"
                    },
                    domProps: { value: _vm.job.title },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "title", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Job Category")]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.job.category,
                          expression: "job.category"
                        }
                      ],
                      staticClass: "new-input",
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.job,
                            "category",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "" } }, [
                        _vm._v("Select")
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.categories, function(cat, cindex) {
                        return _c("option", { key: cindex }, [
                          _vm._v(
                            "\n                                        " +
                              _vm._s(cat) +
                              "\n                                    "
                          )
                        ])
                      })
                    ],
                    2
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Company Name:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.company_name,
                        expression: "job.company_name"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: { type: "text", placeholder: "Abc Company" },
                    domProps: { value: _vm.job.company_name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "company_name", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Salary Range:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.salary_range,
                        expression: "job.salary_range"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: { type: "text", placeholder: "e.g: $600-$6000" },
                    domProps: { value: _vm.job.salary_range },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "salary_range", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Apply Before:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.apply_before,
                        expression: "job.apply_before"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: {
                      type: "date",
                      min: "" + _vm.dateFormat(new Date())
                    },
                    domProps: { value: _vm.job.apply_before },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "apply_before", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Period of Employment:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.period_of_employment,
                        expression: "job.period_of_employment"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: {
                      type: "text",
                      placeholder: "Period of Employment"
                    },
                    domProps: { value: _vm.job.period_of_employment },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.job,
                          "period_of_employment",
                          $event.target.value
                        )
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Hours of Work:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.hours_of_work,
                        expression: "job.hours_of_work"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: { type: "text", placeholder: "Hours of Work" },
                    domProps: { value: _vm.job.hours_of_work },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "hours_of_work", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }),
              _vm._v(" "),
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Select Country:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.country,
                        expression: "job.country"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: { type: "text", placeholder: "Country" },
                    domProps: { value: _vm.job.country },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "country", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Select City:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.city,
                        expression: "job.city"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: { type: "text", placeholder: "city" },
                    domProps: { value: _vm.job.city },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "city", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-12" }, [
                _c("h5", { staticClass: "mb-3 blue-h5" }, [
                  _vm._v("Job Responsibilities")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Job Description:")]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.description,
                        expression: "job.description"
                      }
                    ],
                    staticClass: "txtara",
                    attrs: { placeholder: "Description" },
                    domProps: { value: _vm.job.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "description", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Years of experience needed:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.experience,
                        expression: "job.experience"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: {
                      type: "number",
                      placeholder: "Experience in numbers"
                    },
                    domProps: { value: _vm.job.experience },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "experience", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Degree requirements:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.degree_requirment,
                        expression: "job.degree_requirment"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: { type: "text", placeholder: "Country" },
                    domProps: { value: _vm.job.degree_requirment },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.job,
                          "degree_requirment",
                          $event.target.value
                        )
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-12" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("Skills:")]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.job.skills,
                        expression: "job.skills"
                      }
                    ],
                    staticClass: "txtara",
                    attrs: { placeholder: "Skills e.g. PHP, Java, HTML, css" },
                    domProps: { value: _vm.job.skills },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.job, "skills", $event.target.value)
                      }
                    }
                  })
                ])
              ])
            ]),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "mt-5 main-blue-btn fa-pull-right",
                attrs: { type: "button" },
                on: { click: _vm.saveJob }
              },
              [_vm._v("Create")]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-12" }, [
      _c("hr"),
      _vm._v(" "),
      _c("h5", { staticClass: "mb-3 blue-h5" }, [_vm._v("Address Details")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-12" }, [
      _c("h5", { staticClass: "mb-3 blue-h5" }, [
        _vm._v("Experience Information")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/business/views/job/CreateComponent.vue":
/*!*************************************************************!*\
  !*** ./resources/js/business/views/job/CreateComponent.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateComponent_vue_vue_type_template_id_a77efc86___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateComponent.vue?vue&type=template&id=a77efc86& */ "./resources/js/business/views/job/CreateComponent.vue?vue&type=template&id=a77efc86&");
/* harmony import */ var _CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateComponent.vue?vue&type=script&lang=js& */ "./resources/js/business/views/job/CreateComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateComponent_vue_vue_type_template_id_a77efc86___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateComponent_vue_vue_type_template_id_a77efc86___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/business/views/job/CreateComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/business/views/job/CreateComponent.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/business/views/job/CreateComponent.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/CreateComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/business/views/job/CreateComponent.vue?vue&type=template&id=a77efc86&":
/*!********************************************************************************************!*\
  !*** ./resources/js/business/views/job/CreateComponent.vue?vue&type=template&id=a77efc86& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_a77efc86___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=template&id=a77efc86& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/business/views/job/CreateComponent.vue?vue&type=template&id=a77efc86&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_a77efc86___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_a77efc86___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);