(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["job-view"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/job/ShowComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      baseUrl: window.base_url,
      user: window.user,
      job: "",
      file: undefined
    };
  },
  mounted: function mounted() {
    this.fetchJob();
  },
  methods: {
    handleFileChange: function handleFileChange(e) {
      console.log(e);
      this.file = e.target.files[0];
    },
    applyJob: function applyJob() {
      var _this = this;

      var formData = new FormData();
      formData.append("file", this.file);
      axios.post("/jobs/".concat(this.$route.params.id, "/apply"), formData).then(function (_ref) {
        var data = _ref.data;
        $('#applyJob').modal('hide');

        _this.$toastr.success(data.message, 'Success');

        _this.fetchJob();
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this.$toastr.error(errors[key], "Error!");
        });
      });
    },
    fetchJob: function fetchJob() {
      var _this2 = this;

      axios.get("/jobs/".concat(this.$route.params.id)).then(function (_ref2) {
        var data = _ref2.data;
        _this2.job = data.data;
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.bussnies-job-view-row{\n\tbackground:#F2F2F2;\n\tpadding: 20px;\n\tmargin-left: -20px;\n\tmargin-right: -20px;\n\tdisplay: flex;\n\tjustify-content: space-between;\n\tmargin-top: 15px;\n\tmargin-bottom: 15px;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css&");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=template&id=634d72e7&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/job/ShowComponent.vue?vue&type=template&id=634d72e7& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "cstm-job-view-card" }, [
        _c("h3", { staticClass: "table-h3" }, [_vm._v("View Job")]),
        _vm._v(" "),
        _c("div", { staticClass: "cstm-job-view-card-inner" }, [
          _c("h5", { staticClass: "text-right" }, [
            _vm._v("Status: "),
            _c("span", { staticClass: "blue-txt" }, [
              _vm._v(_vm._s(_vm.job.status_value))
            ])
          ]),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("JOB CATEGORY")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.category_id))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("JOB TITLE")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.title))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("COMPANY NAME")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.company_name))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("SALARY RANGE")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.salary))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("POSTED ON")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.created_date))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("APPLY BEFORE")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.apply_before_date))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("PERIOD OF EMPLOYMENT")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.period_of_employment))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("HOURS OF WORK")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.hours_of_work))])
              ])
            ])
          ]),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("COUNTRY:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.country))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("CITY:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.city))])
              ])
            ])
          ]),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("JOB DESCRIPTION:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.description))])
              ])
            ])
          ]),
          _vm._v(" "),
          _vm._m(3),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-12" }, [
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("Skills:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.skills))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-4" }, [
                _c("h5", [_vm._v("DEGREE REQUIREMENT:")]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.job.minimum_requirment))])
              ])
            ]),
            _vm._v(" "),
            _vm.job.haveApplied == false
              ? _c("div", { staticClass: "col-lg-12" }, [
                  _c(
                    "a",
                    {
                      staticClass: "main-blue-btn fa-pull-right",
                      attrs: {
                        href: "javascript:;",
                        "data-toggle": "modal",
                        "data-target": "#applyJob"
                      }
                    },
                    [_vm._v("Apply")]
                  )
                ])
              : _c("div", { staticClass: "col-lg-12" }, [
                  _c(
                    "a",
                    {
                      staticClass: "main-blue-btn fa-pull-right",
                      attrs: { href: "javascript:;" }
                    },
                    [_vm._v("Already Applied")]
                  )
                ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm.job.haveApplied == false
        ? _c(
            "div",
            {
              staticClass: "modal fade",
              attrs: {
                id: "applyJob",
                tabindex: "-1",
                role: "dialog",
                "aria-labelledby": "exampleModalCenterTitle",
                "aria-hidden": "true"
              }
            },
            [
              _c(
                "div",
                {
                  staticClass: "modal-dialog modal-dialog-centered ",
                  attrs: { role: "document" }
                },
                [
                  _c("div", { staticClass: "modal-content" }, [
                    _vm._m(4),
                    _vm._v(" "),
                    _c("div", { staticClass: "px-1 pt-5 pb-5" }, [
                      _c("h5", { staticClass: " text-center" }, [
                        _vm._v("Apply For Job")
                      ]),
                      _vm._v(" "),
                      _c(
                        "p",
                        { staticClass: "mt-1 medium blue-txt  text-center" },
                        [
                          _vm._v(
                            "Are you sure that you want to apply for this group"
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12 form-group" }, [
                        _c("strong", [_vm._v("Upload Resume (PDF)")]),
                        _vm._v("   \n                            "),
                        _c("input", {
                          attrs: { type: "file" },
                          on: { change: _vm.handleFileChange }
                        })
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "d-flex flex-wrap justify-content-center text-center"
                        },
                        [
                          _c(
                            "a",
                            {
                              attrs: { href: "javascript:;" },
                              on: {
                                click: function($event) {
                                  return _vm.applyJob()
                                }
                              }
                            },
                            [_vm._v("Yes")]
                          ),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: {
                                href: "javascript:;",
                                "data-dismiss": "modal",
                                "aria-label": "Close"
                              }
                            },
                            [
                              _vm._v(
                                "\n                                No\n                            "
                              )
                            ]
                          )
                        ]
                      )
                    ])
                  ])
                ]
              )
            ]
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("JOB DETAILS")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("ADDRESS DETAILS")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("JOB RESPONSIBILITIES")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bussnies-job-view-row" }, [
      _c("h6", [_vm._v("EXPERIENCE INFORMATION")]),
      _vm._v(" "),
      _c("i", { staticClass: "fas fa-arrow-up" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close text-right mr-1 mt-1",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/views/job/ShowComponent.vue":
/*!*******************************************************!*\
  !*** ./resources/js/user/views/job/ShowComponent.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowComponent_vue_vue_type_template_id_634d72e7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=template&id=634d72e7& */ "./resources/js/user/views/job/ShowComponent.vue?vue&type=template&id=634d72e7&");
/* harmony import */ var _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=script&lang=js& */ "./resources/js/user/views/job/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowComponent_vue_vue_type_template_id_634d72e7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowComponent_vue_vue_type_template_id_634d72e7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/views/job/ShowComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/views/job/ShowComponent.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/user/views/job/ShowComponent.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************!*\
  !*** ./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/user/views/job/ShowComponent.vue?vue&type=template&id=634d72e7&":
/*!**************************************************************************************!*\
  !*** ./resources/js/user/views/job/ShowComponent.vue?vue&type=template&id=634d72e7& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_634d72e7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=template&id=634d72e7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/job/ShowComponent.vue?vue&type=template&id=634d72e7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_634d72e7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_634d72e7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);