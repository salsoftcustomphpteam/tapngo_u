(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-create"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/CreateComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-dropzone/dist/vue2Dropzone.min.css */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.min.css");
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue_input_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-input-tag */ "./node_modules/vue-input-tag/dist/vueInputTag.common.js");
/* harmony import */ var vue_input_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_input_tag__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_typeahead_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-typeahead-bootstrap */ "./node_modules/vue-typeahead-bootstrap/src/components/VueTypeaheadBootstrap.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




 // import fragment from 'vue-fragment'

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      project: {
        name: '',
        description: '',
        skill: '',
        type: 0
      },
      jobs: [{
        title: '',
        category: '',
        duration: '',
        budget: ''
      }],
      projectUser: [],
      users: [],
      uids: [],
      query: '',
      baseUrl: window.axios.defaults.baseURL,
      dropzoneOptions: {
        autoProcessQueue: false,
        url: window.axios.defaults.baseURL + "projects/7/files/upload",
        thumbnailWidth: 250,
        headers: {
          "Authorization": 'Bearer ' + this.getCookie('p_token')
        },
        addRemoveLinks: true
      },
      job_category: window.job_category
    };
  },
  components: {
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_2__["TheMask"],
    vue2Dropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_1___default.a,
    InputTag: vue_input_tag__WEBPACK_IMPORTED_MODULE_3___default.a,
    VueTypeaheadBootstrap: vue_typeahead_bootstrap__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  mounted: function mounted() {},
  methods: {
    getCookie: function getCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
          c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }

      return null;
    },
    setUser: function setUser(event) {
      this.projectUser = [event].concat(_toConsumableArray(this.projectUser));
      this.uids = [event.id].concat(_toConsumableArray(this.uids));
      this.query = "";
    },
    removeUser: function removeUser(user) {
      var removeIndex = this.projectUser.map(function (item) {
        return item.id;
      }).indexOf(user.id);
      console.log(removeIndex);
      this.projectUser.splice(removeIndex, 1);
      var removeIndexUid = this.uids.indexOf(user.id);
      console.log(removeIndexUid);
      this.uids.splice(removeIndexUid, 1);
    },
    addRow: function addRow() {
      this.jobs.push({
        title: '',
        category: '',
        duration: '',
        budget: ''
      });
    },
    removeRow: function removeRow(index) {
      this.jobs.splice(index, 1);
    },
    lookupUser: function lookupUser() {
      var _this = this;

      axios.get("/search?disbloadr=true&resource=bo&name=".concat(this.query, "&notIn=").concat(this.uids.join(","))).then(function (data) {
        _this.users = data.data;
      });
    },
    onDropzoneUploadComplete: function onDropzoneUploadComplete() {
      this.$toastr.success("Project is created succesfully", "Success!");
      this.$router.push({
        name: 'projects'
      });
    },
    submitProject: function submitProject() {
      var _this2 = this;

      if (this.$refs.myVueDropzone.getAcceptedFiles().length == 0) {
        this.$toastr.error("Please attach document to proceed", "Error!");
        return;
      }

      this.cp = {
        project: this.project,
        user: this.projectUser,
        jobs: this.jobs
      };
      axios.post("/projects", this.cp).then(function (data) {
        _this2.$refs.myVueDropzone.dropzone.options.url = window.axios.defaults.baseURL + "projects/".concat(data.data.data.id, "/files/upload");
        setTimeout(function () {
          _this2.$refs.myVueDropzone.processQueue();
        }, 500); // this.dropzoneOptions.url = this.baseURL+`projects/${data.data.data.id}/files/upload`;
        // this.$refs.myVueDropzone.processQueue();
      })["catch"](function (e) {
        console.log(e);
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this2.$toastr.error(errors[key], "Error!");
        });
      });
    },
    submitFiles: function submitFiles() {
      this.$refs.myVueDropzone.myVueDropzone();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n#dropzone{\n    background: none;\n    border: 2px dashed #01AEEE;\n    border-radius: 15px;\n    padding: 15px;\n    text-align: center;\n    margin: 0 0 20px 0;\n}\n.vue-input-tag-wrapper .input-tag{\n    list-style-type: none;\n    display: inline-block;\n    margin: 10px 5px 0 0;\n    border-radius: 25px;\n    border: 2px solid #CFD2E4;\n    padding: 10px 15px;\n    font-size: 13px;\n    color: #7D8094;\n    font-weight: 500;\n    background: #CFD2E4;\n}\n\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css&");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=template&id=f25d0d3a&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/CreateComponent.vue?vue&type=template&id=f25d0d3a& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-10 offset-lg-1" }, [
          _c("h5", { staticClass: "blue-h5" }, [_vm._v("Create Project")]),
          _vm._v(" "),
          _c("div", { staticClass: "cret-project-card" }, [
            _c(
              "div",
              { staticClass: "row crt-project-bottom-line" },
              [
                _c("div", { staticClass: "col-lg-12" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.project.name,
                          expression: "project.name"
                        }
                      ],
                      staticClass: "new-input",
                      attrs: {
                        type: "text",
                        placeholder: "Type a name for your project"
                      },
                      domProps: { value: _vm.project.name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.project, "name", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.project.description,
                          expression: "project.description"
                        }
                      ],
                      staticClass: "txtara",
                      attrs: {
                        placeholder: "Tell us more about your project..."
                      },
                      domProps: { value: _vm.project.description },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.project,
                            "description",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-lg-12" },
                  [
                    _c(
                      "vue2Dropzone",
                      {
                        ref: "myVueDropzone",
                        attrs: {
                          id: "dropzone",
                          options: _vm.dropzoneOptions,
                          useCustomSlot: true
                        },
                        on: {
                          "vdropzone-queue-complete":
                            _vm.onDropzoneUploadComplete
                        }
                      },
                      [
                        _c("button", { staticClass: "main-blue-btn2" }, [
                          _vm._v("Upload file")
                        ]),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "Drag and drop any image or document that might be helpful in explaining your\n                                        project"
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c("label", [_vm._v("What skills are required")]),
                      _vm._v(" "),
                      _c("input-tag", {
                        staticClass: "new-input",
                        attrs: {
                          placeholder:
                            "Enter Skills e,g Java (enter), PHP (enter)"
                        },
                        model: {
                          value: _vm.project.tags,
                          callback: function($$v) {
                            _vm.$set(_vm.project, "tags", $$v)
                          },
                          expression: "project.tags"
                        }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12" }, [
                  _c("div", { staticClass: "form-group d-flex mt-4" }, [
                    _c("h6", [_vm._v("How would you like to get it done?")]),
                    _vm._v(" "),
                    _c("label", [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.project.type,
                            expression: "project.type"
                          }
                        ],
                        attrs: {
                          value: "0",
                          name: "project_type",
                          type: "radio"
                        },
                        domProps: { checked: _vm._q(_vm.project.type, "0") },
                        on: {
                          change: function($event) {
                            return _vm.$set(_vm.project, "type", "0")
                          }
                        }
                      }),
                      _vm._v(
                        "\n                                    Post a Project\n                                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("label", [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.project.type,
                            expression: "project.type"
                          }
                        ],
                        attrs: {
                          value: "1",
                          name: "project_type",
                          type: "radio"
                        },
                        domProps: { checked: _vm._q(_vm.project.type, "1") },
                        on: {
                          change: function($event) {
                            return _vm.$set(_vm.project, "type", "1")
                          }
                        }
                      }),
                      _vm._v(
                        "\n                                    Create Jobs\n                                "
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _vm.project.type == 0
                  ? _c("div", { staticClass: "row col-12" }, [
                      _c("div", { staticClass: "row col-12" }, [
                        _c("div", { staticClass: "col-lg-6" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [
                              _vm._v("What is your Estimated Budget?")
                            ]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.project.budget,
                                  expression: "project.budget"
                                }
                              ],
                              staticClass: "new-input",
                              attrs: {
                                type: "number",
                                placeholder: "e.g: 2000 USD"
                              },
                              domProps: { value: _vm.project.budget },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.project,
                                    "budget",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-lg-6" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Expected Duration")]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.project.duration,
                                  expression: "project.duration"
                                }
                              ],
                              staticClass: "new-input",
                              attrs: {
                                type: "number",
                                placeholder: "e.g: 15 month"
                              },
                              domProps: { value: _vm.project.duration },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.project,
                                    "duration",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _vm._m(0),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-5" }, [
                        _c(
                          "div",
                          { staticClass: "csmtr-project-listing-srch2 mt-1" },
                          [
                            _c("vue-typeahead-bootstrap", {
                              attrs: {
                                data: _vm.users,
                                minMatchingChars: 1,
                                placeholder: "Search Organizations",
                                serializer: function(item) {
                                  return item.name
                                }
                              },
                              on: {
                                hit: function($event) {
                                  return _vm.setUser($event)
                                },
                                input: _vm.lookupUser
                              },
                              scopedSlots: _vm._u(
                                [
                                  {
                                    key: "suggestion",
                                    fn: function(ref) {
                                      var data = ref.data
                                      var htmlText = ref.htmlText
                                      return [
                                        _c("div", {}, [
                                          _c("img", {
                                            staticClass:
                                              "rounded-circle float-left",
                                            staticStyle: {
                                              width: "40px",
                                              height: "40px"
                                            },
                                            attrs: { src: data.image }
                                          }),
                                          _vm._v(" "),
                                          _c("span", {
                                            staticClass: "mt-3 ml-2 float-left",
                                            domProps: {
                                              innerHTML: _vm._s(htmlText)
                                            }
                                          })
                                        ])
                                      ]
                                    }
                                  }
                                ],
                                null,
                                false,
                                917059676
                              ),
                              model: {
                                value: _vm.query,
                                callback: function($$v) {
                                  _vm.query = $$v
                                },
                                expression: "query"
                              }
                            }),
                            _vm._v(" "),
                            _vm._m(1)
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-lg-7" },
                        _vm._l(_vm.projectUser, function(user, index) {
                          return _c(
                            "span",
                            { key: index, staticClass: "crt-pro-tag" },
                            [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(user.name) +
                                  " \n                                    "
                              ),
                              _c(
                                "a",
                                {
                                  attrs: { href: "javascript:;" },
                                  on: {
                                    click: function() {
                                      return _vm.removeUser(user)
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fas fa-times-circle"
                                  })
                                ]
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ])
                  : _vm.project.type == 1
                  ? _vm._l(_vm.jobs, function(job, index) {
                      return _c(
                        "div",
                        { key: index, staticClass: "row col-12" },
                        [
                          _c("div", { staticClass: "col-lg-12" }, [
                            _vm.jobs.length > 1
                              ? _c(
                                  "a",
                                  {
                                    staticClass: "fa-pull-right",
                                    attrs: { href: "javascript:;" },
                                    on: {
                                      click: function($event) {
                                        return _vm.removeRow(index)
                                      }
                                    }
                                  },
                                  [
                                    _c("Img", {
                                      attrs: {
                                        src: "/users/images/dlt-icon.png"
                                      }
                                    })
                                  ],
                                  1
                                )
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-lg-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", [_vm._v("Job Title")]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: job.title,
                                    expression: "job.title"
                                  }
                                ],
                                staticClass: "new-input",
                                attrs: {
                                  type: "text",
                                  placeholder: "Job Title"
                                },
                                domProps: { value: job.title },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(job, "title", $event.target.value)
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-lg-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", [_vm._v("Job Category")]),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: job.category,
                                      expression: "job.category"
                                    }
                                  ],
                                  staticClass: "new-input",
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        job,
                                        "category",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c("option", [_vm._v("Select Category")]),
                                  _vm._v(" "),
                                  _vm._l(_vm.job_category, function(
                                    cat,
                                    catindex
                                  ) {
                                    return _c("option", { key: catindex }, [
                                      _vm._v(_vm._s(cat))
                                    ])
                                  })
                                ],
                                2
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-lg-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", [_vm._v("Duration")]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: job.duration,
                                    expression: "job.duration"
                                  }
                                ],
                                staticClass: "new-input",
                                attrs: {
                                  type: "number",
                                  placeholder: "3 Months"
                                },
                                domProps: { value: job.duration },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      job,
                                      "duration",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-lg-6" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", [_vm._v("Estimated Budget")]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: job.budget,
                                    expression: "job.budget"
                                  }
                                ],
                                staticClass: "new-input",
                                attrs: {
                                  type: "number",
                                  placeholder: "e.g: $600"
                                },
                                domProps: { value: job.budget },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(job, "budget", $event.target.value)
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "fa-pull-left border-0",
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  return _vm.addRow()
                                }
                              }
                            },
                            [
                              _c("Img", {
                                attrs: { src: "/users/images/plus-add.png" }
                              })
                            ],
                            1
                          )
                        ]
                      )
                    })
                  : _vm._e()
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "main-blue-btn fa-pull-right",
                attrs: { type: "button" },
                on: { click: _vm.submitProject }
              },
              [_vm._v("Post my Project")]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-12" }, [
      _c("label", [
        _vm._v(
          "Send Project Request to any business Organization you may know."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", [_c("i", { staticClass: "fas fa-search" })])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/views/projects/CreateComponent.vue":
/*!**************************************************************!*\
  !*** ./resources/js/user/views/projects/CreateComponent.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateComponent_vue_vue_type_template_id_f25d0d3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateComponent.vue?vue&type=template&id=f25d0d3a& */ "./resources/js/user/views/projects/CreateComponent.vue?vue&type=template&id=f25d0d3a&");
/* harmony import */ var _CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateComponent.vue?vue&type=script&lang=js& */ "./resources/js/user/views/projects/CreateComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CreateComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CreateComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateComponent_vue_vue_type_template_id_f25d0d3a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateComponent_vue_vue_type_template_id_f25d0d3a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/views/projects/CreateComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/views/projects/CreateComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/user/views/projects/CreateComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/user/views/projects/CreateComponent.vue?vue&type=template&id=f25d0d3a&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/user/views/projects/CreateComponent.vue?vue&type=template&id=f25d0d3a& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_f25d0d3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=template&id=f25d0d3a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/CreateComponent.vue?vue&type=template&id=f25d0d3a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_f25d0d3a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_f25d0d3a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);