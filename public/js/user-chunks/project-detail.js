(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-detail"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      bid: undefined,
      action: '',
      reason: ''
    };
  },
  components: {
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_0__["TheMask"]
  },
  mounted: function mounted() {
    this.getBidDetail();
  },
  methods: {
    perform: function perform() {
      var _this = this;

      axios.get("projectsbid/".concat(this.$route.params.id, "/").concat(this.action, "?text=").concat(this.reason || '')).then(function (_ref) {
        var data = _ref.data;
        $('#bidActionModal').modal('hide');

        _this.$toastr.success(data.message, 'Success');

        _this.$router.push({
          name: 'projects.bid'
        });
      });
    },
    getBidDetail: function getBidDetail() {
      var _this2 = this;

      axios.get("projectsbid/".concat(this.$route.params.id)).then(function (_ref2) {
        var data = _ref2.data;
        _this2.bid = data;
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=template&id=3f6fa846&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=template&id=3f6fa846& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "customer-home-main" }, [
    _c("div", { staticClass: "container" }, [
      _vm.bid
        ? _c("div", { staticClass: "cstm-job-view-card" }, [
            _c("div", { staticClass: "cstm-job-view-card-inner" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-lg-2" }, [
                  _c("img", {
                    staticClass: "common-pro-img-big",
                    attrs: { src: _vm.bid.bidable.image, alt: "" }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-lg-10" },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-lg-7" }, [
                        _c("h5", [_vm._v(_vm._s(_vm.bid.bidable.name))])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-5" }, [
                        _c("p", { staticClass: "text-right d-inline-block" }, [
                          _vm._v(
                            "STATUS REQUEST:\n                                    "
                          ),
                          _vm.bid.status == 0
                            ? _c("span", { staticClass: "blue-txt" }, [
                                _vm._v("PENDING")
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.bid.status == 1
                            ? _c("span", { staticClass: "green-txt" }, [
                                _vm._v("ACCEPTED")
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.bid.status == 2
                            ? _c("span", { staticClass: "red-txt" }, [
                                _vm._v("REJECTED")
                              ])
                            : _vm._e()
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("h5", [
                      _vm._v("$ " + _vm._s(_vm.bid.amount) + "/ "),
                      _c("span", { staticClass: "blue-txt" }, [
                        _vm._v(_vm._s(_vm.bid.timeline) + " days")
                      ])
                    ]),
                    _vm._v(" "),
                    _c("p", [_vm._v(_vm._s(_vm.bid.description))]),
                    _vm._v(" "),
                    _c("h5", [_vm._v("Milestones")]),
                    _vm._v(" "),
                    _vm._l(_vm.bid.milestones, function(milestone, index) {
                      return _c(
                        "div",
                        { key: index, staticClass: "d-inline-block mr-2" },
                        [
                          _c("p", [
                            _vm._v(
                              _vm._s(milestone.text) +
                                ": $" +
                                _vm._s(milestone.amount)
                            )
                          ])
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _c("h5", [_vm._v("Bid")]),
                    _vm._v(" "),
                    _c("p", [_vm._v("Bid: $" + _vm._s(_vm.bid.amount))]),
                    _vm._v(" "),
                    _c("p", [_vm._v("Conek pro Project Fee: $0")]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v("Amount Payable: $" + _vm._s(_vm.bid.amount))
                    ]),
                    _vm._v(" "),
                    _vm.bid.status == 2
                      ? _c("fragment", [
                          _c("h5", [_vm._v("Rejection Reason:")]),
                          _vm._v(" "),
                          _c("p", [_vm._v(_vm._s(_vm.bid.reason))])
                        ])
                      : _vm._e()
                  ],
                  2
                ),
                _vm._v(" "),
                _c("div", { staticClass: "col-lg-12" }, [
                  _vm.bid.status == 0
                    ? _c(
                        "button",
                        {
                          staticClass: "main-blue-btn fa-pull-right",
                          attrs: {
                            "data-toggle": "modal",
                            "data-target": "#bidActionModal",
                            type: "button"
                          },
                          on: {
                            click: function($event) {
                              _vm.action = "accept"
                            }
                          }
                        },
                        [_vm._v("Accept")]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.bid.status == 0
                    ? _c(
                        "button",
                        {
                          staticClass: "main-blue-btn2 fa-pull-right",
                          attrs: {
                            "data-toggle": "modal",
                            "data-target": "#bidActionModal",
                            type: "button"
                          },
                          on: {
                            click: function($event) {
                              _vm.action = "reject"
                            }
                          }
                        },
                        [_vm._v("Reject")]
                      )
                    : _vm._e()
                ])
              ])
            ])
          ])
        : _vm._e()
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "bidActionModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "exampleModalCenterTitle",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered ",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "px-1 pt-2 pb-5 text-center" },
                [
                  _c("Img", {
                    staticClass: "center-img",
                    attrs: { src: "/images/question-mark.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("p", { staticClass: "d-blue mt-1 medium " }, [
                    _vm._v(
                      "Are you sure want to " +
                        _vm._s(_vm.action) +
                        " this request?"
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-12 mb-2 mt-2" }, [
                    _vm.action == "reject"
                      ? _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.reason,
                              expression: "reason"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Enter rejection reason" },
                          domProps: { value: _vm.reason },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.reason = $event.target.value
                            }
                          }
                        })
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "d-flex flex-wrap justify-content-center" },
                    [
                      _c(
                        "a",
                        {
                          attrs: {
                            href: "javascript:;",
                            "aria-label": "Close"
                          },
                          on: {
                            click: function($event) {
                              return _vm.perform()
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                            Yes\n                        "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          attrs: {
                            href: "javascript:;",
                            "data-dismiss": "modal",
                            "aria-label": "Close"
                          }
                        },
                        [
                          _vm._v(
                            "\n                            No\n                        "
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              )
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close text-right mr-1 mt-1",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-the-mask/dist/vue-the-mask.js":
/*!********************************************************!*\
  !*** ./node_modules/vue-the-mask/dist/vue-the-mask.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function(e,t){ true?module.exports=t():undefined})(this,function(){return function(e){function t(r){if(n[r])return n[r].exports;var a=n[r]={i:r,l:!1,exports:{}};return e[r].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,r){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:r})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p=".",t(t.s=10)}([function(e,t){e.exports={"#":{pattern:/\d/},X:{pattern:/[0-9a-zA-Z]/},S:{pattern:/[a-zA-Z]/},A:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleUpperCase()}},a:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleLowerCase()}},"!":{escape:!0}}},function(e,t,n){"use strict";function r(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!0),t}var a=n(2),o=n(0),i=n.n(o);t.a=function(e,t){var o=t.value;if((Array.isArray(o)||"string"==typeof o)&&(o={mask:o,tokens:i.a}),"INPUT"!==e.tagName.toLocaleUpperCase()){var u=e.getElementsByTagName("input");if(1!==u.length)throw new Error("v-mask directive requires 1 input, found "+u.length);e=u[0]}e.oninput=function(t){if(t.isTrusted){var i=e.selectionEnd,u=e.value[i-1];for(e.value=n.i(a.a)(e.value,o.mask,!0,o.tokens);i<e.value.length&&e.value.charAt(i-1)!==u;)i++;e===document.activeElement&&(e.setSelectionRange(i,i),setTimeout(function(){e.setSelectionRange(i,i)},0)),e.dispatchEvent(r("input"))}};var s=n.i(a.a)(e.value,o.mask,!0,o.tokens);s!==e.value&&(e.value=s,e.dispatchEvent(r("input")))}},function(e,t,n){"use strict";var r=n(6),a=n(5);t.a=function(e,t){var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=arguments[3];return Array.isArray(t)?n.i(a.a)(r.a,t,i)(e,t,o,i):n.i(r.a)(e,t,o,i)}},function(e,t,n){"use strict";function r(e){e.component(s.a.name,s.a),e.directive("mask",i.a)}Object.defineProperty(t,"__esModule",{value:!0});var a=n(0),o=n.n(a),i=n(1),u=n(7),s=n.n(u);n.d(t,"TheMask",function(){return s.a}),n.d(t,"mask",function(){return i.a}),n.d(t,"tokens",function(){return o.a}),n.d(t,"version",function(){return c});var c="0.11.1";t.default=r,"undefined"!=typeof window&&window.Vue&&window.Vue.use(r)},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=n(1),a=n(0),o=n.n(a),i=n(2);t.default={name:"TheMask",props:{value:[String,Number],mask:{type:[String,Array],required:!0},masked:{type:Boolean,default:!1},tokens:{type:Object,default:function(){return o.a}}},directives:{mask:r.a},data:function(){return{lastValue:null,display:this.value}},watch:{value:function(e){e!==this.lastValue&&(this.display=e)},masked:function(){this.refresh(this.display)}},computed:{config:function(){return{mask:this.mask,tokens:this.tokens,masked:this.masked}}},methods:{onInput:function(e){e.isTrusted||this.refresh(e.target.value)},refresh:function(e){this.display=e;var e=n.i(i.a)(e,this.mask,this.masked,this.tokens);e!==this.lastValue&&(this.lastValue=e,this.$emit("input",e))}}}},function(e,t,n){"use strict";function r(e,t,n){return t=t.sort(function(e,t){return e.length-t.length}),function(r,a){for(var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=0;i<t.length;){var u=t[i];i++;var s=t[i];if(!(s&&e(r,s,!0,n).length>u.length))return e(r,u,o,n)}return""}}t.a=r},function(e,t,n){"use strict";function r(e,t){var n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],r=arguments[3];e=e||"",t=t||"";for(var a=0,o=0,i="";a<t.length&&o<e.length;){var u=t[a],s=r[u],c=e[o];s&&!s.escape?(s.pattern.test(c)&&(i+=s.transform?s.transform(c):c,a++),o++):(s&&s.escape&&(a++,u=t[a]),n&&(i+=u),c===u&&o++,a++)}for(var f="";a<t.length&&n;){var u=t[a];if(r[u]){f="";break}f+=u,a++}return i+f}t.a=r},function(e,t,n){var r=n(8)(n(4),n(9),null,null);e.exports=r.exports},function(e,t){e.exports=function(e,t,n,r){var a,o=e=e||{},i=typeof e.default;"object"!==i&&"function"!==i||(a=e,o=e.default);var u="function"==typeof o?o.options:o;if(t&&(u.render=t.render,u.staticRenderFns=t.staticRenderFns),n&&(u._scopeId=n),r){var s=u.computed||(u.computed={});Object.keys(r).forEach(function(e){var t=r[e];s[e]=function(){return t}})}return{esModule:a,exports:o,options:u}}},function(e,t){e.exports={render:function(){var e=this,t=e.$createElement;return(e._self._c||t)("input",{directives:[{name:"mask",rawName:"v-mask",value:e.config,expression:"config"}],attrs:{type:"text"},domProps:{value:e.display},on:{input:e.onInput}})},staticRenderFns:[]}},function(e,t,n){e.exports=n(3)}])});

/***/ }),

/***/ "./resources/js/user/views/projects/BidDetailComponent.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/user/views/projects/BidDetailComponent.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BidDetailComponent_vue_vue_type_template_id_3f6fa846___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BidDetailComponent.vue?vue&type=template&id=3f6fa846& */ "./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=template&id=3f6fa846&");
/* harmony import */ var _BidDetailComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BidDetailComponent.vue?vue&type=script&lang=js& */ "./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BidDetailComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BidDetailComponent_vue_vue_type_template_id_3f6fa846___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BidDetailComponent_vue_vue_type_template_id_3f6fa846___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/views/projects/BidDetailComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BidDetailComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BidDetailComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BidDetailComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=template&id=3f6fa846&":
/*!************************************************************************************************!*\
  !*** ./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=template&id=3f6fa846& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BidDetailComponent_vue_vue_type_template_id_3f6fa846___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BidDetailComponent.vue?vue&type=template&id=3f6fa846& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/BidDetailComponent.vue?vue&type=template&id=3f6fa846&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BidDetailComponent_vue_vue_type_template_id_3f6fa846___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BidDetailComponent_vue_vue_type_template_id_3f6fa846___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);