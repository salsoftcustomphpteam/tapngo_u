(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["book-a-ride"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/booking/BookARide.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/booking/BookARide.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-google-maps */ "./node_modules/vue2-google-maps/dist/main.js");
/* harmony import */ var vue2_google_maps__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: window.user,
      pickup: '',
      dropoff: '',
      dataToStore: [],
      EstimatedFare: 100,
      distance: 0,
      form: {
        location: '',
        lat: '',
        lng: '',
        lat2: '',
        lng2: ''
      },
      mapCenter: {
        lat: 10,
        lng: 10
      },
      mapOptions: {
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: false,
        disableDefaultUi: true,
        keyboardShortcuts: false
      },
      path: [],
      center: {
        lat: 1.38,
        lng: 103.8
      },
      data: {
        edited: null,
        mvcPath: null,
        errorMessage: null,
        polylineGeojson: ''
      }
    };
  },
  watch: {
    polylinePath: _.throttle(function (path) {
      if (path) {
        this.path = path;
        this.polylineGeojson = JSON.stringify({
          type: 'Polyline',
          coordinates: this.path.map(function (_ref) {
            var lat = _ref.lat,
                lng = _ref.lng;
            return [lng, lat];
          })
        }, null, 2);
      }
    }, 1000)
  },
  computed: {
    google: vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__["gmapApi"],
    polylinePath: function polylinePath() {
      if (!this.mvcPath) return null;
      var path = [];

      for (var j = 0; j < this.mvcPath.getLength(); j++) {
        var point = this.mvcPath.getAt(j);
        path.push({
          lat: point.lat(),
          lng: point.lng()
        });
      }

      return path;
    }
  },
  methods: {
    bookARide: function bookARide() {
      var _this = this;

      this.dataToStore.push({
        'pickup': this.pickup,
        'dropoff': this.dropoff,
        'locations': this.path,
        'distance': this.distance
      });
      axios.post("/bookings", this.dataToStore).then(function (data) {
        _this.$toastr.success(data.data.message, 'Success', {});

        _this.$router.push({
          name: 'myrides'
        });
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this.$toastr.error(errors[key], "Error!");
        });
      });
    },
    pickUp: function pickUp(place) {
      if (!place) return;
      this.form.location = place.formatted_address;
      this.form.lat = place.geometry.location.lat();
      this.form.lng = place.geometry.location.lng();
      var self = this; // this.path.lat = place.geometry.location.lat();
      // this.path.lng = place.geometry.location.lng();

      var feed = {
        lat: this.form.lat,
        lng: this.form.lng
      };
      var feed2 = {
        lat: this.form.lat2,
        lng: this.form.lng2
      };
      this.path.push(feed);
      this.path.push(feed2);
      console.log(this.path);
      this.path = {
        lat: this.form.lat,
        lng: this.form.lng
      };
      console.log(place.geometry.location.lat(), '==='); // _.each(place.address_components,function(item,key){
      //     var addressType = item.types[0];
      // 	if (self.componentForm[addressType]) {
      //         var val = item[self.componentForm[addressType]];
      // 		console.log(val);
      //         Vue.set(self.form,self.location[addressType],val);
      //     }
      // });
      // this.updateMap();
    },
    dropOff: function dropOff(place) {
      console.log(place);
      if (!place) return;
      this.form.location = place.formatted_address;
      this.form.lat2 = place.geometry.location.lat();
      this.form.lng2 = place.geometry.location.lng();
      var self = this;
      console.log(place.geometry.location.lat()); // _.each(place.address_components,function(item,key){
      //     var addressType = item.types[0];
      // 	if (self.componentForm[addressType]) {
      //         var val = item[self.componentForm[addressType]];
      // 		console.log(val);
      //         Vue.set(self.form,self.location[addressType],val);
      //     }
      // });

      var feed = {
        lat: this.form.lat,
        lng: this.form.lng
      };
      var feed2 = {
        lat: this.form.lat2,
        lng: this.form.lng2
      };
      this.path.push(feed);
      this.path.push(feed2); // this.updateMap();

      console.log(this.path);
    },
    updateMap: function updateMap() {
      this.mapCenter = {
        lat: this.form.lat,
        lng: this.form.lng
      };
    },
    updateCenter: function updateCenter(place) {
      var _this2 = this;

      if (this.path.length > 1) {
        this.path.splice(1, 1);
      }

      var feed = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      };
      this.path.push(feed);
      this.center = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      }; // calculate distance

      var pickup = $(".pickup").val();
      var dropoff = $(".dropoff").val();
      this.pickup = pickup;
      this.dropoff = dropoff;

      if (pickup !== undefined && dropoff !== undefined) {
        axios.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".concat(pickup, "&destinations=").concat(dropoff, "&key=AIzaSyCyDamIuNTXON7fh1TMdf5ik2J0bfQdrdo")).then(function (_ref2) {
          var data = _ref2.data;
          console.log(data, '======google api resp');
          _this2.distance = data.rows.elements.distance.value;
        })["catch"](function (e) {});
      }
    },
    updateEdited: function updateEdited(mvcPath) {
      this.mvcPath = mvcPath;
    },
    handleClickForDelete: function handleClickForDelete($event) {
      if ($event.vertex) {
        this.$refs.polyline.$polylineObject.getPaths().getAt($event.path).removeAt($event.vertex);
      }
    },
    readGeojson: function readGeojson($event) {
      try {
        this.polylineGeojson = $event.target.value;
        var v = JSON.parse($event.target.value);
        this.path = v.coordinates.map(function (_ref3) {
          var _ref4 = _slicedToArray(_ref3, 2),
              lng = _ref4[0],
              lat = _ref4[1];

          return {
            lat: lat,
            lng: lng
          };
        });
        this.errorMessage = null;
      } catch (err) {
        this.errorMessage = err.message;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/booking/BookARide.vue?vue&type=template&id=c8753ada&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/booking/BookARide.vue?vue&type=template&id=c8753ada& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "app-content content dashboard" }, [
    _c("div", { staticClass: "content-wrapper" }, [
      _c("div", { staticClass: "content-body mt-2" }, [
        _c("section", { attrs: { id: "configuration" } }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "card-content collapse show" }, [
                _c("div", { staticClass: "card-dashboard" }, [
                  _c("h1", { staticClass: "in-heading-blk" }, [
                    _vm._v("Book Ride")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "for-user" }, [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("form", [
                          _c("div", { staticClass: "form-row" }, [
                            _c(
                              "div",
                              { staticClass: "form-group col-md-6" },
                              [
                                _c("label", { attrs: { for: "inputEmail4" } }, [
                                  _vm._v("Pickup Location")
                                ]),
                                _vm._v(" "),
                                _c("gmap-autocomplete", {
                                  staticClass: "form-control pickup",
                                  attrs: { "select-first-on-enter": true },
                                  on: {
                                    place_changed: function($event) {
                                      return _vm.updateCenter($event)
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(0)
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "form-group col-md-6" },
                              [
                                _c(
                                  "label",
                                  { attrs: { for: "inputPassword4" } },
                                  [_vm._v("Dropoff Location")]
                                ),
                                _vm._v(" "),
                                _c("gmap-autocomplete", {
                                  staticClass: "form-control dropoff",
                                  attrs: { "select-first-on-enter": true },
                                  on: {
                                    place_changed: function($event) {
                                      return _vm.updateCenter($event)
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _vm._m(1)
                              ],
                              1
                            )
                          ])
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c(
                        "div",
                        { staticClass: "col-md-12" },
                        [
                          _c(
                            "gmap-map",
                            {
                              ref: "map",
                              staticStyle: { width: "100%", height: "500px" },
                              attrs: { center: _vm.center, zoom: 12 }
                            },
                            [
                              _c("gmap-marker", {
                                attrs: {
                                  position: _vm.mapCenter,
                                  clickable: true,
                                  draggable: false
                                }
                              }),
                              _vm._v(" "),
                              _vm.path.length > 0
                                ? _c("gmap-polyline", {
                                    ref: "polyline",
                                    attrs: { path: _vm.path, editable: true },
                                    on: {
                                      path_changed: function($event) {
                                        return _vm.updateEdited($event)
                                      },
                                      rightclick: _vm.handleClickForDelete
                                    }
                                  })
                                : _vm._e()
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _vm._m(2),
                    _vm._v(" "),
                    _vm._m(3),
                    _vm._v(" "),
                    _c("div", { staticClass: "row py-2" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("label", { attrs: { for: "inputPassword4" } }, [
                          _vm._v("Estimated Fare")
                        ]),
                        _vm._v(" "),
                        _c("span", { staticClass: "sm-oring" }, [
                          _vm._v("£" + _vm._s(_vm.EstimatedFare))
                        ]),
                        _vm._v(" "),
                        _vm._m(4)
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row py-2 text-center" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c(
                          "a",
                          {
                            staticClass: "u-blue-l8-btn",
                            attrs: { href: "javascript:;" },
                            on: { click: _vm.bookARide }
                          },
                          [_vm._v("Confirm Booking")]
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      { staticClass: "view-btn position-absolute", attrs: { type: "button" } },
      [_c("i", { staticClass: "fa fa-search" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      { staticClass: "view-btn position-absolute", attrs: { type: "button" } },
      [_c("i", { staticClass: "fa fa-search" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row py-2" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c("label", { attrs: { for: "inputPassword4" } }, [
          _vm._v("Pick-Up Time")
        ]),
        _c("br"),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "custom-control custom-radio custom-control-inline" },
          [
            _c("input", {
              staticClass: "custom-control-input",
              attrs: {
                checked: "",
                type: "radio",
                id: "customRadioInline1",
                name: "customRadioInlineLater",
                value: "now"
              }
            }),
            _vm._v(" "),
            _c(
              "label",
              {
                staticClass: "custom-control-label",
                attrs: { for: "customRadioInline1" }
              },
              [
                _vm._v(
                  "Ride\n                                                        Now"
                )
              ]
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "custom-control custom-radio custom-control-inline" },
          [
            _c("input", {
              staticClass: "custom-control-input",
              attrs: {
                type: "radio",
                id: "customRadioInline2",
                name: "customRadioInlineLater",
                value: "later"
              }
            }),
            _vm._v(" "),
            _c(
              "label",
              {
                staticClass: "custom-control-label",
                attrs: { for: "customRadioInline2" }
              },
              [
                _vm._v(
                  "Ride\n                                                        Later"
                )
              ]
            )
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row py-2" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c("form", [
          _c("div", { staticClass: "form-row" }, [
            _c("div", { staticClass: "form-group col-md-6" }, [
              _c("label", { attrs: { for: "inputPassword4" } }, [
                _vm._v("Payment")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  staticClass: "form-control",
                  attrs: { id: "exampleFormControlSelect1" }
                },
                [
                  _c("option", [_vm._v("Select")]),
                  _vm._v(" "),
                  _c("option", { attrs: { selected: "" } }, [_vm._v("Cash")]),
                  _vm._v(" "),
                  _c("option", [_vm._v("Wallet")]),
                  _vm._v(" "),
                  _c("option", [_vm._v("Credit Card")])
                ]
              )
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("a", { staticClass: "blu-txt", attrs: { href: "javascript:;" } }, [
        _vm._v("Add promocodes")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/views/booking/BookARide.vue":
/*!*******************************************************!*\
  !*** ./resources/js/user/views/booking/BookARide.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BookARide_vue_vue_type_template_id_c8753ada___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BookARide.vue?vue&type=template&id=c8753ada& */ "./resources/js/user/views/booking/BookARide.vue?vue&type=template&id=c8753ada&");
/* harmony import */ var _BookARide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BookARide.vue?vue&type=script&lang=js& */ "./resources/js/user/views/booking/BookARide.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BookARide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BookARide_vue_vue_type_template_id_c8753ada___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BookARide_vue_vue_type_template_id_c8753ada___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/views/booking/BookARide.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/views/booking/BookARide.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/user/views/booking/BookARide.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BookARide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BookARide.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/booking/BookARide.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BookARide_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/views/booking/BookARide.vue?vue&type=template&id=c8753ada&":
/*!**************************************************************************************!*\
  !*** ./resources/js/user/views/booking/BookARide.vue?vue&type=template&id=c8753ada& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BookARide_vue_vue_type_template_id_c8753ada___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BookARide.vue?vue&type=template&id=c8753ada& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/booking/BookARide.vue?vue&type=template&id=c8753ada&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BookARide_vue_vue_type_template_id_c8753ada___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BookARide_vue_vue_type_template_id_c8753ada___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);