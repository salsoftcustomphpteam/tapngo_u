(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["project-tm"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-dropzone/dist/vue2Dropzone.min.css */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.min.css");
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vue_typeahead_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-typeahead-bootstrap */ "./node_modules/vue-typeahead-bootstrap/src/components/VueTypeaheadBootstrap.vue");
/* harmony import */ var vue_moments_ago__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-moments-ago */ "./node_modules/vue-moments-ago/src/main.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      searchQuery: "",
      projects: [],
      allprojects: [],
      users: [],
      user: window.user,
      comment: '',
      currentProject: '',
      currentIndex: '',
      query: '',
      currentTask: undefined,
      cp: {
        id: '',
        name: ''
      },
      task: {
        name: '',
        users: new Set([])
      },
      baseUrl: window.axios.defaults.baseURL,
      dropzoneOptions: {
        autoProcessQueue: false,
        url: window.axios.defaults.baseURL,
        thumbnailWidth: 250,
        headers: {
          "Authorization": 'Bearer ' + this.getCookie('p_token')
        },
        addRemoveLinks: true
      }
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__["default"],
    VueTypeaheadBootstrap: vue_typeahead_bootstrap__WEBPACK_IMPORTED_MODULE_3__["default"],
    vue2Dropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_1___default.a,
    VueMomentsAgo: vue_moments_ago__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  mounted: function mounted() {
    this.getProjects();
    this.getAllProjects();
  },
  methods: {
    getCookie: function getCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
          c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }

      return null;
    },
    setUser: function setUser(event) {
      this.task.users.add(event);
    },
    removeUser: function removeUser(user) {
      this.task.users["delete"](user);
    },
    onDropzoneUploadComplete: function onDropzoneUploadComplete() {
      this.$refs.myVueDropzone.removeAllFiles();
      $('#createTask').modal('hide');
    },
    markComplete: function markComplete(id) {
      var _this = this;

      axios.get("/projects/tasks/".concat(id, "/mark-complete")).then(function (_ref) {
        var data = _ref.data;

        _this.$toastr.success(data.message);

        _this.currentTask = data.task;

        _this.getProjects(); // this.users = data.data;

      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this.$toastr.error(errors[key], "Error!");
        });
      });
    },
    addComment: function addComment(id) {
      var _this2 = this;

      axios.post("/projects/tasks/".concat(id, "/comment"), {
        comment: this.comment
      }).then(function (_ref2) {
        var data = _ref2.data;
        var a = '';
        a = $("<div class=\"mt-2 cstm-job-view-card\"><h5>".concat(user.first_name + ' ' + user.last_name, " </h5> <small><span data-v-55107a8f=\"\" class=\"vue-moments-ago\">posted just now</span></small> <p class=\"mt-2\">").concat(_this2.comment, "</p></div>"));
        $(".comment-section").append(a);
        $(".comment-text-area").val('');

        _this2.$toastr.success(data.message); // this.$router.go(this.$router.currentRoute) 


        _this2.getProjects();

        _this2.comment = "";
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this2.$toastr.error(errors[key], "Error!");
        });
      });
    },
    lookupUser: function lookupUser() {
      var _this3 = this;

      axios.get("/employees?disbloadr=true&name=".concat(this.query)).then(function (_ref3) {
        var data = _ref3.data;
        _this3.users = data.data;
      });
    },
    saveTask: function saveTask() {
      var _this4 = this;

      this.task.users = Array.from(this.task.users);
      this.task.project = this.currentProject;
      this.task.due = this.dateFormat(this.task.due_date);
      axios.post("/projects/tasks/store", this.task).then(function (_ref4) {
        var data = _ref4.data;
        _this4.task = {
          name: '',
          users: new Set([])
        };

        _this4.getProjects();

        $('#createTask').modal('hide');
        _this4.$refs.myVueDropzone.dropzone.options.url = window.axios.defaults.baseURL + "projects/tasks/".concat(data.data.id, "/files/upload");
        setTimeout(function () {
          _this4.$refs.myVueDropzone.processQueue();
        }, 500);

        _this4.$toastr.success(data.message, "Added!");
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this4.$toastr.error(errors[key], "Error!");
        });
      });
    },
    addProjectToBoard: function addProjectToBoard(id) {
      var _this5 = this;

      if (!this.cp.id) {
        this.$toastr.error("Select any project to assign on the board", "Error!");
        return;
      }

      axios.get("/projects/".concat(id, "/board/add/")).then(function (data) {
        _this5.$toastr.success(data.data.message, "Added!");

        document.querySelector(".modal-backdrop").style.display = "none";
        document.getElementById("addproject").classList.remove("show");

        _this5.getProjects();
      })["catch"](function (e) {
        var errors = e.response.data.errors;
        Object.keys(errors).forEach(function (key) {
          _this5.$toastr.error(errors[key], "Error!");
        });
      });
    },
    getAllProjects: function getAllProjects() {
      var _this6 = this;

      axios.get("/projects/logs/all?only_board=0").then(function (data) {
        _this6.allprojects = data.data;
      });
    },
    getProjects: function getProjects() {
      var _this7 = this;

      axios.get("/projects/logs/all?only_board=1").then(function (data) {
        _this7.projects = data.data;
      });
    },
    filterProjects: function filterProjects() {
      var _this8 = this;

      axios.get("/projects/logs/all?only_board=1&keyword=".concat(this.searchQuery)).then(function (data) {
        _this8.projects = data.data;
      });
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n#viewTask p {\n            font-weight: normal;\n}\n.glow {\n\t\t  font-size: 20px;\n\t\t  color: #fff;\n\t\t  text-align: center;\n\t\t  -webkit-animation: glow 1s ease-in-out infinite alternate;\n\t\t          animation: glow 1s ease-in-out infinite alternate;\n}\n@-webkit-keyframes glow {\nfrom {\n\t\t    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #1ba912, 0 0 40px #1ba912, 0 0 50px #1ba912, 0 0 60px #1ba912, 0 0 70px #1ba912;\n}\nto {\n\t\t    text-shadow: 0 0 20px #fff, 0 0 30px #1ba912, 0 0 40px #ff4da6, 0 0 50px #1ba912, 0 0 60px #1ba912, 0 0 70px #1ba912, 0 0 80px #1ba912;\n}\n}\n@keyframes glow {\nfrom {\n\t\t    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #1ba912, 0 0 40px #1ba912, 0 0 50px #1ba912, 0 0 60px #1ba912, 0 0 70px #1ba912;\n}\nto {\n\t\t    text-shadow: 0 0 20px #fff, 0 0 30px #1ba912, 0 0 40px #ff4da6, 0 0 50px #1ba912, 0 0 60px #1ba912, 0 0 70px #1ba912, 0 0 80px #1ba912;\n}\n}\n.mark-complete{\n        \tcolor: #1ba912;\n            font-size: 20px;\n            cursor: pointer;\n}\nh5.mark-complete.active{\n        \tcolor: #1ba912;\n            font-size: 20px\n}\n.mark-complete i.fa-check {\n\t\t    color: white;\n\t\t    background: #ccc;\n\t\t    padding: 7px;\n\t\t    border-radius: 50%;\n}\n.mark-complete.active i.fa-check {\n\t\t    color: white;\n\t\t    background: green;\n\t\t    padding: 7px;\n\t\t    border-radius: 50%;\n}\n.boad-card span.mark-complete {\n            font-size: 10px;\n}\n   \t\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./TaskManagmentComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css&");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=template&id=55fe6612&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=template&id=55fe6612& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", {}, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "boad-top-bar" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-7" }, [
            _c("ul", { staticClass: "nav nav-tabs" }, [
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link active",
                      attrs: { to: { name: "projects.tm" } }
                    },
                    [_vm._v("Board")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: { to: { name: "projects.calender" } }
                    },
                    [_vm._v("Calendar")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm._m(0)
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-5" }, [
            _c("div", { staticClass: "csmtr-project-listing-srch2 w-100" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.searchQuery,
                    expression: "searchQuery"
                  }
                ],
                attrs: { type: "text" },
                domProps: { value: _vm.searchQuery },
                on: {
                  keyup: _vm.filterProjects,
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.searchQuery = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(1)
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "tab-content" }, [
        _c(
          "div",
          { staticClass: "container tab-pane active", attrs: { id: "board" } },
          [
            _c(
              "div",
              { staticClass: "row" },
              _vm._l(_vm.projects, function(project, index) {
                return _c("div", { key: index, staticClass: "col-lg-4" }, [
                  project.project != _vm.NULL
                    ? _c(
                        "div",
                        { staticClass: "boad-main" },
                        [
                          _c("div", { staticClass: "boad-header" }, [
                            _c("h3", [_vm._v(_vm._s(project.project.name))]),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "add-btn",
                                attrs: {
                                  type: "button",
                                  "data-toggle": "modal",
                                  "data-target": "#createTask"
                                },
                                on: {
                                  click: function($event) {
                                    ;(_vm.currentIndex = index),
                                      (_vm.currentProject = project)
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fas fa-plus-circle" })]
                            )
                          ]),
                          _vm._v(" "),
                          _vm._l(project.project.task, function(task, index) {
                            return _c(
                              "div",
                              { key: index, staticClass: "boad-card" },
                              [
                                _c("div", { staticClass: "d-flex" }, [
                                  _c("h4", { staticClass: "task-name" }, [
                                    _vm._v(
                                      _vm._s(task.title) +
                                        "\n                                        "
                                    ),
                                    task.status == 0
                                      ? _c(
                                          "span",
                                          { staticClass: "mark-complete" },
                                          [
                                            _c("i", {
                                              staticClass: "fa fa-check"
                                            })
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    task.status == 1
                                      ? _c(
                                          "span",
                                          {
                                            staticClass: "mark-complete active"
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fa fa-check"
                                            })
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    task.status == 2
                                      ? _c(
                                          "span",
                                          {
                                            staticClass: "mark-complete danger"
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fa fa-times"
                                            })
                                          ]
                                        )
                                      : _vm._e()
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("p", [_vm._v(_vm._s(task.description))]),
                                _vm._v(" "),
                                _c("div", { staticClass: "boad-card-bottom" }, [
                                  _c("span", [
                                    _vm._v(_vm._s(task.created_date))
                                  ])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "btn-group timeline-card-top-drop-down"
                                  },
                                  [
                                    _vm._m(2, true),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass: "dropdown-menu",
                                        staticStyle: {
                                          position: "absolute",
                                          transform:
                                            "translate3d(0px, 23px, 0px)",
                                          top: "0px",
                                          left: "0px",
                                          "will-change": "transform"
                                        },
                                        attrs: { "x-placement": "bottom-start" }
                                      },
                                      [
                                        _c(
                                          "a",
                                          {
                                            staticClass:
                                              "dropdown-item l-grey uppercase",
                                            attrs: {
                                              "data-toggle": "modal",
                                              "data-target": "#viewTask",
                                              href: "javascript:;"
                                            },
                                            on: {
                                              click: function($event) {
                                                ;(_vm.currentTask = task),
                                                  (_vm.currentProject = project)
                                              }
                                            }
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fas fa-trash"
                                            }),
                                            _vm._v(
                                              "\n                                            View"
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ]
                            )
                          })
                        ],
                        2
                      )
                    : _vm._e()
                ])
              }),
              0
            )
          ]
        ),
        _vm._v(" "),
        _c("div", {
          staticClass: "container tab-pane active",
          attrs: { id: "calendar" }
        })
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "addproject",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "exampleModalCenterTitle",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered ",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(3),
              _vm._v(" "),
              _c("div", { staticClass: "p-4" }, [
                _c("h5", [_vm._v("Add Project")]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group mt-3" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.cp.id,
                          expression: "cp.id"
                        }
                      ],
                      staticClass: "new-input",
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.cp,
                            "id",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "" } }, [
                        _vm._v("Select")
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.allprojects, function(project, index) {
                        return _c(
                          "option",
                          {
                            key: index,
                            domProps: {
                              value: project.id,
                              innerHTML: _vm._s(
                                project.job
                                  ? project.project.name +
                                      " - " +
                                      project.job.title
                                  : project.project.name
                              )
                            }
                          },
                          [
                            _vm._v(
                              "\n                                " +
                                _vm._s(project.project.name)
                            )
                          ]
                        )
                      })
                    ],
                    2
                  )
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "main-blue-btn",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        return _vm.addProjectToBoard(_vm.cp.id)
                      }
                    }
                  },
                  [_vm._v("Add")]
                )
              ])
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "createTask",
          tabindex: "-1",
          role: "dialog",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered modal-lg",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(4),
              _vm._v(" "),
              _c("div", { staticClass: "p-4" }, [
                _c("h5", [_vm._v("Create Task")]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group mt-3" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.task.name,
                        expression: "task.name"
                      }
                    ],
                    staticClass: "new-input",
                    attrs: { type: "text", placeholder: "Task Name:" },
                    domProps: { value: _vm.task.name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.task, "name", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.task.description,
                        expression: "task.description"
                      }
                    ],
                    staticClass: "txtara",
                    attrs: { placeholder: "Task Description:" },
                    domProps: { value: _vm.task.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.task, "description", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("label", [_vm._v("Due Date:")]),
                    _vm._v(" "),
                    _c("datepicker", {
                      directives: [
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      attrs: {
                        "input-class": "new-input",
                        name: "due",
                        "disabled-dates": {
                          to: new Date(Date.now() - 8640000)
                        },
                        format: "dd-MM-yyyy",
                        placeholder: ""
                      },
                      model: {
                        value: _vm.task.due_date,
                        callback: function($$v) {
                          _vm.$set(_vm.task, "due_date", $$v)
                        },
                        expression: "task.due_date"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c(
                    "div",
                    { staticClass: "csmtr-project-listing-srch2 w-100" },
                    [
                      _c("vue-typeahead-bootstrap", {
                        staticClass: "mb-4",
                        attrs: {
                          data: _vm.users,
                          minMatchingChars: 1,
                          placeholder: "Search Employee",
                          serializer: function(item) {
                            return item.name
                          }
                        },
                        on: {
                          hit: function($event) {
                            return _vm.setUser($event)
                          },
                          input: _vm.lookupUser
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "suggestion",
                            fn: function(ref) {
                              var data = ref.data
                              var htmlText = ref.htmlText
                              return [
                                _c("div", {}, [
                                  _c("img", {
                                    staticClass: "rounded-circle float-left",
                                    staticStyle: {
                                      width: "40px",
                                      height: "40px"
                                    },
                                    attrs: { src: data.image }
                                  }),
                                  _vm._v(" "),
                                  _c("span", {
                                    staticClass: "mt-3 ml-2 float-left",
                                    domProps: { innerHTML: _vm._s(htmlText) }
                                  })
                                ])
                              ]
                            }
                          }
                        ]),
                        model: {
                          value: _vm.query,
                          callback: function($$v) {
                            _vm.query = $$v
                          },
                          expression: "query"
                        }
                      }),
                      _vm._v(" "),
                      _vm._m(5)
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "popup-tags" },
                  _vm._l(_vm.task.users, function(user, index) {
                    return _c("span", { key: index }, [
                      _vm._v(
                        "\n                            " +
                          _vm._s(user.name) +
                          " "
                      ),
                      _c(
                        "a",
                        {
                          attrs: { href: "jaavscript:;" },
                          on: {
                            click: function($event) {
                              return _vm.removeUser(user)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fas fa-times-circle" })]
                      )
                    ])
                  }),
                  0
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c(
                      "vue2Dropzone",
                      {
                        ref: "myVueDropzone",
                        attrs: {
                          id: "dropzone",
                          options: _vm.dropzoneOptions,
                          useCustomSlot: true
                        },
                        on: {
                          "vdropzone-queue-complete":
                            _vm.onDropzoneUploadComplete
                        }
                      },
                      [
                        _c("button", { staticClass: "main-blue-btn2" }, [
                          _vm._v("Upload file")
                        ]),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "Drag and drop any image or document that might be helpful in explaining your\n                                project"
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "main-blue-btn",
                    attrs: { type: "button" },
                    on: { click: _vm.saveTask }
                  },
                  [_vm._v("Create Task")]
                )
              ])
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "viewTask",
          tabindex: "-1",
          role: "dialog",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered modal-lg",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(6),
              _vm._v(" "),
              _vm.currentTask
                ? _c("div", { staticClass: "p-4" }, [
                    _vm.currentTask.status == 0
                      ? _c(
                          "h5",
                          {
                            staticClass: "mark-complete",
                            on: {
                              click: function($event) {
                                return _vm.markComplete(_vm.currentTask.id)
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fa fa-check" }),
                            _vm._v(" Mark Complete\n                    ")
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.currentTask.status == 1
                      ? _c("h5", { staticClass: "mark-complete active" }, [
                          _c("i", { staticClass: "fa fa-check" }),
                          _vm._v(" Completed\n                    ")
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.currentTask.status == 2
                      ? _c("h5", { staticClass: "mark-complete danger" }, [
                          _c("i", { staticClass: "fa fa-times" }),
                          _vm._v(" Rejected\n                    ")
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group mt-3" }, [
                      _c(
                        "div",
                        { staticClass: "col-lg-12" },
                        [
                          _c("h4", { staticClass: "blue-h5 mb-3" }, [
                            _vm._v(
                              "\n                                " +
                                _vm._s(_vm.currentTask.title) +
                                "\n                            "
                            )
                          ]),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Due Date")]),
                          _vm._v(" "),
                          _c(
                            "p",
                            [
                              _c("vue-moments-ago", {
                                attrs: {
                                  prefix: "",
                                  suffix: "ago",
                                  date: _vm.currentTask.due,
                                  lang: "en"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Project")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(_vm._s(_vm.currentProject.project.name))
                          ]),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Task Description:")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(_vm._s(_vm.currentTask.description))
                          ]),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Attachements")]),
                          _vm._v(" "),
                          _vm._l(_vm.currentTask.media, function(med, mindex) {
                            return _c(
                              "span",
                              { key: mindex, staticClass: "project-attach-cv" },
                              [
                                _c("img", {
                                  attrs: {
                                    src: _vm.getIconByMimeType(med.mime_type),
                                    alt: ""
                                  }
                                }),
                                _vm._v(
                                  _vm._s(med.file_name) +
                                    "\n                                "
                                ),
                                _c(
                                  "a",
                                  {
                                    attrs: { href: med.full_url, download: "" }
                                  },
                                  [_c("i", { staticClass: "fas fa-eye" })]
                                )
                              ]
                            )
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "popup-tags" },
                            [
                              _c("h5", { staticClass: "mb-2" }, [
                                _vm._v("Assigned To:")
                              ]),
                              _vm._v(" "),
                              _vm._l(_vm.currentTask.users, function(
                                tu,
                                tuindex
                              ) {
                                return _c("span", { key: tuindex }, [
                                  _vm._v(
                                    "\n                                    " +
                                      _vm._s(tu.name) +
                                      "\n                                "
                                  )
                                ])
                              })
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-12 row comment-section" },
                            [
                              _c("h5", [_vm._v("Comments: ")]),
                              _vm._v(" "),
                              _vm._l(_vm.currentTask.comments, function(
                                com,
                                cindex
                              ) {
                                return _c(
                                  "div",
                                  {
                                    key: cindex,
                                    staticClass: "mt-2 cstm-job-view-card"
                                  },
                                  [
                                    _c("h5", [
                                      _vm._v(_vm._s(com.commentaddable.name))
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "small",
                                      [
                                        _c("vue-moments-ago", {
                                          attrs: {
                                            prefix: "posted at ",
                                            suffix: "ago",
                                            date: com.created_at,
                                            lang: "en"
                                          }
                                        })
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("p", { staticClass: "mt-2" }, [
                                      _vm._v(_vm._s(com.comment))
                                    ])
                                  ]
                                )
                              })
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.comment,
                                expression: "comment"
                              }
                            ],
                            staticClass: "txtara comment-text-area",
                            attrs: {
                              placeholder: "Ask a question or post a comment"
                            },
                            domProps: { value: _vm.comment },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.comment = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "main-blue-btn",
                              attrs: { type: "button" },
                              on: {
                                click: function($event) {
                                  return _vm.addComment(_vm.currentTask.id)
                                }
                              }
                            },
                            [_vm._v("Comment")]
                          )
                        ],
                        2
                      )
                    ])
                  ])
                : _vm._e()
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item" }, [
      _c(
        "a",
        {
          staticClass: "add-project",
          attrs: {
            href: "#",
            "data-toggle": "modal",
            "data-target": "#addproject"
          }
        },
        [_c("i", { staticClass: "fas fa-plus-circle" }), _vm._v(" Add Project")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", { attrs: { type: "button" } }, [
      _c("i", { staticClass: "fas fa-search" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn dropdown-toggle btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-ellipsis-v" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close text-right mr-1 mt-1",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close text-right mr-1 mt-1",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", { attrs: { type: "button" } }, [
      _c("i", { staticClass: "fas fa-search" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close text-right mr-1 mt-1",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/views/projects/TaskManagmentComponent.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/user/views/projects/TaskManagmentComponent.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TaskManagmentComponent_vue_vue_type_template_id_55fe6612___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TaskManagmentComponent.vue?vue&type=template&id=55fe6612& */ "./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=template&id=55fe6612&");
/* harmony import */ var _TaskManagmentComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TaskManagmentComponent.vue?vue&type=script&lang=js& */ "./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _TaskManagmentComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TaskManagmentComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _TaskManagmentComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TaskManagmentComponent_vue_vue_type_template_id_55fe6612___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TaskManagmentComponent_vue_vue_type_template_id_55fe6612___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/views/projects/TaskManagmentComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./TaskManagmentComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./TaskManagmentComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=template&id=55fe6612&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=template&id=55fe6612& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_template_id_55fe6612___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./TaskManagmentComponent.vue?vue&type=template&id=55fe6612& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/views/projects/TaskManagmentComponent.vue?vue&type=template&id=55fe6612&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_template_id_55fe6612___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TaskManagmentComponent_vue_vue_type_template_id_55fe6612___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);