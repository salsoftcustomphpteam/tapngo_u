<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
// use DB;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [['name'=>'admin',   'status'=> '1'],['name'=>'user',    'status'=> '1']];
        Role::insert($data);
    }
}
