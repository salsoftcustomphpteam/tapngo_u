<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       
        $this->call([
            RoleSeeder::class,
            AdminSeeder::class,
        ]);

        factory(User::class, 20)->create();

    
        
    }
}
