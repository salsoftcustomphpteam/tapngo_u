<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make("admin123"),
        "tag_line" => $faker->tag_line,
        "work" => $faker->work,
        "favorite_food" => $faker->favorite_food,
        "favorite_song" => $faker->favorite_song,
        "favorite_book" => $faker->favorite_book,
        "high_level_of_education" => $faker->high_level_of_education,
        "profile_image" => 'default.png',
        "phone_number" => $faker->phone_number,
        "device_id" => $faker->device_id,
        "device_type" => $faker->device_type,
        "status" => "1",

    ];
});
