<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Feedback;
use Faker\Generator as Faker;

$factory->define(Feedback::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => Hash::make("Admin123"),
        'remember_token' => Str::random(10),
        "phone" => $faker->phoneNumber,
        "dob" => null,
        "address" => $faker->address,
        "country" => $faker->country,
        "state" => $faker->state,
        "city" => $faker->city,
        "zip" => $faker->postcode,
    ];
});
