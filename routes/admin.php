<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:admin_api'], function(){

    Route::apiResource('dashboard', 'DashboardController');
    Route::apiResource('feedbacks', 'FeedbackController');    
});

