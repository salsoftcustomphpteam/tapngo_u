<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'user'], function() {
    Route::post('signup', 'Api\User\UserController@signUp')->name('signup');
    Route::post('login', 'Api\User\UserController@login')->name('login');
    Route::post('forgot-password', 'Api\User\UserController@resetPasswordRequest');
    Route::post('verify-code', 'Api\User\UserController@verifyCode');
    Route::post('reset-password', 'Api\User\UserController@resetPassword');


    Route::group(['middleware' => 'auth:api'], function(){
        //Users
        Route::get('home', 'Api\User\HomeController@index');
        Route::post('change-password', 'Api\User\UserController@changePassword');
        Route::get('get-profile', 'Api\User\UserController@getProfile');
        //booking
        Route::resource('bookings', 'Api\User\BookingController');
        Route::post('update-profile', 'UserController@updateProfile');
        Route::post('complaint', 'UserController@complaint');
        Route::get('refferal', 'User\RefferalController@index');
        Route::resource('wallet', 'User\WalletController');


    });

    // Contact US
    // Route::post('contact-us/store', 'UserController@contactUs');
    // Route::post('subscribe/store', 'UserController@subscribe');
});

