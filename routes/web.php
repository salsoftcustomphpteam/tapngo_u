<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::post('custom-login',  'UserController@customLogin')->name('customlogin');

//Admin Routes
Route::prefix('admin')->name('admins.')->namespace('Admin')->group(function(){
    Auth::routes();
    Route::get('logout',function(){
        Auth::guard('admin')->logout();
        Auth::logout();
        return redirect(url('/admin/login'));
    });
    Route::view('/{home?}','admins/welcome')->middleware('auth:admin')->where('home', '.*');
});

// User Routes
Route::get('logout',function(){
    Auth::logout();
    return redirect(url('/login'));
});
Auth::routes();
Route::view('signup-as', 'auth.signup-as')->name('signup.as');
Route::get('/{home?}', function(){
    if(auth('admin')->check())
        return view('admins.welcome');
    // else if(auth('business')->check())
    //     return view('business.welcome');
    else
        return view('users.welcome');
})->where('home', '.*');

